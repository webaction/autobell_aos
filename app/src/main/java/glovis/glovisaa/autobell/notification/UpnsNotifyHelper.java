package glovis.glovisaa.autobell.notification;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.RingtoneManager;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;

import androidx.core.app.NotificationCompat;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;

import glovis.glovisaa.autobell.BuildConfig;
import glovis.glovisaa.autobell.MainActivity;
import glovis.glovisaa.autobell.PushMessageActivity;
import glovis.glovisaa.autobell.R;
import glovis.glovisaa.autobell.db.DBUtils;
import m.client.push.library.common.PushConstants;

public class UpnsNotifyHelper {

	public static final String NOTIFICATION_CHANNEL_ID = "10001";

	/**
	 * showNotification
	 * 메세지를 분석하여 타입 별 노티피케이션 생성
	 * @param context
	 * @param jsonMsg
	 * @param psid
	 * @param encryptData
	 * @throws Exception
	 */
	public static void showNotification(final Context context, final JSONObject jsonMsg, final String psid, final String encryptData) throws Exception {
	    String extData = "";
	    final String SEQNO = jsonMsg.getString("SEQNO");
	    if (jsonMsg.has(PushConstants.KEY_EXT)) {
	        extData = jsonMsg.getString(PushConstants.KEY_EXT);
	    }
	 
	    // EXT가 존재할 경우 다음과 같은 형태로 전달됨
	    // extData = "http:/211.241.199.158:8180/msg/totalInfo/0218115649_msp.html";
	    if (extData.startsWith("http")) { // HTTP 로 시작하는 url 형태일 경우 - 메세지 구분을 위한 스트링 형태로 서버로부터 내려받는다.
	    	// HttpGetStringThread로부터 응답값을 받은 후 처리 로직
			Handler handler = new Handler() {
				@Override
				public void handleMessage(Message msg) {
	        		if(msg.what == 0) {
	        			String message = (String) msg.obj;
	        			if (message != null) {
	        				message = message.replaceAll("https", "http");
	        				message = message.replaceAll("\\\\", "/");

	        				try { 
	        					// 다음과 같은 형태로 응답 값이 구성되었을 경우 치환을 위한 로직 수행
	        					// "TYPE":"R","VAR":"hoseok|2015-06-31|2015-07-30|104320|11"
	        					if (jsonMsg.has("TYPE") && jsonMsg.getString("TYPE").equals("R")) {
	        						String var = jsonMsg.getString("VAR");
	        						HashMap<String, String> varMap = new HashMap<String, String>();
	        						if (var != null) {
	        							String[] varArray = var.split("\\|");
	        							for (int i=0; i<varArray.length; i++) {
	        								int idx = i+1;
	        								varMap.put("%VAR" + idx + "%", varArray[i]);
	        								//Log.d("test", "%VAR" + idx + "%" + " " +  varArray[i]);
	        							}
	        							
	        							Iterator<?> keys = varMap.keySet().iterator();
	        							while (keys.hasNext()) {
	        								String key = (String) keys.next();
	        								message = message.replaceAll(key, varMap.get(key));
	        								//Log.d("test", "message: " + message);
	        							}
	        						}
	        					}
	        				} catch (JSONException e) {
	        					e.printStackTrace();
	        				}
	        			}
	        			
	        			DBUtils.getDbOpenHelper(context).insertPushMSG(SEQNO, jsonMsg.toString(), message, encryptData);
	        			createNotification(context, jsonMsg, psid, encryptData, message);
	        		}
				}
			};
			// EXT url 로부터 메세지 정보를 수신하기 위한 쓰레드
			new HttpGetStringThread(handler, extData).start();
		}
		else { // 일반 스크링의 경우
			DBUtils.getDbOpenHelper(context).insertPushMSG(SEQNO, jsonMsg.toString(), extData, encryptData);
			createNotification(context, jsonMsg, psid, encryptData, extData);
		}
	}	
	
	/**
	 * createNotification
	 * 노티피케이션 생성
	 * @param context
	 * @param jsonMsg
	 * @param psid
	 * @param encryptData
	 * @param message
	 */
	private static void createNotification(final Context context, final JSONObject jsonMsg, final String psid, final String encryptData, String message) {
		ArrayList<String> params = null;
		if (message != null) {
			String[] paramArray = message.split("\\|");
			params = new ArrayList<String>(Arrays.asList(paramArray));
		}

		Log.d("NotificationManager", "[NotificationManager] params size:: " + params.size());
		// 메세지 타입에 따라 노티피케이션 생성 - 용도에 따라 커스터마이징하여 사용이 가능 (타입 번호/파싱 룰 등등..)
		if (params != null && params.size() > 0) {
			try {
				// URL을 실제 상세 메세지 데이터로 치환
				jsonMsg.remove(PushConstants.KEY_EXT);
				jsonMsg.put(PushConstants.KEY_EXT, message);
				
				// 일반 메세지의 경우
				if (params.get(0).equals("0")) {
					Log.d("NotificationManager", "[NotificationManager] defaultNotification: " + message);
					defaultNotification(context, jsonMsg, params.get(1), psid, message, encryptData);
				}
				// 이미지 메세지의 경우
				else if (params.size() > 2) {
					Log.d("NotificationManager", "[NotificationManager] showImageNotification: " + message);
					showImageNotification(context, jsonMsg, params.get(1), params.get(2), psid, message);
				}
				// 기타의 경우 - 일반메세지로 처리했으나 용도에 맞게 변경
				else {
					Log.d("NotificationManager", "[NotificationManager] UNKNOUWN TYPE:: " + params.get(0));
					defaultNotification(context, jsonMsg, message, psid, message, encryptData);
				}
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
		
	private static void defaultNotification(Context context, JSONObject jsonMsg, String strMessage, String psid, String ext, String encryptData) throws Exception {
		String alertMessage = jsonMsg.getString(PushConstants.KEY_MESSAGE);
//	    int icon = R.mipmap.ic_launcher;
//	    String title = context.getString(R.string.app_name);
	    int seqno = Integer.parseInt(jsonMsg.getString(PushConstants.KEY_SEQNO));
	    
	    /*Intent intent = new Intent(context, PushMessageActivity.class);
		intent.putExtra(PushConstants.KEY_JSON, jsonMsg.toString());
    	intent.putExtra(PushConstants.KEY_PSID, psid);
    	intent.putExtra(PushConstants.KEY_TITLE, alertMessage);
    	intent.putExtra(PushConstants.KEY_EXT, ext);
    	intent.putExtra("ENCRYPT", encryptData);
    	intent.putExtra(PushConstants.KEY_PUSHTYPE, "UPNS");
    	intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);*/
	    // 일반 푸시일 경우, 메시지 터치 시 앱 메인화면으로 이동하도록 수정
		Intent intent = new Intent(context, MainActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);

		PendingIntent pIntent = PendingIntent.getActivity(context, seqno, intent, PendingIntent.FLAG_UPDATE_CURRENT);		
		//mBuilder.setDefaults(Notification.DEFAULT_VIBRATE);
		PushNotificationManager.sendNotification(context, 1, PushNotificationManager.Channel.MESSAGE, BuildConfig.APP_NAME, alertMessage, pIntent);
	}
	
	private static void showImageNotification(Context context, final JSONObject jsonMsg, final String strMessage, String img, final String psid, final String ext) {
		final int icon = R.mipmap.ic_launcher;
		final String title = BuildConfig.APP_NAME;
		final Context ctx = context;
		
		if (img.contains("https")) {
			img = img.replaceAll("https", "http");
			img = img.replaceAll("\\\\", "/");
		}
		
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context).build();
		ImageLoader.getInstance().init(config);
		ImageLoader.getInstance().loadImage(img, new SimpleImageLoadingListener() {
			
		    @Override
		    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
		    	notificationWithBigPicture(ctx, jsonMsg, title, strMessage, icon, loadedImage, psid, ext);
		    }
		});		
	}
	
	private static void notificationWithBigPicture(Context context, JSONObject jsonMsg, String title, String message, int icon, Bitmap banner, String psid, String ext) {
		
		Intent intent = new Intent(context, PushMessageActivity.class);
		intent.putExtra(PushConstants.KEY_JSON, jsonMsg.toString());
		intent.putExtra(PushConstants.KEY_PSID, psid);
		intent.putExtra(PushConstants.KEY_TITLE, message);
		intent.putExtra(PushConstants.KEY_EXT, ext);
		intent.putExtra(PushConstants.KEY_PUSHTYPE, "UPNS");
		intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY); 
    	int seqno = 0;
		try {
			seqno = Integer.parseInt(jsonMsg.getString("SEQNO"));
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		PendingIntent pendingIntent = PendingIntent.getActivity(context, seqno, intent, PendingIntent.FLAG_UPDATE_CURRENT);
		
		NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
			.setSmallIcon(icon)
			.setTicker(title)
			.setContentTitle(title)
			.setContentText(message)
			.setAutoCancel(true);
		
		NotificationCompat.BigPictureStyle style = new NotificationCompat.BigPictureStyle();
		style.setBigContentTitle(title);
		style.setSummaryText(message);
		style.bigPicture(banner);
		
		builder.setStyle(style);
		builder.setContentIntent(pendingIntent);
		
		builder.setDefaults(Notification.DEFAULT_VIBRATE);
		builder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
		NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.notify("upns", seqno/*seqno*/, builder.build());
	}
}