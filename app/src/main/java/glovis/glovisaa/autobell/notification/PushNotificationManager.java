package glovis.glovisaa.autobell.notification;

import android.annotation.TargetApi;
import android.app.KeyguardManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationChannelGroup;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringDef;
import androidx.core.app.NotificationCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.FutureTarget;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import glovis.glovisaa.autobell.BuildConfig;
import glovis.glovisaa.autobell.PushMessageActivity;
import glovis.glovisaa.autobell.R;
import glovis.glovisaa.autobell.ShowPushPopup;
import glovis.glovisaa.autobell.service.OhMyCallService;
import glovis.glovisaa.autobell.util.Common;
import m.client.push.library.PushManager;
import m.client.push.library.common.PushConstants;

public class PushNotificationManager {
	// 마지막 메세지 체크를 위한 변수
	private static String lastestSeqNo = "";

	private static final String TAG = Common.COMPANY + "PushNotiMng";
	/**
	 * createNotification
	 * 푸시 타입에 따른 노티피케이션 생성
	 * @param context
	 * @param intent
	 * @param pushType
	 * UPNS = {"HEADER":{"ACTION":"SENDMSG"},"BODY":{"MESSAGE":"dfsfdsfsdaf","EXT":"","SEQNO":"607","PSID":"3d5c7522a0a893957d230d70d196e8dcbf3785e9","APPID":"kr.co.morpheus.mobile1","CUID":"12345","MESSAGEID":"20150212143841e6fcce8440e502b45a223a54f0","PUBLIC":"N","SENDERCODE":"admin"}}
	 * GCM = {"aps":{"badge":"1","sound":"alert.aif","alert":"fdsafasfa"},"mps":{"appid":"com.uracle.push.test","messageuniquekey":"20141204112801b6f1fd7840868afc82e51fd6a5","seqno":"387","sender":"admin",,"ext":"http://211.241.199.139:8080/msp-admin/totalInfo\\1203143401_I.html"}}
	 * http://docs.morpheus.co.kr/client/push/gcm.html#service 참고.
	 */
	public static void createNotification(final Context context, final Intent intent, final String pushType) {
		try {
			// 타입에 따라 프로세스 진행
			if (pushType.equals(PushConstants.STR_UPNS_PUSH_TYPE)) { // UPNS
				createUpnsNotification(context, intent);
			}
			else { // GCM
				createGcmNotification(context, intent);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * isRestrictedScreen
	 * 화면 온/오프 상태 체크 
	 * @param context
	 * @return
	 */
	public static boolean isRestrictedScreen(final Context context) {
		KeyguardManager km = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
		return km.inKeyguardRestrictedInputMode();
	}
	
	/**
	 * showToast
	 * 토스트 발생 - 각 용도에 맞게 커스터마이징
	 * @param context
	 * @param intent
	 * @param pushType
	 * @throws Exception
	 */
	public static void showToast(final Context context, final Intent intent, String pushType) throws Exception {
		String jsonData = intent.getExtras().getString(PushConstants.KEY_JSON);
		//String psid = intent.getExtras().getString(PushConstants.KEY_PSID);
		JSONObject jsonMsg = new JSONObject(jsonData);
		
		String extData = ""; // ext 데이터
		String title = ""; // 메세지 타이틀
		String seqno = ""; // 메세지 시퀀스
		if (PushConstants.STR_UPNS_PUSH_TYPE.equals(pushType)) { // UPNS
			if (jsonMsg.has(PushConstants.KEY_EXT)) { // 존재하지 않을 수 있으므로
				extData = jsonMsg.getString(PushConstants.KEY_EXT);
		    }
			// 필수 항목
			title = jsonMsg.getString(PushConstants.KEY_MESSAGE);
		}
		else { //GCM
			JSONObject aps = jsonMsg.getJSONObject(PushConstants.KEY_APS);
			JSONObject mps = jsonMsg.getJSONObject(PushConstants.KEY_MPS);
			if (aps.has(PushConstants.KEY_GCM_ALERT)) {
				title = aps.getString(PushConstants.KEY_GCM_ALERT);
		    }
		    if (mps.has(PushConstants.KEY_EXT)) {
		    	extData = mps.getString(PushConstants.KEY_EXT);
		    }
		    if (mps.has(PushConstants.KEY_GCM_SEQNO)) {
		    	seqno = mps.getString(PushConstants.KEY_GCM_SEQNO);
		    	// 중복 메세지에 대한 체크
		    	if (lastestSeqNo.equals(seqno)) {
		    		return;
		    	}
		    	lastestSeqNo = seqno;
		    }
		}
		
		Toast.makeText(context, title + ": " + extData,Toast.LENGTH_SHORT).show();
	}
	
	/**
	 * showNotificationPopupDialog
	 * 팝업 다이얼로그 예시 - 용도에 맞게 커스터마이징
	 * @param context
	 * @param intent
	 * @param pushType
	 * @throws Exception
	 */
	public static void showNotificationPopupDialog(final Context context, final Intent intent, String pushType) throws Exception {
		String jsonData = intent.getExtras().getString(PushConstants.KEY_JSON);
		String psid = intent.getExtras().getString(PushConstants.KEY_PSID);
		JSONObject jsonMsg = new JSONObject(jsonData);
		
		String extData = "";
		String title = "";
		String seqno = "";
		if (PushConstants.STR_UPNS_PUSH_TYPE.equals(pushType)) {
			if (jsonMsg.has(PushConstants.KEY_EXT)) {
				extData = jsonMsg.getString(PushConstants.KEY_EXT);
		    }
			title = jsonMsg.getString(PushConstants.KEY_MESSAGE);
		}
		else {
			JSONObject aps = jsonMsg.getJSONObject(PushConstants.KEY_APS);
			JSONObject mps = jsonMsg.getJSONObject(PushConstants.KEY_MPS);
			if (aps.has(PushConstants.KEY_GCM_ALERT)) {
				title = aps.getString(PushConstants.KEY_GCM_ALERT);
		    }
		    if (mps.has(PushConstants.KEY_EXT)) {
		    	extData = mps.getString(PushConstants.KEY_EXT);
		    }
		    if (mps.has(PushConstants.KEY_GCM_SEQNO)) {
		    	seqno = mps.getString(PushConstants.KEY_GCM_SEQNO);
		    	// 중복 메세지에 대한 체크
		    	if (lastestSeqNo.equals(seqno)) {
		    		return;
		    	}
		    	lastestSeqNo = seqno;
		    }
		}
		
		Intent popupIntent = new Intent(); 
		popupIntent.setClass(context, ShowPushPopup.class);
		popupIntent.setAction(ShowPushPopup.class.getName());
		popupIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		popupIntent.setAction(ShowPushPopup.class.getName());
		popupIntent.putExtra(PushConstants.KEY_TITLE, title);
		popupIntent.putExtra(PushConstants.KEY_MESSAGE, "message");
		popupIntent.putExtra(PushConstants.KEY_JSON, jsonData);
		popupIntent.putExtra(PushConstants.KEY_PSID, psid);
		popupIntent.putExtra(PushConstants.KEY_EXT, extData);
		popupIntent.putExtra(PushConstants.KEY_PUSHTYPE, pushType);
	    context.startActivity(popupIntent);
	}
	
	/**
	 * createUpnsNotification
	 * UPNS 노티피케이션 생성
	 * @param context
	 * @param intent
	 * @throws Exception
	 */
	public static void createUpnsNotification(final Context context, final Intent intent) throws Exception {
        String jsonData = intent.getExtras().getString(PushConstants.KEY_JSON);
        String encryptData = intent.getExtras().getString(PushConstants.KEY_MESSAGE_ENCRYPT);
        if (jsonData != null) {
        	jsonData = jsonData.replaceAll("https", "http");
        	jsonData = jsonData.replaceAll("\\\\", "/");
        	jsonData = jsonData.replaceAll("//", "/");
        }
        
        try {
        	Log.d(TAG, "[PushNotificationManager] createUpnsNotification: " + jsonData);
	        JSONObject jsonMsg = new JSONObject(jsonData);
	        String psid = intent.getExtras().getString(PushConstants.KEY_PSID);
	        //PushWakeLock.acquireCpuWakeLock(context);
	        UpnsNotifyHelper.showNotification(context, jsonMsg, psid, encryptData);
	        //PushWakeLock.releaseCpuLock();
        } 
        catch(Exception e) {
        	e.printStackTrace();
        }	
	}

	public static String NOTIFICATION_CHANNEL_ID =  "";
	public static CharSequence name = "";

	/**
	 * createGcmNotification
	 * gcm 노티피케이션 생성
	 * @param context
	 * @param intent
	 * @throws Exception
	 */
	public static void createGcmNotification(final Context context, final Intent intent) throws Exception {

		NOTIFICATION_CHANNEL_ID = context.getPackageName();
		name = BuildConfig.APP_NAME;//context.getString(R.string.app_name);

		String jsonData = intent.getExtras().getString(PushConstants.KEY_JSON);
		String psid = intent.getExtras().getString(PushConstants.KEY_PSID);
		JSONObject rootJsonObj = new JSONObject(jsonData);
	    JSONObject apsJsonObj = rootJsonObj.getJSONObject(PushConstants.KEY_APS);
	    JSONObject mpsJsonObj = rootJsonObj.getJSONObject(PushConstants.KEY_MPS);

		String alertMessage = apsJsonObj.getString(PushConstants.KEY_GCM_ALERT);
		String alertTitle = "";
		String alertBody = "";
		try {
//			String jsonString = apsJsonObj.getString(PushConstants.KEY_GCM_ALERT);
			JSONObject jsonObject = new JSONObject(alertMessage);
			alertTitle = jsonObject.getString("title");
			alertBody = jsonObject.getString("body");
		} catch (JSONException e) {
			alertTitle = BuildConfig.APP_NAME;
			alertBody = alertMessage;
			e.printStackTrace();
		}


	    String seqNo = mpsJsonObj.getString(PushConstants.KEY_GCM_SEQNO);
	    if (lastestSeqNo.equals(seqNo)) {
    		return;
    	}
    	lastestSeqNo = seqNo;
	    
	    String extUrl = "";
		String pushKind = "";
		String imgUrl = "";
		String linkUrl = "";
	    if (mpsJsonObj.has(PushConstants.KEY_LOWERCASE_EXT)) {
	        extUrl = mpsJsonObj.getString(PushConstants.KEY_LOWERCASE_EXT);
	    }
		if(extUrl.contains("|")) {
			String[] parts = extUrl.split("\\|");
			pushKind = parts[0];
			linkUrl= parts[1];
			imgUrl = parts[2];
			Log.d("push", "extUrl = " + parts[0]);
		}else{
			linkUrl = extUrl;
		}
	
	    int icon = R.mipmap.ic_launcher;
	    //long when = System.currentTimeMillis();

	    String title = alertTitle;//BuildConfig.APP_NAME;//context.getString(R.string.app_name);
	    Intent notificationIntent = new Intent(context, PushMessageActivity.class);
	    notificationIntent.putExtra(PushConstants.KEY_JSON, rootJsonObj.toString());
	    notificationIntent.putExtra(PushConstants.KEY_PSID, psid);
	    notificationIntent.putExtra(PushConstants.KEY_TITLE, alertBody);//사용하지 않는듯
	    notificationIntent.putExtra(PushConstants.KEY_EXT, linkUrl);
	    notificationIntent.putExtra(PushConstants.KEY_PUSHTYPE, "GCM");
	    //notificationIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
		notificationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
		PendingIntent pendingIntent = PendingIntent.getActivity(context, Integer.parseInt(seqNo), notificationIntent,  PendingIntent.FLAG_CANCEL_CURRENT | PendingIntent.FLAG_IMMUTABLE);

		if(pushKind.equals("3")){
			String finalAlertBody = alertBody;
			Glide.with(context)
					.asBitmap()
					.load(imgUrl)
					.into(new CustomTarget<Bitmap>() {
						@Override
						public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
							NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
							Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
							NotificationCompat.Builder mBuilder = null;
							if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
								mBuilder = new NotificationCompat.Builder(context.getApplicationContext(), NOTIFICATION_CHANNEL_ID)
										.setAutoCancel(true)
										.setContentIntent(pendingIntent)
										.setSmallIcon(icon)
										.setContentTitle(title)
										.setContentText(finalAlertBody)
										.setPriority(NotificationCompat.PRIORITY_DEFAULT)
										.setTicker(title)
										.setStyle(new NotificationCompat.BigPictureStyle()
												.bigPicture(resource)
												.bigLargeIcon(null)
												.setSummaryText(finalAlertBody))
										.setLargeIcon(resource);
							}
							notificationManager.notify("gcm", Integer.parseInt(seqNo), mBuilder.build());

							if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O){
								int importance = NotificationManager.IMPORTANCE_HIGH;
								NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, name, importance);
								notificationChannel.setDescription("샘플 푸시 앱에 대한 채널 설명");
								notificationChannel.enableLights(true);
								notificationChannel.setLightColor(Color.RED);
								notificationChannel.enableVibration(true);
								notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
								assert notificationManager != null;
								//mBuilder.setChannelId(NOTIFICATION_CHANNEL_ID);
								notificationManager.createNotificationChannel(notificationChannel);
							}
							notificationManager.notify("gcm", Integer.parseInt(seqNo), mBuilder.build());
						}
						@Override
						public void onLoadCleared(@Nullable Drawable placeholder) {

						}
					});
		}else{
			NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
			Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

			NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context.getApplicationContext(), NOTIFICATION_CHANNEL_ID)
					.setAutoCancel(true)
					.setContentIntent(pendingIntent)
					.setSmallIcon(icon)
					.setContentTitle(title)
					.setContentText(alertBody)
					.setPriority(NotificationCompat.PRIORITY_DEFAULT)
					.setTicker(title)
					.setStyle(new NotificationCompat.BigTextStyle()
						.bigText(alertBody))
					.setSound(soundUri);

			if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O){
				int importance = NotificationManager.IMPORTANCE_HIGH;
				NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, name, importance);
				notificationChannel.setDescription("샘플 푸시 앱에 대한 채널 설명");
				notificationChannel.enableLights(true);
				notificationChannel.setLightColor(Color.RED);
				notificationChannel.enableVibration(true);
				notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
				assert notificationManager != null;
				//mBuilder.setChannelId(NOTIFICATION_CHANNEL_ID);
				notificationManager.createNotificationChannel(notificationChannel);
			}

			//mManager.notify("UPNS", seqno, mBuilder.build());
			//mBuilder.setDefaults(Notification.DEFAULT_VIBRATE);
			notificationManager.notify("gcm", Integer.parseInt(seqNo), mBuilder.build());
		}




		PushManager.getInstance().pushMessageReceiveConfirm(context, rootJsonObj.toString());
	}

	private static final String GROUP_GLOVIS = "Glovis";

	private static android.app.NotificationManager getManager(Context context) {
		return (android.app.NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
	}

	@TargetApi(Build.VERSION_CODES.O)
	public static void createChannel(Context context) {

		NotificationChannelGroup group1 = new NotificationChannelGroup(GROUP_GLOVIS, GROUP_GLOVIS);
		getManager(context).createNotificationChannelGroup(group1);


		NotificationChannel channelMessage = new NotificationChannel(Channel.MESSAGE,
				context.getString(R.string.notification_channel_message_title), android.app.NotificationManager.IMPORTANCE_DEFAULT);
		channelMessage.setDescription(context.getString(R.string.notification_channel_message_description));
		channelMessage.setGroup(GROUP_GLOVIS);
		channelMessage.setLightColor(Color.GREEN);
		channelMessage.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
		getManager(context).createNotificationChannel(channelMessage);

		NotificationChannel channelComment = new NotificationChannel(Channel.COMMENT,
				context.getString(R.string.notification_channel_comment_title), android.app.NotificationManager.IMPORTANCE_DEFAULT);
		channelComment.setDescription(context.getString(R.string.notification_channel_comment_description));
		channelComment.setGroup(GROUP_GLOVIS);
		channelComment.setLightColor(Color.BLUE);
		channelComment.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
		getManager(context).createNotificationChannel(channelComment);

		NotificationChannel channelNotice = new NotificationChannel(Channel.NOTICE,
				context.getString(R.string.notification_channel_notice_title), android.app.NotificationManager.IMPORTANCE_HIGH);
		channelNotice.setDescription(context.getString(R.string.notification_channel_notice_description));
		channelNotice.setGroup(GROUP_GLOVIS);
		channelNotice.setLightColor(Color.RED);
		channelNotice.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
		Log.d(TAG, "createChannel()");
		getManager(context).createNotificationChannel(channelNotice);

		SharedPreferences pref = context.getSharedPreferences(Common.PREFERENCE, context.MODE_PRIVATE);
		SharedPreferences.Editor editor = pref.edit();
		editor.putBoolean(Common.CREATE_NOTIFICATION_CHANNEL, true);
		editor.commit();
	}

	public static void sendNotification(Context context, int id, @Channel String channel, String title, String body, PendingIntent pi) {

		Notification.Builder builder = null;
		if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
			Log.d(TAG, "sendNotification() => BigTextStyle : " + body);
			builder = new Notification.Builder(context, channel)
					.setContentTitle(title)
					.setContentText(body)
					// 더 많은 내용이라서 일부만 보여줘야 하는 경우 아래 주석을 제거하면 setContentText에 있는 문자열 대신 아래 문자열을 보여줌
					.setStyle( new Notification.BigTextStyle().bigText(body) )
					//.setSmallIcon(getSmallIcon())
					.setAutoCancel(true);
		}else{
			Log.d(TAG, "sendNotification() => NoBigTextStyle : " + body);
		}

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
			builder.setSmallIcon(R.drawable.ic_launcher);	// mipmap 사용시 Oreo 이상에서 시스템 UI 에러남
		else
			builder.setSmallIcon(R.mipmap.ic_launcher);		// Oreo 이하에서 mipmap 사용하지 않으면 Couldn't create icon: StatusBarIcon 에러남

		if(pi != null){
			builder.setContentIntent(pi);
		}
		Log.d(TAG, "sendNotification() => body : " + body);
		getManager(context).notify(id, builder.build());
	}

	@Retention(RetentionPolicy.SOURCE)
	@StringDef({
			Channel.MESSAGE,
			Channel.COMMENT,
			Channel.NOTICE
	})
	public @interface Channel {
		String MESSAGE = "message";
		String COMMENT = "comment";
		String NOTICE = "notice";
	}
	
	/*@Deprecated
	public static void showPopupDialog(final Context context, final Intent intent, String pushType) throws Exception {
		String jsonData = intent.getExtras().getString("message");
		String psid = intent.getExtras().getString("psid");
		JSONObject jsonMsg = new JSONObject(jsonData);
		
		String extData = "";
		if ("UPNS".equals(pushType)) {
			if (jsonMsg.has("EXT")) {
				extData = jsonMsg.getString("EXT");
		    }
		}
		else {
			JSONObject mps = jsonMsg.getJSONObject("mps");
		    if (mps.has("ext")) {
		    	extData = mps.getString("ext");
		    }
		}
		
		Intent popupIntent = new Intent(); 
		popupIntent.setClass(context, ShowPushPopup.class);
		popupIntent.setAction(ShowPushPopup.class.getName());
		popupIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		popupIntent.setAction(ShowPushPopup.class.getName());
		popupIntent.putExtra("TITLE", "push Notification");
		popupIntent.putExtra("MESSAGE", "message");
		popupIntent.putExtra("JSON", jsonData);
		popupIntent.putExtra("PS_ID", psid);
		popupIntent.putExtra("EXT", extData);
		popupIntent.putExtra("PUSH_TYPE", pushType);
	    context.startActivity(popupIntent);
		
	}*/
	
	/*@Deprecated
	public static void showToast(final Context context, final String message) throws Exception {
		Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
	}*/
	
	/*@Deprecated
	public static AlertDialog createAlertDialog(final Context context, final String title, final String message, final Handler handler) {
		return new AlertDialog.Builder(context)
        .setIcon(android.R.drawable.ic_dialog_alert)
        .setTitle(title)
        .setMessage(message)
        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
	        @Override
	        public void onClick(DialogInterface dialog, int which) {
	        	if (handler != null) {
					Message msg = handler.obtainMessage(Dialog.BUTTON_POSITIVE, 0, 0, 0);
					handler.sendMessage(msg);
					
					dialog.cancel();
					dialog.dismiss();
				}   
	        }
	    })
	    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
	        @Override
	        public void onClick(DialogInterface dialog, int which) {
	        	if (handler != null) {
					Message msg = handler.obtainMessage(Dialog.BUTTON_NEGATIVE, 0, 0, 0);
					handler.sendMessage(msg);
					
					dialog.dismiss();
				}    
	        }
	    })
	    .create();
	}
	
	@Deprecated
	public static AlertDialog createConfirmDialog(final Context context, final String title, final String message, final Handler handler) {
		return new AlertDialog.Builder(context)
        .setIcon(android.R.drawable.ic_dialog_alert)
        .setTitle(title)
        .setMessage(message)
        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
	        @Override
	        public void onClick(DialogInterface dialog, int which) {
	        	if (handler != null) {
					Message msg = handler.obtainMessage(Dialog.BUTTON_POSITIVE, 0, 0, 0);
					handler.sendMessage(msg);
					
					dialog.cancel();
					dialog.dismiss();
				}   
	        }
	    })
	    .create();
	}*/
}
