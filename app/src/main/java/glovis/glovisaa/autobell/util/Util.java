package glovis.glovisaa.autobell.util;

import android.Manifest;
import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.media.Image;
import android.media.ThumbnailUtils;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Comparator;

import glovis.glovisaa.autobell.data.ShootGuide;

public class Util {

    public static int getScreenWidth() {
        return Resources.getSystem().getDisplayMetrics().widthPixels;
    }

    public static int getScreenHeight() {
        return Resources.getSystem().getDisplayMetrics().heightPixels;
    }

    public static String getPhotoFilePath() {
        return Environment.getExternalStorageDirectory().getPath() + File.separator + Common.PHOTO_FILE_PATH + File.separator;
    }

    public static String getFilePathInDCIM() {
        return Environment.getExternalStorageDirectory().getPath() + File.separator + Common.DCIM_FILE_PATH + File.separator + Common.PHOTO_FILE_PATH;
    }

    public static Bitmap getThumbnailFromPath(Context context, String path, int width, int height) {

        File imgFile = new File(path);
        if (imgFile.exists()) {
            // 썸네일 추출
            Bitmap thumbnail = ThumbnailUtils.extractThumbnail(BitmapFactory.decodeFile(path), width, height);
            // bitmap 타입을 drawable로 변경
            //Drawable drThumb = new BitmapDrawable(context.getResources(), thumbnail);
            return thumbnail;
        }else {
            Log.e("Util", path + ": Image File Not Exist");
            return null;
        }
    }

    // ImageView 에서 Bitmap 추출하기
    public static Bitmap getBitmapFromImageView(ImageView iv) {
        Drawable d = iv.getDrawable();
        Bitmap bitmap = ((BitmapDrawable)d).getBitmap();
        return bitmap;
    }

    // Bitmap 이미지 합성
    public static Bitmap overlayMark(Bitmap bmp1, Bitmap bmp2, int distanceLeft, int distanceTop) {
        Bitmap bmOverlay = Bitmap.createBitmap(bmp1.getWidth(), bmp1.getHeight(), bmp1.getConfig());
        Canvas canvas = new Canvas(bmOverlay);
        canvas.drawBitmap(bmp1, 0, 0, null);
        canvas.drawBitmap(bmp2, distanceLeft, distanceTop, null);
        return bmOverlay;
    }

    // 가로길이를 맞춰 이미지 크기를 조절해주기
    public static Bitmap resizeBitmap(Bitmap original, int resizeWidth) {

        double aspectRatio = (double) original.getHeight() / (double) original.getWidth();
        int targetHeight = (int) (resizeWidth * aspectRatio);
        Bitmap result = Bitmap.createScaledBitmap(original, resizeWidth, targetHeight, false);
        if (result != original) {
            original.recycle();
        }
        return result;
    }

    public static byte[] bitmapToByteArray( Bitmap bitmap ) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress( Bitmap.CompressFormat.JPEG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }

    public static Bitmap getBitmapFromImage(Image image) {
        Image localImage = image;
        ByteBuffer buffer = localImage.getPlanes()[0].getBuffer();
        byte[] bytes = new byte[buffer.capacity()];
        buffer.get(bytes);
        Bitmap bitmap = BitmapFactory.decodeByteArray(bytes,0,bytes.length,null);
        return bitmap;
    }

    public static void writeExifTag(String image_location) {

        try {
            ExifInterface exif = null;
            exif = new ExifInterface(image_location);
            exif.setAttribute(ExifInterface.TAG_COPYRIGHT, "Polarium");
            //exif.setAttribute(ExifInterface.TAG_MODEL, "Model Name@");
            exif.setAttribute("PrayTag", "Pray Tag Value");
            exif.saveAttributes();
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("Util", "writeExifTag : " + e.toString());
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.Q)
    public static void writeExifTag(InputStream is) {

        if (is != null) {
            try {
                ExifInterface exif = null;
                exif = new ExifInterface(is);
                exif.setAttribute(ExifInterface.TAG_COPYRIGHT, "Polarium");
                //exif.setAttribute(ExifInterface.TAG_MODEL, "Model Name@");
                //exif.setAttribute("PrayTag", "Pray Tag Value");
                exif.saveAttributes();
            } catch(IOException e){
                e.printStackTrace();
                Log.e("Util", "writeExifTag : " + e.toString());
            }
        }
    }

    /*
     * Input: URI -- something like content://com.example.app.provider/table2/dataset1
     * Output: PATH -- something like /sdcard/DCIM/123242-image.jpg
     */
    public static String convertMediaUriToPath(Context context, Uri uri) {
        String [] proj={MediaStore.Images.Media.DATA};
        Cursor cursor = context.getContentResolver().query(uri, proj,  null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String path = cursor.getString(column_index);
        cursor.close();
        return path;
    }

    //오버레이 권한 확인
    //마시멜로 이상부터만 가능
    public static boolean checkDrawOverlayPermission(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (Settings.canDrawOverlays(context)) {
                return true;
            } else {
                return false;
            }
        }
        else{
            return true;
        }
    }

    public static class DrawUrlImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView ivSample;

        public DrawUrlImageTask(ImageView ivSample) {
            this.ivSample = ivSample;
        }

        protected Bitmap doInBackground(String... urls) {
            String url = urls[0];
            Bitmap bitmap = null;
            InputStream in = null;

            try {
                in = new java.net.URL(url).openStream();
                bitmap = BitmapFactory.decodeStream(in);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            finally {
                try {
                    in.close();
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }

            return bitmap;
        }

        protected void onPostExecute(Bitmap bitmap) {
            ivSample.setImageBitmap(bitmap);
        }
    }

    // 네트웍 연결 체크
    public static boolean checkNetworkConnection(Context context) {
        ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
        return isConnected;
    }

    // 루팅 여부 체크
    public static boolean isRooted() {

        boolean runtimeFlag = false;

        try{
            Runtime.getRuntime().exec("su");
            runtimeFlag = true;
        }catch(Exception e){
            runtimeFlag = false;
        }

        return runtimeFlag;
    }

    public static boolean checkRooting(){
        final String[] files = {
                "/sbin/su", "/system/su", "/system/bin/su", "/system/sbin/su", "/system/xbin/su", "/system/xbin/mu",
                "/system/bin/.ext/.su", "/system/usr/su-backup", "/data/data/com.noshufou.android.su", "/system/app/Superuser.apk",
                "/system/app/su.apk", "/system/bin/.ext", "/system/xbin/.ext", "/data/local/xbin/su", "/data/local/bin/su",
                "/system/sd/xbin/su", "/system/bin/failsafe/su", "/data/local/su", "/su/bin/su"};

        String buildTags = Build.TAGS;

        if (buildTags != null && buildTags.contains("test-keys")){
            return true;
        }

        for (String path : files){
            if (new File(path).exists()){
                return true;
            }
        }

        try {
            Runtime.getRuntime().exec("su");
            return true;
        }
        catch (Exception e){
            return false;
        }

    }

    // 가이드 순번으로 정렬
    public final static Comparator<ShootGuide> sortByGuideSortNo = new Comparator<ShootGuide>() {
        @Override
        public int compare(ShootGuide object1, ShootGuide object2) {
            return Integer.compare(object1.getGuide_sort_no(), object2.getGuide_sort_no());
        }
    };

    // Activity 실행 여부 확인
    public static boolean isMyActivityRunning(Context context, Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    // 핸드폰 번호 가져오기
    public static String getMyPhoneNumber(Context context) {
        String telNumber = "";
        TelephonyManager mTelephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        if (mTelephonyManager != null) {
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(context, "전화 권한이 허용되지 않았습니다.", Toast.LENGTH_SHORT).show();
            }
            else if (true == usimCheck(context)) {
                telNumber = mTelephonyManager.getLine1Number();
                telNumber = telNumber.replace("+82", "0");
            }
        }
        return telNumber;
    }

    private static boolean usimCheck(Context context) {
        boolean bUsimCheck = false;
        TelephonyManager mTelephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        if (mTelephonyManager == null) {
            Toast.makeText(context, "전화번호를 가져올 수 없습니다.", Toast.LENGTH_LONG).show();
        } else {
            try {
                int simState = mTelephonyManager.getSimState();
                switch (simState) {
                    // 유심 상태를 알 수 없는 경우
                    case TelephonyManager.SIM_STATE_UNKNOWN:
                    // 유심이 없는 경우
                    case TelephonyManager.SIM_STATE_ABSENT:
                    // 유심 오류, 영구적인 사용 중지 상태
                    case TelephonyManager.SIM_STATE_PERM_DISABLED:
                    // 유심이 있지만 오류 상태인 경우
                    case TelephonyManager.SIM_STATE_CARD_IO_ERROR:
                    // 유심이 있지만 통신사 제한으로 사용 불가
                    case TelephonyManager.SIM_STATE_CARD_RESTRICTED:
                        Toast.makeText(context, "단말기의 유심이 존재하지 않거나 오류가 있는 경우, 앱을 사용할 수 없습니다.", Toast.LENGTH_LONG).show();
                        bUsimCheck = false;
                        break;
                    default :
                        Log.d("simCheck", "Success");
                        bUsimCheck = true;
                        break;
                }
            } catch (Exception e) {
                Toast.makeText(context, "단말기의 정보를 가져오는 중 오류가 발생했습니다. 잠시 후 다시 시도해주세요.", Toast.LENGTH_LONG).show();
                Log.e("simCheck", "Exception: " + e.toString());
                bUsimCheck = false;
                return bUsimCheck;
            }
        }
        return bUsimCheck;
    }
}
