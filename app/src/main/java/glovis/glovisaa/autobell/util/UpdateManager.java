package glovis.glovisaa.autobell.util;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;

import java.util.concurrent.Executor;

import glovis.glovisaa.autobell.MainActivity;

public class UpdateManager {

    public static void versionCheck(Activity context) {
        FirebaseRemoteConfig mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .setMinimumFetchIntervalInSeconds(600)
                .build();
        mFirebaseRemoteConfig.setConfigSettingsAsync(configSettings);

        mFirebaseRemoteConfig.fetchAndActivate()
                .addOnCompleteListener(context, new OnCompleteListener<Boolean>() {
                    @Override
                    public void onComplete(@NonNull Task<Boolean> task) {
                        if (task.isSuccessful()) {
                            boolean updated = task.getResult();
                            task.getResult();
                            String latest_version_android = mFirebaseRemoteConfig.getString("latest_version_android");
                            String update_message = mFirebaseRemoteConfig.getString("update_message");
                            String update_force_android = mFirebaseRemoteConfig.getString("update_force_android");

                            try {
                                String versionName = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
                                Log.d("version", versionName);
                                if(!getVersionCheck(versionName,latest_version_android)){
                                    if (update_force_android.equals("1")) {
                                        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                        builder.setMessage(update_message);
                                        builder.setPositiveButton("확인", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                Intent intent = new Intent(Intent.ACTION_VIEW);
                                                intent.addCategory(Intent.CATEGORY_DEFAULT);
                                                intent.setData(Uri.parse("market://details?id="+context.getPackageName()));
                                                context.startActivity(intent);
                                                context.finish();
                                            }
                                        });
                                        builder.setCancelable(false);
                                        final AlertDialog dialog = builder.create();
                                        dialog.show();
                                    }else{
                                        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                        builder.setMessage(update_message);
                                        builder.setPositiveButton("확인", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                Intent intent = new Intent(Intent.ACTION_VIEW);
                                                intent.addCategory(Intent.CATEGORY_DEFAULT);
                                                intent.setData(Uri.parse("market://details?id="+context.getPackageName()));
                                                context.startActivity(intent);
                                            }
                                        });
                                        builder.setNegativeButton("취소", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                            }
                                        });
                                        builder.setCancelable(true);
                                        final AlertDialog dialog = builder.create();
                                        dialog.show();
                                    }
                                }
                            } catch (PackageManager.NameNotFoundException e) {
                                e.printStackTrace();
                            }
                        } else {

                        }
                    }
                });

        mFirebaseRemoteConfig.fetch(600).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                String latest_version_android = mFirebaseRemoteConfig.getString("latest_version_android");
                String update_message = mFirebaseRemoteConfig.getString("update_message");
                String update_force_android = mFirebaseRemoteConfig.getString("update_force_android");
            }
        });
    }

    public static boolean getVersionCheck(String appVer, String compareVer){

        // 각각의 버전을 split을 통해 String배열에 담습니다.
        String[] appVerArray = new String[]{};
        if(!"".equals(appVer) && appVer != null ){
            appVerArray = appVer.split("\\.");
        }

        String[] compareVerArray = new String[]{};
        if(!"".equals(compareVer) && compareVer != null ){
            compareVerArray = compareVer.split("\\.");
        }

        // 비교할 버전이 없을 경우 false;
        if(appVerArray.length == 0 || compareVerArray.length == 0) return false;

        // 비교할 버전들 중 버전 길이가 가장 작은 버전을 구함
        int minLength = appVerArray.length;
        if(minLength > compareVerArray.length){
            minLength = compareVerArray.length;
        }

    /* 반복을 통해 A B C 순으로 비교하며 기존의 버전이 비교할 버전보다
       크거나 같으면 true, 작을 경우 false를 리턴합니다. */
        for(int i=0; i<minLength; i++){
            int appVerSplit = Integer.parseInt(appVerArray[i]);
            int compareVerSplit = Integer.parseInt(compareVerArray[i]);
            if(appVerSplit > compareVerSplit){
                return true;
            }else if(appVerSplit == compareVerSplit){
                continue;
            }else if(appVerSplit < compareVerSplit){
                return false;
            }
        }

        return true;
    }
}
