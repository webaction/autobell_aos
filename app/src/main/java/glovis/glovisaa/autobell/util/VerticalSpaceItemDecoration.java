package glovis.glovisaa.autobell.util;

import android.graphics.Rect;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;

public class VerticalSpaceItemDecoration extends RecyclerView.ItemDecoration {
    private final int verticalSpaceHeight;

    public VerticalSpaceItemDecoration(int paramInt) { this.verticalSpaceHeight = paramInt; }

    public void getItemOffsets(Rect paramRect, View paramView, RecyclerView paramRecyclerView, RecyclerView.State paramState) {
        if (paramRecyclerView.getChildAdapterPosition(paramView) != paramRecyclerView.getAdapter().getItemCount() - 1)
            paramRect.bottom = this.verticalSpaceHeight;
    }
}
