package glovis.glovisaa.autobell.util;

import glovis.glovisaa.autobell.BuildConfig;

import okhttp3.MediaType;

public class Common {

    public static final String COMPANY          = "POLA_R_";                                        /* 로그값 구분을 위한 회사명 */
//    public static final String MOBILE_WEB_URL   = "https://test.autobell.co.kr";                /* 모바일 웹 메인 URL (테스트 운영(외부접속 가능)*/
    public static final String MOBILE_WEB_URL   = BuildConfig.BASE_URL;                         /* 모바일 웹 메인 URL (운영(외부접속 가능)*/
//    public static final String MOBILE_WEB_URL   = "https://www.chatbook.co.kr/test/polarium/autobell_test5.html";

//    public static final String MOBILE_WEB_URL   = "http://10.47.212.73";
//    public static final String MOBILE_WEB_URL   = "http://10.6.58.32";                            /* 모바일 웹 메인 URL (개발서버) */
    //public static final String MOBILE_WEB_URL   = "http://10.25.44.90";                           /* 모바일 웹 메인 URL (개인 테스트(강석남)) */
    //public static final String MOBILE_WEB_URL   = "http://10.25.44.82";                           /* 모바일 웹 메인 URL (개인 테스트(성연욱)) */
    public static final String MOBILE_LOGIN_URL = "/login";                                         /* 모바일 웹 로그인 URL */
    public static final String MOBILE_MAIN_URL = "/main";                                           /* 모바일 웹 메인 URL */
    public static final String MOBILE_UPLOAD_URL = "/api/gl/thrid/img/prdImgUpload.do";             /* 모바일 웹 사진 업로드 URL */

    // 마켓 배포시에는 꼭 MNG로 설정하기 바랍니다.
    public static final String RELEASE_TYPE = "MNG"; // MNG:m, DEV
    public static final String JAVASCRIP_INTERFACE_NAME = "JSInterface";

    public static final int MENU_TYPE_DETAIL_SHOOT            = 0;                                  /* 상세 촬영 차량 */
    public static final int MENU_TYPE_PASS_EVALUATION_SHOOT   = 1;                                  /* 무평가 촬영 차량 */
    public static final int MENU_TYPE_OVERALL_SHOOT           = 2;                                  /* 360도 전면적 촬영 차량 */

    public static final int CAR_TYPE_LIGHT                    = 0;                                  /* 차량 타입 - 경차 */
    public static final int CAR_TYPE_SEDAN                    = 1;                                  /* 차량 타입 - 세단 */
    public static final int CAR_TYPE_SUV                      = 2;                                  /* 차량 타입 - SUV */
    public static final int CAR_TYPE_RV                       = 3;                                  /* 차량 타입 - RV */

    public static final int SHOOT_CONTINUOUS           = 1;                                         /* 연속 촬영 */
    public static final int SHOOT_MANUAL               = 2;                                         /* 수동 촬영 */

    public static final String PARAM_UUID               = "uuid";                                   /* 유저 ID */
    public static final String PARAM_USERID             = "userId";                                 /* 유저 ID */
    public static final String PARAM_CARID              = "crId";                                   /* 차량 매물 ID */
    public static final String PARAM_PHCODE             = "phCode";                                 /* 사진 위치 코드 */
    public static final String PARAM_PHTYPE             = "phType";                                 /* 촬영 타입(일반/360) */
    public static final String PARAM_FILE               = "file";                                   /* 사진 파일 */

    public static final MediaType JSON                  = MediaType.get("application/json; charset=utf-8");
    public static final MediaType TEXT_PLAIN            = MediaType.get("text/plain; charset=utf-8");
    public static final String CONTENT_TYPE_MULTIPART   = "multipart/form-data";

    public static final int DETAIL_SHOOT_NEW        = 0;                                            /* 상세 촬영 차량 목록 신규 상태 */
    public static final int DETAIL_SHOOT_PROGRESS   = 1;                                            /* 상세 촬영 차량 목록 촬영중 상태 */
    public static final int DETAIL_SHOOT_COMPLETE   = 2;                                            /* 상세 촬영 차량 목록 촬영완료(업로드 전) 상태 */
    public static final int DETAIL_SHOOT_UPLOAD     = 3;                                            /* 상세 촬영 차량 목록 업로드 완료 상태 */
    public static final int DETAIL_SHOOT_FROM_SERVER= 4;                                            /* 상세 촬영 차량 목록 서버에 업로드된 상태 */

    public static final int DETAIL_PHOTO_MAX        = 10;                                           /* 상세 촬영 전체 필요 장수 */

    public static final int DETAIL_SHOOT_GUIDE_OUTSIDE  = 0;                                        /* 상세 촬영 외부 가이드 구분 값 */
    public static final int DETAIL_SHOOT_GUIDE_INSIDE   = 1;                                        /* 상세 촬영 내부 가이드 구분 값 */
    public static final int PASS_EVALUATION_SHOOT_GUIDE = 2;                                        /* 무평가 촬영 가이드 구분 값 */
    public static final int OVERALL_SHOOT_GUIDE         = 3;                                        /* 360도 전면적 촬영 가이드 구분 값 */

    public static final int OVERALL_SHOOT_NUM          = 12;                                        /* 360도 촬영 장 수 */
    public static final int NO_USER_BLIND              = 0;                                         /* 무평가 촬영 가리기 도구 미사용 */
    public static final int USE_BLIND                  = 1;                                         /* 무평가 촬영 가리기 도구   사용 */

    public static final String DB_NAME      = "assistant_data";                                     /* 앱 내부 DB File Name */
    public static final String TABLE_CAR    = "tb_car";                                             /* 등록 차량 DB Table */
    public static final String TABLE_GUIDE  = "tb_guide";                                           /* 촬영 가이드 DB Table */
    public static final String TABLE_PHOTO  = "tb_photo";                                           /* 촬영 사진 DB Table */

    public static final String PHOTO_FILE_PATH      = "images";                                     /* 사진 파일 저장 폴더 (sdcard 아래) */
    public static final String DCIM_FILE_PATH       = "DCIM";
    public static final String PHOTO_FILE_EXTENSION = ".jpg";                                       /* 사진 파일 확장자 */

    public static final String PREFERENCE = "glovis.autobell.sharedpreference";                     /* Shared Preference Key */
    public static final String CREATE_NOTIFICATION_CHANNEL = "CREATE_NOTIFICATION_CHANNEL";         /* Notification Channel 생성 여부 확인 */
    public static final String DONT_SHOT_DETAIL_USING_GUIDE = "DETAIL_USING_GUIDE";                 /* 상세 촬영 사용 가이드 보지 않기 키값 */
    public static final String DONT_SHOT_OVERALL_USING_GUIDE = "OVERALL_USING_GUIDE";               /* 360 촬영 사용 가이드 보지 않기 키값 */
    public static final String DONT_SHOT_OVERALL_CONFIRM_GUIDE = "OVERALL_CONFIRM_GUIDE";           /* 360 촬영 확인 가이드 보지 않기 키값 */

    public static final int REQ_SHOOT_ACT                   = 3000;                                 /* 촬영 Activity 요청 코드 */

    public static final int USER_TYPE_CUSTOMER              = 0;                                    /* 사용자 코드 */
    public static final int USER_TYPE_DEALER                = 1;                                    /* 딜러 코드 */
    public static final int USER_TYPE_EVALUATOR             = 2;                                    /* 평가사 코드 */

    public static final int GUIDE_SORT_FRONT_SIDE           = 1;                                    /* 상세 촬영 가이드 Sort No */
    public static final int GUIDE_SORT_BACK_SIDE            = 2;
    public static final int GUIDE_SORT_FRONT                = 3;
    public static final int GUIDE_SORT_BACK                 = 4;
    public static final int GUIDE_SORT_INSTRUMENT_BOARD     = 5;
    public static final int GUIDE_SORT_WHELL                = 6;
    public static final int GUIDE_SORT_BONNET               = 7;
    public static final int GUIDE_SORT_DRIVERS_SEAT         = 8;
    public static final int GUIDE_SORT_DRIVERS_BACK_SEAT    = 9;
    public static final int GUIDE_SORT_PASSENGER_SEAT       = 10;
    public static final int GUIDE_SORT_PASSENGER_BACK_SEAT  = 11;
    public static final int GUIDE_SORT_DASHBOARD            = 12;
    public static final int GUIDE_SORT_CENTERFASCIA         = 13;
    public static final int GUIDE_SORT_OVERHEAD_CONSOLE     = 14;
    public static final int GUIDE_SORT_TOP                  = 15;
    public static final int GUIDE_SORT_TRUNK                = 16;
    public static final int GUIDE_SORT_GEARBOX              = 17;
    public static final int GUIDE_SORT_BACK_CAMERA          = 18;
    public static final int GUIDE_SORT_SCTATCHSHOOT         = 19;
    public static final int GUIDE_SORT_SCTATCHSHOOT2        = 20;
    //public static final int GUIDE_SORT_LEFT_SIDE            = 5;
    //public static final int GUIDE_SORT_RIGHT_SIDE           = 6;

    public static final String PASS_EVALUATION_GUIDE_CODE_FRONT    = "PASS_FR";                     /* 무평가 전면 촬영 가이드 코드 */
    public static final String PASS_EVALUATION_GUIDE_CODE_BOARD    = "PASS_BD";                     /* 무평가 대쉬보드 촬영 가이드 코드 */
    public static final String PASS_EVALUATION_GUIDE_CODE_LICENSE  = "PASS_LC";                     /* 무평가 차량등록증 촬영 가이드 코드 */
}
