package glovis.glovisaa.autobell.util;

import android.util.Log;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class FileUploadUtils {

    public static void imageUploadToServer(ProgressRequestBody fileBody, String fileName, String userId, String carId, String phCode, String phType) {

        RequestBody requestBody = new MultipartBody.Builder()
            .setType(MultipartBody.FORM)
            .addFormDataPart(Common.PARAM_USERID, userId)
            .addFormDataPart(Common.PARAM_CARID, carId)
            .addFormDataPart(Common.PARAM_PHCODE, phCode)
            .addFormDataPart(Common.PARAM_PHTYPE, phType)
            .addFormDataPart(Common.PARAM_FILE, fileName, fileBody)
            .build();

        Request request = new Request.Builder()
            .url(Common.MOBILE_WEB_URL + Common.MOBILE_UPLOAD_URL) // Server URL
            .post(requestBody)
            .build();

        OkHttpClient client = new OkHttpClient();
        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                Log.d("TEST : ", response.body().string());
            }
        });
    }
}