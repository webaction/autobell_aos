package glovis.glovisaa.autobell;

import android.Manifest;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.os.PowerManager;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.JsResult;
import android.webkit.SslErrorHandler;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import glovis.glovisaa.autobell.bridge.AndroidBridge;
import glovis.glovisaa.autobell.db.GuideDBHelper;
import glovis.glovisaa.autobell.dialog.CustomDialog;
import glovis.glovisaa.autobell.notification.PushNotificationManager;
import glovis.glovisaa.autobell.util.Common;
import glovis.glovisaa.autobell.util.UpdateManager;
import glovis.glovisaa.autobell.util.Util;
import m.client.push.library.PushManager;
import m.client.push.library.common.Logger;
import m.client.push.library.common.PushConstants;
import m.client.push.library.common.PushConstantsEx;
import m.client.push.library.common.PushLog;
import m.client.push.library.utils.PushUtils;

/**
 * 설명 : 메인화면 (권환 획득, 웹뷰 준비, 푸시 설정)
 * @fileOverview 메인화면
 * @author 나기도
 */
public class MainActivity extends AppCompatActivity {

    private static final String TAG = Common.COMPANY + "MainActivity";
    private BroadcastReceiver mMainBroadcastReceiver;
    static final int REQUEST_CAMERA = 100;
    private String mOhMyCallURL = "";
    private String PushURL = "";
    private static final String  GOOGLE_PLAY_STORE_PREFIX = "market://details?id=";

    @BindView(R.id.webview)
    WebView mWebview;
    @BindView(R.id.web_progress)
    ProgressBar mWebProgress;
    public Dialog mDialog;

    android.webkit.CookieManager mCookieManager;

    private final static int FILECHOOSER_RESULTCODE = 5000;
    private final static int ACTION_VIEW_RESULTCODE = 6000;

    private ValueCallback<Uri[]> mUploadMessage;
    private Uri cameraImageUri = null;
    ArrayList<WebView> arr_views = new ArrayList<WebView>();

    // DEV ////////////////////////////
    @BindView(R.id.lyLinkBody)
    LinearLayout lyLinkBody;
    @BindView(R.id.edtUrl)
    EditText edtUrl;
    @BindView(R.id.btnClear)
    Button btnClear;
    @BindView(R.id.btnGo)
    Button btnGo;
    @BindView(R.id.btnChangeView)
    Button btnChangeView;
    @BindView(R.id.fl_webview)
    FrameLayout fl_webview;
    // DEV ////////////////////////////

    @BindView(R.id.btn_detailshoot_light)
    Button btn_detailshoot_light;
    @BindView(R.id.btn_detailshoot_sedan)
    Button btn_detailshoot_sedan;
    @BindView(R.id.btn_detailshoot_suv)
    Button btn_detailshoot_suv;
    @BindView(R.id.btn_detailshoot_rv)
    Button btn_detailshoot_rv;
    @BindView(R.id.btn_360pic_light)
    Button btn_360pic_light;
    @BindView(R.id.btn_360pic_sedan)
    Button btn_360pic_sedan;
    @BindView(R.id.btn_360pic_suv)
    Button btn_360pic_suv;
    @BindView(R.id.btn_360pic_rv)
    Button btn_360pic_rv;

    @BindView(R.id.btn_register_push)
    Button btn_registerServiceAndUser;
    @BindView(R.id.btn_unregister_push)
    Button btn_unregister_push;
    @BindView(R.id.btn_send_push_msg)
    Button btn_send_push_msg;
    @BindView(R.id.rl_loading)
    RelativeLayout rl_loading;

    Handler handlerLoading = new Handler();
    Runnable runnableLoading = new Runnable() {
        @Override
        public void run() {
            rl_loading.setVisibility(View.GONE);
        }
    };

    private CustomDialog mExitDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate()");
        setContentView(R.layout.activity_main);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            WebView.setWebContentsDebuggingEnabled(true);
        }
        ButterKnife.bind(this);

        // 딥 링크 데이터 추출
        Intent intent = getIntent();
        Uri data = intent.getData();
        if (data != null) {
            String scheme = data.getScheme(); // 스킴 추출
            String host = data.getHost(); // 호스트 추출
            String path = data.getPath(); // 경로 추출
            String paramUrl = data.getQueryParameter("url");

            // 딥 링크 데이터 사용
            // 여기에서 딥 링크에 따른 특정 동작을 수행할 수 있습니다.
            SharedPreferences pref = getSharedPreferences(Common.PREFERENCE, MODE_PRIVATE);
            SharedPreferences.Editor editor = pref.edit();
            editor.putString("start_url", paramUrl.toString());
            editor.commit();
            editor.apply();
        }


        // Device 루팅 체크
        /*if (Util.isRooted()) {

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setMessage("루트권한을 가진 디바이스에서는 실행할 수 없습니다.")
                .setCancelable(false)
                .setPositiveButton("확인", new DialogInterface.OnClickListener() {
                    public void onClick(
                        DialogInterface dialog, int id) {
                            moveTaskToBack(true);
                            finish();
                            android.os.Process.killProcess(android.os.Process.myPid());
                        }
                });

            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
            return;
        }*/
        checkApp();
        FirebaseDynamicLinks.getInstance()
                .getDynamicLink(getIntent())
                .addOnSuccessListener(this, new OnSuccessListener<PendingDynamicLinkData>() {
                    @Override
                    public void onSuccess(PendingDynamicLinkData pendingDynamicLinkData) {
                        // Get deep link from result (may be null if no link is found)
                        Uri deepLink = null;
                        if (pendingDynamicLinkData != null) {
                            deepLink = pendingDynamicLinkData.getLink();
                            SharedPreferences pref = getSharedPreferences(Common.PREFERENCE, MODE_PRIVATE);
                            SharedPreferences.Editor editor = pref.edit();
                            editor.putString("start_url", deepLink.toString());
                            editor.commit();
                            editor.apply();
                        }
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "getDynamicLink:onFailure", e);
                    }
                });

        setCookieAllow(mCookieManager, mWebview);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(this, Manifest.permission.WAKE_LOCK) != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(this, Manifest.permission.READ_MEDIA_IMAGES) != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(this, Manifest.permission.READ_MEDIA_VIDEO) != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(this, Manifest.permission.READ_MEDIA_AUDIO) != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(this, Manifest.permission.POST_NOTIFICATIONS) != PackageManager.PERMISSION_GRANTED) {
                // 권한이 없으면 권한을 요청한다.
                ActivityCompat.requestPermissions(this,
                        new String[]{ Manifest.permission.CAMERA,
                                Manifest.permission.WAKE_LOCK,
                                Manifest.permission.READ_PHONE_STATE,
                                Manifest.permission.READ_MEDIA_IMAGES,
                                Manifest.permission.READ_MEDIA_VIDEO,
                                Manifest.permission.READ_MEDIA_AUDIO,
                                Manifest.permission.POST_NOTIFICATIONS},
                        REQUEST_CAMERA);
            }else{
                settingStart();
            }
        }else{
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(this, Manifest.permission.WAKE_LOCK) != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(this, Manifest.permission.POST_NOTIFICATIONS) != PackageManager.PERMISSION_GRANTED) {
                // 권한이 없으면 권한을 요청한다.
                ActivityCompat.requestPermissions(this,
                        new String[]{ Manifest.permission.CAMERA,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.WAKE_LOCK,
                                Manifest.permission.READ_PHONE_STATE,
                                Manifest.permission.POST_NOTIFICATIONS},
                        REQUEST_CAMERA);
            }else{
                settingStart();
            }
        }
//        // 권한이 있을 경우에만 layout을 전개한다.
//        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
//                ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
//                ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
//                ContextCompat.checkSelfPermission(this, Manifest.permission.WAKE_LOCK) != PackageManager.PERMISSION_GRANTED ||
//                ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED ||
//                ContextCompat.checkSelfPermission(this, Manifest.permission.READ_MEDIA_IMAGES) != PackageManager.PERMISSION_GRANTED ||
//                ContextCompat.checkSelfPermission(this, Manifest.permission.READ_MEDIA_VIDEO) != PackageManager.PERMISSION_GRANTED ||
//                ContextCompat.checkSelfPermission(this, Manifest.permission.READ_MEDIA_AUDIO) != PackageManager.PERMISSION_GRANTED) {
//            // 권한이 없으면 권한을 요청한다.
//            ActivityCompat.requestPermissions(this,
//                    new String[]{ Manifest.permission.CAMERA,
//                                  Manifest.permission.WRITE_EXTERNAL_STORAGE,
//                                  Manifest.permission.READ_EXTERNAL_STORAGE,
//                                  Manifest.permission.WAKE_LOCK,
//                                  Manifest.permission.READ_PHONE_STATE,
//                                  Manifest.permission.READ_MEDIA_IMAGES,
//                                  Manifest.permission.READ_MEDIA_VIDEO,
//                                  Manifest.permission.READ_MEDIA_AUDIO},
//                    REQUEST_CAMERA);
//        } else {
//
//            //OREO API 26 이상에서는 채널 필요
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                SharedPreferences pref = getSharedPreferences(Common.PREFERENCE, MODE_PRIVATE);
//                boolean isCreateChannel = pref.getBoolean(Common.CREATE_NOTIFICATION_CHANNEL, false);  // Notification Channel 생성 여부 확인
//                if (false == isCreateChannel) {
//                    Log.d(TAG, "createChannel()");
//                    PushNotificationManager.createChannel(this);
//                }else {
//                    Log.d(TAG, "Already Channel created");
//                }
//            }
//
//            initView();
//
//            setGuideDBData();
//
//            // 오마이콜에서 호출한 경우
//            mOhMyCallURL = getIntent().getStringExtra("webpage_url");
//            // 오마이콜아닌 푸시에서 호출한경
//            PushURL = getIntent().getStringExtra("push_url");
//
//            initWebView();
//
//            // 다른 앱 위에 표시 허용
//            if ( false == Util.checkDrawOverlayPermission(getApplicationContext()) ) {
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                    startActivity(
//                            new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getPackageName()))
//                                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
//                    );
//                }
//            }
//
//            // 절전 모드 해제 권한 얻기
//            getIgnoreBatteryOptimizationPermission();
//
//            //앱 실행시점에서 호출 (4.0 )
//            if (PushUtils.checkNetwork(getApplicationContext())) {
//                PushManager.getInstance().initPushServer(getApplicationContext());
//            }
//            // Push service 및 User 등록 (앱 실행 시 마다 호출)
////            registerPushServiceAndUser();
//        }
        UpdateManager.versionCheck(this);
    }

    public void settingStart(){
        //OREO API 26 이상에서는 채널 필요
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            SharedPreferences pref = getSharedPreferences(Common.PREFERENCE, MODE_PRIVATE);
            boolean isCreateChannel = pref.getBoolean(Common.CREATE_NOTIFICATION_CHANNEL, false);  // Notification Channel 생성 여부 확인
            if (false == isCreateChannel) {
                Log.d(TAG, "createChannel()");
                PushNotificationManager.createChannel(this);
            }else {
                Log.d(TAG, "Already Channel created");
            }
        }

        initView();

        setGuideDBData();

        // 오마이콜에서 호출한 경우
        mOhMyCallURL = getIntent().getStringExtra("webpage_url");
        // 오마이콜아닌 푸시에서 호출한경
        PushURL = getIntent().getStringExtra("push_url");

        initWebView();

        // 다른 앱 위에 표시 허용
        if ( false == Util.checkDrawOverlayPermission(getApplicationContext()) ) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                startActivity(
                        new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getPackageName()))
                                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                );
            }
        }

        // 절전 모드 해제 권한 얻기
        getIgnoreBatteryOptimizationPermission();

        //앱 실행시점에서 호출 (4.0 )
        if (PushUtils.checkNetwork(getApplicationContext())) {
            PushManager.getInstance().initPushServer(getApplicationContext());
        }
    }

    //퍼미션 체크 분기 테스트 필요 SDK 33 이상 필요체크
//    public boolean checkPermission()
//    {
//        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//
//            String[] permissions = null;
//            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
//                permissions = new String[]{
//                        Manifest.permission.CAMERA,
//                        Manifest.permission.READ_MEDIA_IMAGES,
//                        Manifest.permission.READ_MEDIA_VIDEO,
//                        Manifest.permission.READ_MEDIA_AUDIO,
//                        Manifest.permission.POST_NOTIFICATIONS,
//                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
//                        Manifest.permission.READ_PHONE_STATE};
//            } else {
//                permissions = new String[]{
//                        Manifest.permission.CAMERA,
//                        Manifest.permission.WAKE_LOCK,
//                        Manifest.permission.READ_EXTERNAL_STORAGE,
//                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
//                        Manifest.permission.READ_PHONE_STATE};
//            }
//
//            List<String> denied_permissions = new ArrayList<String>();
//            for (String perm : permissions) {
//                if (ActivityCompat.checkSelfPermission(this, perm)
//                        != PackageManager.PERMISSION_GRANTED)
//                    denied_permissions.add(perm);
//            }
//
//            if(denied_permissions.size() > 0){
//                String [] deniedPerms = denied_permissions.toArray(new String[denied_permissions.size()]);
//                ActivityCompat.requestPermissions(this, deniedPerms, REQUEST_CAMERA);
//                return false;
//            }
//        }
//        return true;
//    }

    public void checkApp(){
//        String installer = getPackageManager().getInstallerPackageName(getPackageName());
//        if(!installer.startsWith("com.android.vending")){
//
//        }
        if(Util.checkRooting()){
            android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(this);
            alertDialog.setPositiveButton("확인", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
            alertDialog.setTitle("앱을 시작할 수 없습니다.");
            alertDialog.setMessage("앱이 위변조 되어 있거나 기기가 루팅되어 앱을 시작할 수 없습니다.");
            Dialog dialog = alertDialog.create();
            dialog.setCancelable(false);
            dialog.show();
        }
    }
    @Override
    public void onBackPressed() {
        Log.d(TAG, "onBackPressed()");
        if(mOhMyCallURL != null && mOhMyCallURL.length() > 1) {
            // 웹뷰에 첫 주소를 연결
            mWebview.loadUrl(Common.MOBILE_WEB_URL/* + Common.MOBILE_MAIN_URL*/);
            mOhMyCallURL = null;
            return;
        }
        if(arr_views.size() > 0){
            WebView view = arr_views.get(arr_views.size()-1);
            if(view.canGoBack()){
                view.goBack();
            }else{
                fl_webview.removeView(view);
                arr_views.remove(view);
            }
            return;
        }
        if(mWebview.getUrl().equals(Common.MOBILE_WEB_URL + Common.MOBILE_MAIN_URL) || mWebview.getUrl().equals(Common.MOBILE_WEB_URL + Common.MOBILE_LOGIN_URL)) {
            mExitDialog = new CustomDialog(this, "서비스를 종료하시겠습니까?", cancelExitListener, okExitListener);
            mExitDialog.show();
        }else {
            if(mWebview.canGoBack()) {
                mWebview.goBack();
            }else {
                //super.onBackPressed();
                mExitDialog = new CustomDialog(this, "서비스를 종료하시겠습니까?", cancelExitListener, okExitListener);
                mExitDialog.show();
            }
        }
    }

    public View.OnClickListener okExitListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            mExitDialog.dismiss();
            finish();
        }
    };

    public View.OnClickListener cancelExitListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            mExitDialog.dismiss();
        }
    };

    private void setCookieAllow(CookieManager cookieManager, WebView webView) {

        try {
            cookieManager = android.webkit.CookieManager.getInstance();
            cookieManager.setAcceptCookie(true);
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                webView.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
                cookieManager.setAcceptThirdPartyCookies(webView, true);
            }
        } catch (Exception e) {
        }
    }

    /**
     * 설명 : 매매플랫폼 모바일 웹뷰 설정
     */
    private void initWebView() {
        Log.d(TAG, "initWebView()");
        boolean isConnected = Util.checkNetworkConnection(this);

        if (false == isConnected) {
            Toast.makeText(this, "인터넷에 접속되어 있지 않습니다!", Toast.LENGTH_SHORT).show();
            finish();//액티비티 종료
        } else {

            // 개발 버전일 경우, 주소 입력창 보여주기
            if (BuildConfig.FLAVOR.equalsIgnoreCase("dev")) {
                lyLinkBody.setVisibility(View.VISIBLE);
                edtUrl.setText(Common.MOBILE_WEB_URL + Common.MOBILE_LOGIN_URL);
            }else {
                lyLinkBody.setVisibility(View.GONE);
            }

            // 웹뷰에 자바 스크립트를 사용할 수 있게 허용.
            mWebview.getSettings().setJavaScriptEnabled(true);
            // For React JS Function in webview
            mWebview.getSettings().setDomStorageEnabled(true);
            mWebview.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
            // User-Agent Setting
            mWebview.getSettings().setUserAgentString(mWebview.getSettings().getUserAgentString() + " AUTOBELL_Android_Ver"+BuildConfig.VERSION_NAME);

            // Java (or similar [laughs])
            // Enable hardware acceleration using code (>= level 19)
            if (Build.VERSION.SDK_INT >= 19) {
                mWebview.setLayerType(View.LAYER_TYPE_HARDWARE, null);
            } else {
                mWebview.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                mWebview.setRendererPriorityPolicy(WebView.RENDERER_PRIORITY_IMPORTANT, true);
            }
            mWebview.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
            mWebview.getSettings().setSupportMultipleWindows(true);
            mWebview.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
            mWebview.getSettings().setTextZoom(96);
//            mWebview.getSettings().setJavaScriptCanOpenWindowsAutomatically(false);
//            mWebview.getSettings().setSupportMultipleWindows(false);
            /*mWebview.getSettings().setAppCacheEnabled(true);
            mWebview.getSettings().setLoadsImagesAutomatically(true);*/

            // For WebView Chrome debuging
            /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                mWebview.setWebContentsDebuggingEnabled(true);
            }*/

            // 크롬 핸들러 설정. 이 핸들러는 자바 스크립트 대화상자, favicon, 제목과 진행상황 처리에 사용하기 위해.
            //mWebview.setWebChromeClient(new WebChromeClient());
            WebChromeClient webChromeClient = new WebChromeClient() {
                @Override
                public boolean onJsAlert(WebView view, String url, String message, final JsResult result) {
                    new AlertDialog.Builder(MainActivity.this)
                            .setTitle("Alert Dialog")
                            .setMessage(message)
                            .setPositiveButton(android.R.string.ok,
                                    new AlertDialog.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            result.confirm();
                                        }
                                    })
                            .setCancelable(false)
                            .create()
                            .show();
                    return true;
                }

                @Override
                public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> filePathCallback,
                                                 WebChromeClient.FileChooserParams fileChooserParams) {
                    Log.d(TAG, "setWebChromeClient() => onShowFileChooser()");

                    mUploadMessage = filePathCallback;

                    boolean isCapture = fileChooserParams.isCaptureEnabled();
                    runCamera(isCapture);

//                    selectAddPictureType();
//                    openFileExplorer();
                    return true;
                }

                @Override
                public boolean onCreateWindow(WebView view, boolean isDialog, boolean isUserGesture, Message resultMsg) {
                    // Dialog Create Code
                    Log.d(TAG, "initWebView : onCreateWindow() : isDialog = " + isDialog);

                    WebView newWebView = new WebView(MainActivity.this);

                    WebSettings webSettings = newWebView.getSettings();
                    webSettings.setJavaScriptEnabled(true);

                    // 쿠키허용
                    setCookieAllow(mCookieManager, newWebView);

                    // 화면 창 사라지게하는 dialog 설정
                    AndroidBridge ab = new AndroidBridge(newWebView, MainActivity.this);
                    newWebView.addJavascriptInterface(ab, Common.JAVASCRIP_INTERFACE_NAME);

                    // 화면 보이기
//                    mDialog = new Dialog(MainActivity.this);
//                    mDialog.setContentView(newWebView);
//                    ViewGroup.LayoutParams params = mDialog.getWindow().getAttributes();
//                    params.width = ViewGroup.LayoutParams.MATCH_PARENT;
//                    params.height = ViewGroup.LayoutParams.MATCH_PARENT;
//                    mDialog.getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);
//                    mDialog.show();
                    fl_webview.addView(newWebView);
                    arr_views.add(newWebView);

                    // 웹뷰에 자바 스크립트를 사용할 수 있게 허용.
                    newWebView.getSettings().setJavaScriptEnabled(true);
                    // For React JS Function in webview
                    newWebView.getSettings().setDomStorageEnabled(true);
                    newWebView.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
                    // User-Agent Setting
                    newWebView.getSettings().setUserAgentString(newWebView.getSettings().getUserAgentString() + " AUTOBELL_Android_Ver"+BuildConfig.VERSION_NAME);

                    // Java (or similar [laughs])
                    // Enable hardware acceleration using code (>= level 19)
                    if (Build.VERSION.SDK_INT >= 19) {
                        newWebView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
                    } else {
                        newWebView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                    }

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        newWebView.setRendererPriorityPolicy(WebView.RENDERER_PRIORITY_IMPORTANT, true);
                    }
                    newWebView.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
                    newWebView.getSettings().setSupportMultipleWindows(false);
                    newWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(false);
                    newWebView.getSettings().setTextZoom(96);

                    newWebView.setWebChromeClient(new WebChromeClient() {
                        @Override
                        public boolean onCreateWindow(WebView view, boolean isDialog, boolean isUserGesture, Message resultMsg) {
                            return true;
                        }
                        @Override
                        public void onCloseWindow(WebView window) {
//                            mDialog.dismiss();
                            fl_webview.removeView(newWebView);
                            arr_views.remove(newWebView);
                        }
                    });

                    // WebView Popup에서 내용이 안보이고 빈 화면만 보여 아래 코드 추가
                    newWebView.setWebViewClient(new WebViewClient() {

                        @Override
                        public boolean shouldOverrideUrlLoading(WebView webView, WebResourceRequest request) {
                            String url = request.getUrl().toString();
                            Log.d(TAG, "newWebView : shouldOverrideUrlLoading() : url = " + url);
                            String ext = url.substring(url.lastIndexOf(".")+1);
                            if (url != null && url.startsWith("https://map.kakao.com")) {
                                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                                startActivity(intent);
                                fl_webview.removeView(newWebView);
                                arr_views.remove(newWebView);
                            }else if (url != null && url.startsWith("intent:")) {
                                try {
                                    Intent intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME);
                                    Intent existPackage = getPackageManager().getLaunchIntentForPackage(intent.getPackage());
                                    startActivityForResult(intent, ACTION_VIEW_RESULTCODE);
//                                    if (existPackage != null) {
//                                    } else {
//                                        Intent marketIntent = new Intent(Intent.ACTION_VIEW);
//                                        marketIntent.setData(Uri.parse("market://details?id=" + intent.getPackage()));
//                                        startActivityForResult(marketIntent, ACTION_VIEW_RESULTCODE);
//                                    }
                                    //네이버 오피스 흰색 화면 나오는 현상
                                    if(url.startsWith("intent://naver.me")){
                                        fl_webview.removeView(newWebView);
                                        arr_views.remove(newWebView);
                                    }
                                    return true;
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            } else if (url != null && url.startsWith("market://")) {
                                try {
                                    Intent intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME);
                                    if (intent != null) {
                                        startActivityForResult(intent, ACTION_VIEW_RESULTCODE);
                                    }
                                    return true;
                                } catch (URISyntaxException e) {
                                    e.printStackTrace();
                                }

                            } else if (url != null && url.startsWith("https://play.google.com/store/apps")) {
                                try {
                                    Intent intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME);
                                    if (intent != null) {
                                        startActivityForResult(intent, ACTION_VIEW_RESULTCODE);
                                    }
                                    return true;
                                } catch (URISyntaxException e) {
                                    e.printStackTrace();
                                }

                            } else if (url.startsWith("tel:")) {

                                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse(url));
                                startActivityForResult(intent, ACTION_VIEW_RESULTCODE);
                                return true;

                            } else if (ext.equals("pdf") || ext.equals("doc") || ext.equals("xls") || ext.equals("hwp")) {

                                Uri uri = Uri.parse(url);
                                startActivityForResult(new Intent(Intent.ACTION_VIEW, uri), ACTION_VIEW_RESULTCODE);
                                return true;
                            } else if (url.contains("download.do")) {
                                Uri uri = Uri.parse(url);
                                startActivity(new Intent(Intent.ACTION_VIEW, uri));
                                return true;
                            }else if (url != null && !url.startsWith(Common.MOBILE_WEB_URL)) {
                                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                                startActivity(intent);
                                fl_webview.removeView(newWebView);
                                arr_views.remove(newWebView);
                                return true;
                            }

                            return false;
                        }

                        @Override
                        public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error){

                            if(BuildConfig.FLAVOR=="dev"){
                                super.onReceivedSslError(view, handler, error);
                            }else{
                                Log.e(TAG, "onReceivedSslError() error : " + error.toString());
                                final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                                builder.setMessage("이 사이트의 보안 인증서는 신뢰할 수 없습니다.");
                                builder.setPositiveButton("continue", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        handler.proceed();
                                    }
                                });
                                builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        handler.cancel();
                                    }
                                });
                                final AlertDialog dialog = builder.create();
                                dialog.show();
                            }
                        }
                    });

                    ((WebView.WebViewTransport)resultMsg.obj).setWebView(newWebView);
                    resultMsg.sendToTarget();
                    return true;
                }
            };

            mWebview.setWebChromeClient(webChromeClient);
            // Bridge setting
            AndroidBridge ab = new AndroidBridge(mWebview, MainActivity.this);
            mWebview.addJavascriptInterface(ab, Common.JAVASCRIP_INTERFACE_NAME);

            // 각종 알림 및 요청을 받게되는 WebViewClient를 설정 (직접만든 웹뷰 클라이언트를 상속한 클래스 설정)
            mWebview.setWebViewClient(new WebViewClient() {
                // 링크 클릭에 대한 반응
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                    String url = request.getUrl().toString();
                    Log.d(TAG, "shouldOverrideUrlLoading() > URL : " + url);
                    if (url.startsWith("tel:")) {

                        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse(url));
                        startActivity(intent);
                        return true;
                    }
                    else if (url.startsWith("intent:")) {

                        try {
                            Intent intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME);
                            Intent existPackage = getPackageManager().getLaunchIntentForPackage(intent.getPackage());
                            startActivity(intent);
//                            if (existPackage != null) {
//                            } else {
//                                Intent marketIntent = new Intent(Intent.ACTION_VIEW);
//                                marketIntent.setData(Uri.parse("market://details?id=" + intent.getPackage()));
//                                startActivity(marketIntent);
//                            }
                            return true;
                        } catch (ActivityNotFoundException e) {
                            Log.e(TAG, "ActivityNotFoundException url : " + url);
                            // 카카오 링크가 포함된 경우
                            if(url.contains("kakaolink://send")) {
                                Intent intentKakao = new Intent(Intent.ACTION_VIEW, Uri.parse((GOOGLE_PLAY_STORE_PREFIX + "com.kakao.talk")));
                                startActivity(intentKakao);
                            }
                            return true;
                        } catch (URISyntaxException e) {
                            Log.e(TAG, "URISyntaxException url : " + url);
                        }

                    } else if (url.contains("download.do")) {
                        Uri uri = Uri.parse(url);
                        startActivity(new Intent(Intent.ACTION_VIEW, uri));
                        return true;
                    }
                    view.stopLoading();
                    //referer 처리
                    Map<String, String> headers = new HashMap<>();
                    headers.put("Referer", view.getUrl());

                    // cache 삭제 처리
                    view.clearCache(true);
                    view.loadUrl(url, headers);
                    return true;
                }
                // 웹페이지 호출시 오류 발생에 대한 처리
                @SuppressWarnings("deprecation")
                @Override
                //public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                    mWebProgress.setVisibility(View.GONE);
                    if(errorCode == -6) {
                        // -6, desc: net::ERR_CONNECTION_REFUSED
                        try {
                            view.stopLoading();
                        } catch ( Exception e ) {
                            // ignore
                        } finally {
                            // reload
                            Uri failingUri = Uri.parse(failingUrl);
                            if(failingUri.getQueryParameter("cap_reloaded") == null) {
                                Uri.Builder reloadUri = failingUri.buildUpon();
                                reloadUri.appendQueryParameter("cap_reloaded", "true");
                                view.loadUrl(reloadUri.build().toString());
                            }
                        }
                    }else {
                        Log.e(TAG, "Fail to load web page : errorCode = " + errorCode);
                        Toast.makeText(getApplicationContext(), "웹페이지 호출에 실패하였습니다.", Toast.LENGTH_SHORT).show();
                        view.loadData("<html><body></body></html>", "text/html", "UTF-8");
                    }
                }
                // 페이지 로딩 시작시 호출
                @Override
                public void onPageStarted(WebView view,String url, Bitmap favicon){
                    super.onPageStarted(view, url, favicon);
                    Log.d(TAG, "onPageStarted() url : " + url);
                    mWebProgress.setVisibility(View.VISIBLE);
                }
                //페이지 로딩 종료시 호출
                public void onPageFinished(WebView view,String Url){
                    Log.d(TAG, "onPageFinished() url : " + Url);
                    mWebProgress.setVisibility(View.GONE);
                    CookieManager.getInstance().flush();
                    edtUrl.setText(Url);


                    SharedPreferences pref = getSharedPreferences(Common.PREFERENCE, MODE_PRIVATE);
                    String start_url = pref.getString("start_url", "");
                    if(start_url != null && start_url != ""){
                        view.loadUrl(start_url);
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putString("start_url", "");
                        editor.commit();
                        editor.apply();
                    }
                    if(PushURL != null && PushURL != ""){
                        view.loadUrl("javascript:rtnRoutingUrl('"+PushURL+"')");
                        PushURL = null;
                    }
                }
                @Override
                public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error){
                    if (BuildConfig.FLAVOR == "dev") {
                        super.onReceivedSslError(view, handler, error);
                    }else{
                        final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                        builder.setMessage("이 사이트의 보안 인증서는 신뢰할 수 없습니다.");
                        builder.setPositiveButton("continue", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                handler.proceed();
                            }
                        });
                        builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                handler.cancel();
                            }
                        });
                        final AlertDialog dialog = builder.create();
                        dialog.show();
                    }
                }
            });

            mWebProgress.setVisibility(View.VISIBLE);

            if(mOhMyCallURL != null && mOhMyCallURL.length() > 1) {
                Log.d(TAG, "initWebView() => mOhMyCall : " + mOhMyCallURL);
                rl_loading.setVisibility(View.GONE);
                mWebview.loadUrl(mOhMyCallURL);
            }else {
                Log.d(TAG, "initWebView() => Connect : " + Common.MOBILE_WEB_URL/* + Common.MOBILE_MAIN_URL*/);
                // 웹뷰에 첫 주소를 연결
                mWebview.loadUrl(Common.MOBILE_WEB_URL/* + Common.MOBILE_MAIN_URL*/);
            }

            ImageView ivSplash = findViewById(R.id.iv_splash);
            Glide.with(this).load(R.raw.intro_spinner).into(ivSplash);
            handlerLoading.postDelayed(runnableLoading, 1500); // 1.5초 뒤에 Runnable 객체 수행
        }
    }

    /**
     * 설명 : 웹뷰 사진 추가 버튼 선택 시 선택 창(사진촬영 / 파일선택)
     */
    private void selectAddPictureType() {
        Log.d(TAG, "selectAddPictureType()");
        final List<String> ListItems = new ArrayList<>();
        ListItems.add(getString(R.string.takepicture_text));
        ListItems.add(getString(R.string.selectfile_text));
        final CharSequence[] items =  ListItems.toArray(new String[ ListItems.size()]);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.title_addpicture_dialog));
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int pos) {
                // 사진촬영
                if(pos == 0) {
                    Log.d(TAG, "selectAddPictureType() => Take Picture");
                    takePicture();
                // 파일 선택
                }else {
                    Log.d(TAG, "selectAddPictureType() => Select File");
                    openFileExplorer();
                }
            }
        });
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                // dialog dismiss without button press
                mUploadMessage.onReceiveValue(null);
                mUploadMessage = null;
            }
        });
        builder.show();
    }

    /**
     * 설명 : 카메라 기능 구현
     */
    private void runCamera(boolean _isCapture)
    {
        Intent intentCamera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        File path = getFilesDir();
        File file = new File(path, "sample.jpg"); // sample.png 는 카메라로 찍었을 때 저장될 파일명이므로 사용자 마음대로
        // File 객체의 URI 를 얻는다.
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
        {
            String strpa = getApplicationContext().getPackageName();
            Uri result = FileProvider.getUriForFile(this, strpa + ".fileprovider", file);
            cameraImageUri = FileProvider.getUriForFile(this, strpa + ".fileprovider", file);
        }
        else
        {
            cameraImageUri = Uri.fromFile(file);
        }

        intentCamera.putExtra(MediaStore.EXTRA_OUTPUT, cameraImageUri);

        if (!_isCapture)
        { // 선택팝업 카메라, 갤러리 둘다 띄우고 싶을 때
            Intent pickIntent = new Intent(Intent.ACTION_PICK);
            pickIntent.setType(MediaStore.Images.Media.CONTENT_TYPE);
            pickIntent.setData(MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

            String pickTitle = "사진 가져올 방법을 선택하세요.";
            Intent chooserIntent = Intent.createChooser(pickIntent, pickTitle);

            // 카메라 intent 포함시키기..
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Parcelable[]{intentCamera});
            startActivityForResult(chooserIntent, FILECHOOSER_RESULTCODE);
        }
        else
        {// 바로 카메라 실행..
            startActivityForResult(intentCamera, FILECHOOSER_RESULTCODE);
        }
    }

    /**
     * 설명 : 메인 화면 UI 초가화
     */
    private void initView() {
        Log.d(TAG, "initView()");
        btn_detailshoot_light.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(MainActivity.this, DetailShootActivity.class);
                intent.putExtra("user_type", Common.USER_TYPE_CUSTOMER);
                intent.putExtra("car_id", "CAR_ID_001");
                intent.putExtra("car_type", Common.CAR_TYPE_LIGHT);
                intent.putExtra("car_number", "12가3456");
                startActivity(intent);
            }
        });

        btn_detailshoot_sedan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(MainActivity.this, DetailShootActivity.class);
                intent.putExtra("user_type", Common.USER_TYPE_DEALER);
                intent.putExtra("car_id", "CAR_ID_002");
                intent.putExtra("car_type", Common.CAR_TYPE_SEDAN);
                intent.putExtra("car_number", "12가1111");
                startActivity(intent);
            }
        });

        btn_detailshoot_suv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(MainActivity.this, DetailShootActivity.class);
                intent.putExtra("user_type", Common.USER_TYPE_EVALUATOR);
                intent.putExtra("car_id", "CAR_ID_003");
                intent.putExtra("car_type", Common.CAR_TYPE_SUV);
                intent.putExtra("car_number", "12가2222");
                startActivity(intent);
            }
        });

        btn_detailshoot_rv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(MainActivity.this, DetailShootActivity.class);
                intent.putExtra("user_type", 0);
                intent.putExtra("car_id", "CAR_ID_004");
                intent.putExtra("car_type", 3);
                intent.putExtra("car_number", "12가3333");
                startActivity(intent);
            }
        });

        btn_360pic_light.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(MainActivity.this, OverallShootActivity.class);
                intent.putExtra("user_type", 0);
                intent.putExtra("car_id", "CAR_ID_3601");
                intent.putExtra("car_type", 0);
                intent.putExtra("car_number", "12가0000");
                startActivity(intent);
            }
        });

        btn_360pic_sedan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(MainActivity.this, OverallShootActivity.class);
                intent.putExtra("user_type", 0);
                intent.putExtra("car_id", "CR20200510152");
                intent.putExtra("car_type", 1);
                intent.putExtra("car_number", "12가1111");
                startActivity(intent);
            }
        });

        btn_360pic_suv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(MainActivity.this, OverallShootActivity.class);
                intent.putExtra("user_type", 0);
                intent.putExtra("car_id", "CAR_ID_3603");
                intent.putExtra("car_type", 2);
                intent.putExtra("car_number", "12가2222");
                startActivity(intent);
            }
        });

        btn_360pic_rv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(MainActivity.this, OverallShootActivity.class);
                intent.putExtra("user_type", 0);
                intent.putExtra("car_id", "CAR_ID_3604");
                intent.putExtra("car_type", 3);
                intent.putExtra("car_number", "12가3333");
                startActivity(intent);
            }
        });

        btn_registerServiceAndUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                registerPushServiceAndUser();
            }
        });

        btn_unregister_push.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(PushUtils.checkNetwork(MainActivity.this)){
                    new Handler().post(new Runnable() {

                        @Override
                        public void run() {
                            // TODO Auto-generated method stub
                            // 푸시 서비스 등록 해제 - 서비스 및 사용자가 해제
                            PushManager.getInstance().unregisterPushService(getApplicationContext());
                        }
                    });
                }else{
                    PushLog.e("POLA_R PushLog", "network is not connected.");
                    Toast.makeText(MainActivity.this, "[MainActivity] network is not connected.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btn_send_push_msg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendPushMessage(5);
            }
        });

    }

    /**
     * 설명 : 상단 주소창 클리어
     */
    @OnClick(R.id.btnClear)
    public void onClickEditClear(View view) {
        if (edtUrl != null) {
            edtUrl.setText(Common.MOBILE_WEB_URL);
        }
    }

    /**
     * 설명 : 상단 주소창 입력 URL 이동
     */
    @OnClick(R.id.btnGo)
    public void onClickEditGo(View view) {
        if (edtUrl != null) {
            String strUrl = edtUrl.getText().toString();
            mWebview.loadUrl(strUrl);
        }
    }

    /**
     * 설명 : 촬영 테스트 화면 / 웹뷰 화면 전환
     */
    @OnClick(R.id.btnChangeView)
    public void onClickChangeDEVView(View view) {
        if (fl_webview.getVisibility() == View.VISIBLE) {
            fl_webview.setVisibility(View.GONE);
            btnChangeView.setText("웹뷰");
        }else {
            fl_webview.setVisibility(View.VISIBLE);
            btnChangeView.setText("촬영");
        }
    }

    /**
     * 설명 : 촬영 가이드 DB를 생성 (이미 생성되어 있다면 생략)
     */
    public void setGuideDBData() {
        Log.d(TAG, "setGuideDBData()");
        // CREATE GUIDE DB TABLE
        GuideDBHelper guideDBHelper = new GuideDBHelper(this, Common.DB_NAME, null, 1);

        // Test CODE : 필수 촬영 5개 부분만 촬영하는 임시 기능을 위해 추가
        //guideDBHelper.removeAll();

        int nGuideSize = (int) guideDBHelper.getDataSize();

        if (nGuideSize == 0) {
            guideDBHelper.insert(Common.DETAIL_SHOOT_GUIDE_OUTSIDE, 1,  "front_side",           "차량 전면(측)","1", 2);
            guideDBHelper.insert(Common.DETAIL_SHOOT_GUIDE_OUTSIDE, 2,  "back_side",            "차량 후면(측)","2", 7);
            guideDBHelper.insert(Common.DETAIL_SHOOT_GUIDE_OUTSIDE, 3,  "front",                "차량 전면",    "3", 1);
            guideDBHelper.insert(Common.DETAIL_SHOOT_GUIDE_OUTSIDE, 4,  "back",                 "차량 후면",    "4", 5);
            guideDBHelper.insert(Common.DETAIL_SHOOT_GUIDE_INSIDE,  5,  "instrument_board",     "내부 계기판",  "5", 13);
            guideDBHelper.insert(Common.DETAIL_SHOOT_GUIDE_OUTSIDE, 6,  "wheel",                "휠&타이어",    "6", 4);
            guideDBHelper.insert(Common.DETAIL_SHOOT_GUIDE_OUTSIDE, 7,  "bonnet",               "엔진룸",       "7", 8);
            guideDBHelper.insert(Common.DETAIL_SHOOT_GUIDE_INSIDE,  8,  "drivers_seat",         "운전석 앞 문", "8", 9);
            guideDBHelper.insert(Common.DETAIL_SHOOT_GUIDE_INSIDE,  9,  "drivers_back_seat",    "운전석 뒷 문", "9", 10);
            guideDBHelper.insert(Common.DETAIL_SHOOT_GUIDE_INSIDE,  10, "passenger_seat",       "조수석 앞 문", "10", 11);
            guideDBHelper.insert(Common.DETAIL_SHOOT_GUIDE_INSIDE,  11, "passenger_back_seat",  "조수석 뒷 문", "11", 12);
            guideDBHelper.insert(Common.DETAIL_SHOOT_GUIDE_INSIDE,  12, "dashboard",            "내부(앞)",     "12", 18);
            guideDBHelper.insert(Common.DETAIL_SHOOT_GUIDE_INSIDE,  13, "center_fascia",        "센터페시아",   "13", 14);
            guideDBHelper.insert(Common.DETAIL_SHOOT_GUIDE_INSIDE,  14, "overhead_console",     "룸미러",       "14", 16);
            guideDBHelper.insert(Common.DETAIL_SHOOT_GUIDE_OUTSIDE, 15, "top",                  "차량 상단",    "15", 3);
            guideDBHelper.insert(Common.DETAIL_SHOOT_GUIDE_OUTSIDE, 16, "trunk",                "트렁크",       "16", 6);
            guideDBHelper.insert(Common.DETAIL_SHOOT_GUIDE_INSIDE,  17, "gearbox",              "기어박스",     "17", 17);
            guideDBHelper.insert(Common.DETAIL_SHOOT_GUIDE_INSIDE,  18, "back_camera",          "후방카메라",   "18", 15);
            guideDBHelper.insert(Common.DETAIL_SHOOT_GUIDE_OUTSIDE, 19, "scratch_shoot",        "스크래치샷",   "19", 19);
            guideDBHelper.insert(Common.DETAIL_SHOOT_GUIDE_OUTSIDE, 20, "scratch_shoot2",       "스크래치샷2",  "20", 20);

            guideDBHelper.insert(Common.PASS_EVALUATION_SHOOT_GUIDE, 1, "front", "차량 전면", "0210", 0);
            guideDBHelper.insert(Common.PASS_EVALUATION_SHOOT_GUIDE, 2, "instrument_board", "내부 계기판", "0220", 0);
            guideDBHelper.insert(Common.PASS_EVALUATION_SHOOT_GUIDE, 3, "license", "차량등록증", "0230", 0);
            guideDBHelper.insert(Common.PASS_EVALUATION_SHOOT_GUIDE, 4, "option", "기타", "0240", 0);

            guideDBHelper.insert(Common.OVERALL_SHOOT_GUIDE, 1, "img_0_0_0", "차량 전면", "", 0);
            guideDBHelper.insert(Common.OVERALL_SHOOT_GUIDE, 2, "img_0_0_1", "차량 우측 전면1", "", 0);
            guideDBHelper.insert(Common.OVERALL_SHOOT_GUIDE, 3, "img_0_0_2", "차량 우측 전면2", "", 0);
            guideDBHelper.insert(Common.OVERALL_SHOOT_GUIDE, 4, "img_0_0_3", "차량 측면(운전석)", "", 0);
            guideDBHelper.insert(Common.OVERALL_SHOOT_GUIDE, 5, "img_0_0_4", "차량 우측 후면1", "", 0);
            guideDBHelper.insert(Common.OVERALL_SHOOT_GUIDE, 6, "img_0_0_5", "차량 우측 후면2", "", 0);
            guideDBHelper.insert(Common.OVERALL_SHOOT_GUIDE, 7, "img_0_0_6", "차량 후면", "", 0);
            guideDBHelper.insert(Common.OVERALL_SHOOT_GUIDE, 8, "img_0_0_7", "차량 좌측 후면1", "", 0);
            guideDBHelper.insert(Common.OVERALL_SHOOT_GUIDE, 9, "img_0_0_8", "차량 좌측 후면2", "", 0);
            guideDBHelper.insert(Common.OVERALL_SHOOT_GUIDE, 10, "img_0_0_9", "차량 측면(조수석)", "", 0);
            guideDBHelper.insert(Common.OVERALL_SHOOT_GUIDE, 11, "img_0_0_10", "차량 좌측 전면1", "", 0);
            guideDBHelper.insert(Common.OVERALL_SHOOT_GUIDE, 12, "img_0_0_11", "차량 좌측 전면2", "", 0);
        }
    }

    /**
     * 설명 : Push service 등록 (앱 실행 시 마다 호출)
     */
//    public void registerPushService() {
//        // 필수 데이터 구성 - Client ID 와 Client Name 으로 User 등록
//        final JSONObject params = new JSONObject();
//        try {
//            SharedPreferences pref = getSharedPreferences(Common.PREFERENCE, MODE_PRIVATE);
//            String uuid = pref.getString(Common.PARAM_UUID, "A"+UUID.randomUUID().toString());
//            pref.edit().putString(Common.PARAM_UUID, uuid);
//            pref.edit().commit();
//            pref.edit().apply();
//
//            params.put(PushConstants.KEY_CUID, uuid);
//            params.put(PushConstants.KEY_CNAME, uuid);   // CNAME도 ID와 동일하게 입력
//            Log.d("","registerPushService : " + uuid);
//        }
//        catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        //PushConstants.PushRegistStatus.REGISTER_RUNNING : 등록 수행중 상태
//        //PushConstants.PushRegistStatus.ALREADY_REGISTERED : 등록 완료 상태
//        //PushConstants.PushRegistStatus.NOT_REGISTERED : 미등록 상태
//        if (PushUtils.checkNetwork(MainActivity.this)) {
//            int getPushRegStatus = PushManager.getInstance().getPushRegStatus(MainActivity.this);
//            if (getPushRegStatus == PushConstants.PushRegistStatus.NOT_REGISTERED) {
//                new Handler().post(new Runnable() {
//                    @Override
//                    public void run() {
//                        // TODO Auto-generated method stub
//
//                        PushManager.getInstance().registerPushService(MainActivity.this, params);
//                    }
//                });
//            }
//        }
//        else {
//            PushLog.e("POLA_R PushLog", "registerPushService() network is not connected.");
//            Toast.makeText(MainActivity.this, "[MainActivity] network is not connected.", Toast.LENGTH_SHORT).show();
//        }
//    }
//
//    /**
//     * 설명 : Push User 등록 (로그인 시 마다 호출)
//     */
//    public void registerPushUser() {
//        // 필수 데이터 구성 - Client ID 와 Client Name 으로 User 등록
//        final JSONObject params = new JSONObject();
//        try {
//            SharedPreferences pref = getSharedPreferences(Common.PREFERENCE, MODE_PRIVATE);
//            String user_ID = pref.getString(Common.PARAM_USERID, "ClientUID001Android");  // def 값은 테스트 ID : ClientUID001Android
//
//            params.put(PushConstants.KEY_CUID, user_ID);
//            params.put(PushConstants.KEY_CNAME, user_ID);   // CNAME도 ID와 동일하게 입력
//        }
//        catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        if(PushUtils.checkNetwork(MainActivity.this)){
//            new Handler().post(new Runnable() {
//
//                @Override
//                public void run() {
//                    // TODO Auto-generated method stub
//                    PushManager.getInstance().registerPushUser(MainActivity.this, params);
//                }
//            });
//        }else{
//            PushLog.e("POLA_R PushLog", "network is not connected.");
//            Toast.makeText(MainActivity.this, "[MainActivity] network is not connected.", Toast.LENGTH_SHORT).show();
//        }
//    }

    /**
     * 설명 : Push service 및 User 등록 (앱 실행 시 마다 호출)
     */
    public void registerPushServiceAndUser() {
        // 필수 데이터 구성 - Client ID 와 Client Name 으로 User 등록
        final JSONObject params = new JSONObject();
        try {
            SharedPreferences pref = getSharedPreferences(Common.PREFERENCE, MODE_PRIVATE);
            String uuid = pref.getString(Common.PARAM_UUID, "A"+UUID.randomUUID().toString());
            pref.edit().putString(Common.PARAM_UUID, uuid);
            pref.edit().commit();
            pref.edit().apply();

            Log.d(TAG, "registerPushServiceAndUser() CUID : " + uuid);
            params.put(PushConstants.KEY_CUID, uuid);
            params.put(PushConstants.KEY_CNAME, uuid);   // CNAME도 ID와 동일하게 입력

            // 서비스 등록 및 사용자 등록을 함께 진행할 시 이용하는 API
            // 별도의 서비스 등록이 필요 없고 사용자 등록 시에 해당 API를 통해 서비스 등록_사용자 등록을 진행
            // http://docs.morpheus.co.kr/client/push/gcm.html#service 참고.
            if(PushUtils.checkNetwork(MainActivity.this)){
                int getPushRegStatus = PushManager.getInstance().getPushRegStatus(MainActivity.this);
                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        // TODO Auto-generated method stub
                        Log.d(TAG, "registerPushServiceAndUser() : register Push Service");
                        PushManager.getInstance().registerServiceAndUser(MainActivity.this, uuid, uuid);
                    }
                });
                /*if (getPushRegStatus == PushConstants.PushRegistStatus.NOT_REGISTERED) {
                    new Handler().post(new Runnable() {
                        @Override
                        public void run() {
                            // TODO Auto-generated method stub
                            Log.d(TAG, "registerPushServiceAndUser() : register Push Service");
                            PushManager.getInstance().registerServiceAndUser(MainActivity.this, params);
                        }
                    });
                }else {
                    Log.d(TAG, "registerPushServiceAndUser() : already registerd Push Service");
                }*/
            }else {
                Log.e(TAG, "registerPushServiceAndUser() : network is not connected.");
                Toast.makeText(MainActivity.this, "[MainActivity] network is not connected.", Toast.LENGTH_SHORT).show();
            }
        }
        catch (JSONException e) {
            e.printStackTrace();
            Log.e(TAG, "registerPushServiceAndUser() : JSONException => " + e.toString());
        }
    }

    /**
     * 설명 : 사용자 권한 획득 후 결과 처리
     * @param {int} requestCode 권한 요청 코드
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == REQUEST_CAMERA) {

            //OREO API 26 이상에서는 채널 필요
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                SharedPreferences pref = getSharedPreferences(Common.PREFERENCE, MODE_PRIVATE);
                boolean isCreateChannel = pref.getBoolean(Common.CREATE_NOTIFICATION_CHANNEL, false);  // Notification Channel 생성 여부 확인
                if (false == isCreateChannel) {
                    Log.d(TAG, "createChannel()");
                    PushNotificationManager.createChannel(this);
                }else {
                    Log.d(TAG, "Already Channel created");
                }
            }

            initView();

            setGuideDBData();

            initWebView();

            // 다른 앱 위에 표시 허용
            if ( false == Util.checkDrawOverlayPermission(getApplicationContext()) ) {
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                    startActivity(
                            new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getPackageName()))
                                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    );
                }
            }

            // 절전 모드 해제 권한 얻기
            getIgnoreBatteryOptimizationPermission();

            //앱 실행시점에서 호출 (4.0 )
            PushManager.getInstance().initPushServer(getApplicationContext());
            // Push service 및 User 등록 (앱 실행 시 마다 호출)
//            registerPushServiceAndUser();

        }else {
            Toast.makeText(MainActivity.this, R.string.fail_permisson, Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        registerReceiver();
    }

    @Override
    protected void onPause() {
        super.onPause();

        unregisterReceiver();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy()");
    }

    private void openFileExplorer(){
        Intent i = new Intent(Intent.ACTION_GET_CONTENT);
        i.addCategory(Intent.CATEGORY_OPENABLE);
        i.setType("image/*");
        MainActivity.this.startActivityForResult( Intent.createChooser( i, "File Chooser" ), MainActivity.FILECHOOSER_RESULTCODE );
    }

    private void takePicture() {
        Log.d(TAG, "takePicture()");
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        /*if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, FILECHOOSER_RESULTCODE);
        }*/

        File path = getFilesDir();
        File file = new File(path + "/Pictures", "sample.jpg"); // sample.png 는 카메라로 찍었을 때 저장될 파일명이므로 사용자 마음대로
        // File 객체의 URI 를 얻는다.
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
        {
            String strpa = getApplicationContext().getPackageName();
            cameraImageUri = FileProvider.getUriForFile(this, strpa + ".fileprovider", file);
        }
        else
        {
            cameraImageUri = Uri.fromFile(file);
        }

        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, cameraImageUri);

        // 선택팝업 카메라, 갤러리 둘다 띄우고 싶을 때
        Intent pickIntent = new Intent(Intent.ACTION_PICK);
        pickIntent.setType(MediaStore.Images.Media.CONTENT_TYPE);
        pickIntent.setData(MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        String pickTitle = "사진 가져올 방법을 선택하세요.";
        Intent chooserIntent = Intent.createChooser(pickIntent, pickTitle);

        // 카메라 intent 포함시키기..
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Parcelable[]{takePictureIntent});
        startActivityForResult(chooserIntent, FILECHOOSER_RESULTCODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        // startActivityForResult에 지정된 Integer 인수가 촬영 화면이라면 WEB에 촬영 종료 메서드 전달
        if (requestCode == Common.REQ_SHOOT_ACT && resultCode == RESULT_OK) {
            Log.d(TAG, "onActivityResult() => rtnShootingComplete()");
            mWebview.loadUrl("javascript:rtnShootingComplete()");
        }
        else if (requestCode == FILECHOOSER_RESULTCODE) {
            Log.d(TAG, "onActivityResult() => requestCode == FILECHOOSER_RESULTCODE");
            if (null == mUploadMessage) return;
            Uri result = intent == null || resultCode != RESULT_OK ? null : intent.getData();
            Log.d(TAG, cameraImageUri+"<===cameraImageUri");
            Log.d(TAG, result+"<===result");

//            if (intent.getData() == null)
//                intent.setData(cameraImageUri);

            Log.d(TAG, result+"<===result");

            if (result == null) {
                mUploadMessage.onReceiveValue(null);
            } else {
                mUploadMessage.onReceiveValue(new Uri[]{result});
            }
            mUploadMessage = null;
        }
        else if (requestCode == ACTION_VIEW_RESULTCODE) {
            Log.d(TAG, "onActivityResult() => requestCode == ACTION_VIEW_RESULTCODE");
//            if (mDialog != null)
//                mDialog.dismiss();
        }
    }

    /**
     * 설명 : 절전모드 해제 권한 획득
     */
    public void getIgnoreBatteryOptimizationPermission() {
        PowerManager pm = (PowerManager) getApplicationContext().getSystemService(POWER_SERVICE);
        boolean isWhiteListing = false;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            isWhiteListing = pm.isIgnoringBatteryOptimizations(getApplicationContext().getPackageName());
        }
        if (!isWhiteListing) {
            AlertDialog.Builder setdialog = new AlertDialog.Builder(MainActivity.this);
            setdialog.setTitle("권한이 필요합니다")
                    .setMessage("오마이콜 기능을 사용하기 위해서는 \"배터리 사용량 최적화\" 목록에서 제외하는 권한이 필요합니다. 계속하시겠습니까?")
                    .setPositiveButton("예", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent  = new Intent(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
                            intent.setData(Uri.parse("package:"+ getApplicationContext().getPackageName()));
                            startActivity(intent);
                        }
                    })
                    .setNegativeButton("아니오", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Toast.makeText(MainActivity.this, "권한 설정을 취소했습니다.", Toast.LENGTH_SHORT).show();
                        }
                    })
                    .create()
                    .show();
        }
    }

    /**
     * 설명 : 푸시 API 처리 결과 수신
     */
    public void registerReceiver() {
        Log.d(TAG, "registerReceiver()");
        if (mMainBroadcastReceiver != null) {
            return;
        }

        // 푸시 API 결과를 받기 위한 인텐트 필터 설정
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(MainActivity.this.getPackageName()  + PushConstantsEx.ACTION_COMPLETED);

        mMainBroadcastReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {

                // 액션 유효성 체크
                if (!PushUtils.checkValidationOfCompleted(intent, context)) {
                    return;
                }

                //intent 정보가 정상적인지 판단
                String result = intent.getExtras().getString(PushConstants.KEY_RESULT);
                String bundle = intent.getExtras().getString(PushConstantsEx.KEY_BUNDLE);

                JSONObject result_obj = null; // 결과 오브젝트
                String resultCode = ""; // 결과 코드
                String resultMsg = ""; // 결과 메세지
                try {
                    result_obj = new JSONObject(result);
                    resultCode = result_obj.getString(PushConstants.KEY_RESULT_CODE);
                    resultMsg = result_obj.getString(PushConstants.KEY_RESULT_MSG);
                }
                catch (JSONException e) {
                    e.printStackTrace();
                }

                // 액션에 따라 분기 처리
                // 사용자 등록 결과 처리
                if (bundle.equals(PushConstantsEx.COMPLETE_BUNDLE.REG_USER)) {
                    if (resultCode.equals(PushConstants.SUCCESS_RESULT_CODE)) {
                        //Toast.makeText(context, "푸시 사용자 등록 성공!", Toast.LENGTH_SHORT).show();
                        Log.d(TAG, "Success REG_USER");
                    }
                    else {
                        Toast.makeText(context, "[MainActivity] error code: " + resultCode + " msg: " + resultMsg, Toast.LENGTH_SHORT).show();
                    }
                }
                // 서비스 및 사용자 등록 API를 사용했을 경우
                else if(bundle.equals(PushConstantsEx.COMPLETE_BUNDLE.REG_SERVICE_AND_USER)) {
                    if (resultCode.equals(PushConstants.SUCCESS_RESULT_CODE)) {
                        //Toast.makeText(context, "푸시 서비스 및 사용자 등록 성공!", Toast.LENGTH_SHORT).show();
                        Log.d(TAG, "Success REG_SERVICE_AND_USER");
                    }
                    else {
                        Toast.makeText(context, "[MainActivity] error code: " + resultCode + " msg: " + resultMsg, Toast.LENGTH_SHORT).show();
                    }
                }
                // 푸시 서비스 해제
                else if (bundle.equals(PushConstantsEx.COMPLETE_BUNDLE.UNREG_PUSHSERVICE)) {

                    if (resultCode.equals(PushConstants.SUCCESS_RESULT_CODE)) {
                        //Toast.makeText(context, "푸시 해제 성공!", Toast.LENGTH_SHORT).show();
                        Log.d(TAG, "Success UNREG_PUSHSERVICE");
                    }
                    else {
                        Toast.makeText(context, "[MainActivity] error code: " + resultCode + " msg: " + resultMsg, Toast.LENGTH_SHORT).show();
                    }
                }
                // 그룹 등록
                else if (bundle.equals(PushConstantsEx.COMPLETE_BUNDLE.REG_GROUP)) {

                    if (resultCode.equals(PushConstants.SUCCESS_RESULT_CODE)) {
                        //Toast.makeText(context, "그룹 등록 성공!", Toast.LENGTH_SHORT).show();
                        Log.d(TAG, "Success REG_GROUP");
                    }
                    else {
                        Toast.makeText(context, "[MainActivity] error code: " + resultCode + " msg: " + resultMsg, Toast.LENGTH_SHORT).show();
                    }
                }
                // 그룹 해제
                else if (bundle.equals(PushConstantsEx.COMPLETE_BUNDLE.UNREG_GROUP)) {

                    if (resultCode.equals(PushConstants.SUCCESS_RESULT_CODE)) {
                        //Toast.makeText(context, "그룹 해제 성공!", Toast.LENGTH_SHORT).show();
                        Log.d(TAG, "Success UNREG_GROUP");
                    }
                    else {
                        Toast.makeText(context, "[MainActivity] error code: " + resultCode + " msg: " + resultMsg, Toast.LENGTH_SHORT).show();
                    }
                }
                // 뱃지 초기화 - 안드로이드 기본 기능으로 제공되는 것이 아니므로
                // 뱃지 카운트를 서버를 통해 초기화 및 업데이트를 진행하고 이에 대해 디바이스에 적용해준다.
                else if (bundle.equals(PushConstantsEx.COMPLETE_BUNDLE.INITBADGENO)) {

                    if (resultCode.equals(PushConstants.SUCCESS_RESULT_CODE)) {
                        //Toast.makeText(context, "Badge Number 초기화 성공 !", Toast.LENGTH_SHORT).show();
                        Log.d(TAG, "Success INITBADGENO");
                        // 초기화 성공 시 실제 디바이스의 뱃지 값을 변경하여 준다.
                        PushManager.getInstance().setDeviceBadgeCount(getApplicationContext(), "0");
                    }
                    else {
                        Toast.makeText(context, "[MainActivity] error code: " + resultCode + " msg: " + resultMsg, Toast.LENGTH_SHORT).show();
                    }
                }
                // 이미 등록되어 있다면
                else if (bundle.equals(PushConstantsEx.COMPLETE_BUNDLE.IS_REGISTERED_SERVICE)) {
                    String isRegister = "";
                    try {
                        isRegister = result_obj.getString(PushConstants.KEY_ISREGISTER);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    if (isRegister.equals("C")) {
                        Toast.makeText(context, "CHECK ON [ 사용자 재등록 필요 !! ]", Toast.LENGTH_LONG).show();
                    }
                    else if(isRegister.equals("N")) {
                        Toast.makeText(context, "CHECK ON [ 서비스 재등록 필요 !! ]", Toast.LENGTH_LONG).show();
                    }
                    else {
                        Logger.i("서비스 정상 등록 상태 ");
                    }
                }
            }
        };

        LocalBroadcastManager.getInstance(this).registerReceiver(mMainBroadcastReceiver, intentFilter);
    }

    /**
     * 설명 : 푸시 BroadcastReceiver 해제
     */
    public void unregisterReceiver() {
        Log.d(TAG, "unregisterReceiver()");
        if (mMainBroadcastReceiver != null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(mMainBroadcastReceiver);
            mMainBroadcastReceiver = null;
        }
    }

    /**
     * 설명 : 푸시 Message 전송
     *  @param {int} type 전송 푸시 타입
     */
    private void sendPushMessage(int type) {
        String message = "일반 알림";
        String ext = "메세지 테스트";
        if (type == 2) {
            message = "Web 알림";
            ext = "1|웹페이지|http://lab.morpheus.kr/push/sample/image|http://lab.morpheus.kr/push/sample/webpage";
        }
        else if (type == 3) {
            message = "동영상 알림";
            ext = "1|웹페이지|http://lab.morpheus.kr/push/sample/image|https://youtu.be/IIu0VMdOe10";
        }
        else if (type == 4) {
            message = "이미지 알림";
            ext = "1|웹페이지|http://lab.morpheus.kr/push/sample/image"; // 파라메터 갯수로 구분
        }
        else if (type == 5) {
            message = "오마이콜 알림";
            ext = "{\"car_number\":\"12가3456\"" +
                  ",\"image_url\":\"http://blogfiles.naver.net/20160607_57/chowon_home_1465290347896jple3_JPEG/%BE%C6%B9%DD%B6%BC_%BD%BA%C6%F7%C3%F7_%281%29.jpg\"" +
                  ",\"webpage_url\":\"https://gloviscar.com/sellingcars/200102_1/\"" +
                  ",\"model_name\":\"제네시스 G80 3.8 GDI AWD\"" +
                  ",\"mileage\":\"38,000km\"" +
                  ",\"region\":\"경기\"" +
                  ",\"reg_date\":\"15/04\"" +
                  ",\"fuel_type\":\"가솔린\"" +
                  ",\"selling_price\":\"3,250\"" +
                  ",\"transfer_fee\":\"110만원\"" +
                  ",\"accident_status\":\"사고없음\"}";
        }

        Log.d(TAG, "login_btn >> sendPushMessage PushManager.getInstance().sendPushMessage");


        //service type은 서버 규약에 따름 upns 발송시 사용- 테스트 서버 기준 (GCM/ APNS 발송)
        //PushManager.getInstance().sendPushMessage(getApplicationContext(), message, ext, "", "0001");

        //service type은 서버 규약에 따름 upns 발송시 사용- 테스트 서버 기준 (gcm => dumy (doze mode), upns : 실발송 )

        if(PushUtils.checkNetwork(MainActivity.this)){
            final String finalMessage = message;
            final String finalExt = ext;
            new Handler().post(new Runnable() {

                @Override
                public void run() {
                    // TODO Auto-generated method stub
                    PushManager.getInstance().sendPushMessage(getApplicationContext(), finalMessage, finalExt, "ClientUID001Android", "ALL");   // <android-push-type> ALL 인 경우, SERVICE CODE : ALL
                    //PushManager.getInstance().sendPushMessage(getApplicationContext(), finalMessage, finalExt, "test1", "PUBLIC");   // <android-push-type> GCM 인 경우, SERVICE CODE : PUBLIC
                }
            });
        }else{
            PushLog.e("POLA_R PushLog", "network is not connected.");
            Toast.makeText(MainActivity.this, "network is not connected.", Toast.LENGTH_SHORT).show();
        }
    }
}
