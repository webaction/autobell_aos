package glovis.glovisaa.autobell.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import glovis.glovisaa.autobell.DetailShootActivity;
import glovis.glovisaa.autobell.util.Common;

/**
 * 차량 DB관리 클래스
 */

public class CarDBHelper extends SQLiteOpenHelper {

    Context context;
    DetailShootActivity activity;
    String tb_name;
    int version;
    SQLiteDatabase.CursorFactory factory;

    // CarDBHelper 생성자로 관리할 DB 이름과 버전 정보를 받음
    public CarDBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
        this.context = context;
        tb_name = Common.TABLE_CAR;
        this.version = version;
        this.factory = factory;
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("CREATE TABLE IF NOT EXISTS " + tb_name + " (_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "menu_type INT, " +                 // 메뉴 타입 (상세촬영 / 무평가촬영)
                "car_id VARCHAR2(100), " +          // 차량 ID
                "register_state INT, " +            // 등록상태(신규, 1장이상 촬영, 촬영완료, 업로드완료, 서버업로드),
                "shooted_photo_number INT, " +      // 촬영된 장수
                "total_needed_photo INT, " +        // 전체촬영필요장수
                "car_type INT);");                  // 차량타입 (세단, VAN 등)
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    //데이터 베이스 데이터의 갯수를 출력
    public long getDataSize() {
        SQLiteDatabase db = this.getReadableDatabase();
        long count = DatabaseUtils.queryNumEntries(db, tb_name);
        db.close();
        return count;
    }

    //상세 촬영 등록 차량 갯수를 출력
    public int getDetailShootSize() {
        // 읽기가 가능하게 DB 열기
        SQLiteDatabase db = getReadableDatabase();
        String result = "";

        String query = "SELECT * FROM " + tb_name + " WHERE menu_type = " + Common.MENU_TYPE_DETAIL_SHOOT;
        Cursor cursor = db.rawQuery(query, null);
        int number_of_data = cursor.getCount();
        return  number_of_data;
    }

    //무평가 촬영 등록 차량 갯수를 출력
    public int getPassEvaluationShootSize() {
        // 읽기가 가능하게 DB 열기
        SQLiteDatabase db = getReadableDatabase();
        String result = "";

        String query = "SELECT * FROM " + tb_name + " WHERE menu_type = ?";
        Cursor cursor = db.rawQuery(query, new String[]{String.valueOf(Common.MENU_TYPE_PASS_EVALUATION_SHOOT)});
        int number_of_data = cursor.getCount();
        return  number_of_data;
    }

    //새로운 데이터 입력
    public void insert(int menu_type, String car_id, int register_state, int shooted_photo_number, int total_needed_photo, int car_type) {

        // 읽고 쓰기가 가능하게 DB 열기
        SQLiteDatabase db = getWritableDatabase();
        // DB에 입력한 값으로 행 추가
        ContentValues values = new ContentValues();
        values.put("menu_type", menu_type);
        values.put("car_id", car_id);
        values.put("register_state", register_state);
        values.put("shooted_photo_number", shooted_photo_number);
        values.put("total_needed_photo", total_needed_photo);
        values.put("car_type", car_type);

        db.insert(tb_name, null, values);
        db.close();
    }

    //기존에 존재하는 데이터 업데이트
    public void update(int id, int menu_type, String car_id, int register_state, int shooted_photo_number, int total_needed_photo, int car_type) {

        // 읽고 쓰기가 가능하게 DB 열기
        SQLiteDatabase db = getWritableDatabase();
        // DB에 입력한 값으로 행 추가
        ContentValues values = new ContentValues();
        values.put("menu_type", menu_type);
        values.put("car_id", car_id);
        values.put("register_state", register_state);
        values.put("shooted_photo_number", shooted_photo_number);
        values.put("total_needed_photo", total_needed_photo);
        values.put("car_type", car_type);

        db.update(tb_name, values, "_id=" + id, null);
        db.close();
    }

    //특정 id를 갖는 데이터를 json으로 출력
    public JSONObject getData(int id) {
        Log.i("ID", String.valueOf(id));
        SQLiteDatabase db = getReadableDatabase();
        String query = "SELECT * FROM " + tb_name + " WHERE _id = ?";
        Cursor cursor = db.rawQuery(query, new String[]{String.valueOf(id)});

        if (cursor.getCount() == 0) {
            return null;
        }

        cursor.moveToPosition(0);

        int totalColumn = cursor.getColumnCount();
        JSONObject rowObject = new JSONObject();

        for (int i = 0; i < totalColumn; i++) {
            if (cursor.getColumnName(i) != null) {
                try {
                    if (cursor.getString(i) != null) {
                        rowObject.put(cursor.getColumnName(i), cursor.getString(i));
                    } else {
                        rowObject.put(cursor.getColumnName(i), "");
                    }
                } catch (Exception e) {

                }
            }
        }
        cursor.close();
        db.close();
        return rowObject;
    }

    // 모든 상세촬영 데이터를 json array로 얻어온다
    public JSONArray getAllDetailShootResult() {
        // 읽기가 가능하게 DB 열기
        SQLiteDatabase db = getReadableDatabase();
        String result = "";

        String query = "SELECT * FROM " + tb_name + " WHERE menu_type = ?";
        Cursor cursor = db.rawQuery(query, new String[]{String.valueOf(Common.MENU_TYPE_DETAIL_SHOOT)});

        JSONArray resultSet = new JSONArray();
        cursor.moveToPosition(0);
        int index = 0;

        int number_of_data = cursor.getCount();

        while (index < number_of_data) {

            int totalColumn = cursor.getColumnCount();
            JSONObject rowObject = new JSONObject();

            for (int i = 0; i < totalColumn; i++) {
                if (cursor.getColumnName(i) != null) {
                    try {
                        if (cursor.getString(i) != null) {
                            rowObject.put(cursor.getColumnName(i), cursor.getString(i));
                        } else {
                            rowObject.put(cursor.getColumnName(i), "");
                        }
                    } catch (Exception e) {
                    }
                }
            }
            resultSet.put(rowObject);
            cursor.moveToNext();
            index = index + 1;
        }
        cursor.close();
        return resultSet;
    }

    // 모든 무평가촬영 데이터를 json array로 얻어온다
    public JSONArray getAllPassEvaluationShootResult() {
        // 읽기가 가능하게 DB 열기
        SQLiteDatabase db = getReadableDatabase();
        String result = "";

        String query = "SELECT * FROM " + tb_name + " WHERE menu_type = ?";
        Cursor cursor = db.rawQuery(query, new String[]{String.valueOf(Common.MENU_TYPE_PASS_EVALUATION_SHOOT)});

        JSONArray resultSet = new JSONArray();
        cursor.moveToPosition(0);
        int index = 0;

        int number_of_data = cursor.getCount();

        while (index < number_of_data) {

            int totalColumn = cursor.getColumnCount();
            JSONObject rowObject = new JSONObject();

            for (int i = 0; i < totalColumn; i++) {
                if (cursor.getColumnName(i) != null) {
                    try {
                        if (cursor.getString(i) != null) {
                            rowObject.put(cursor.getColumnName(i), cursor.getString(i));
                        } else {
                            rowObject.put(cursor.getColumnName(i), "");
                        }
                    } catch (Exception e) {
                    }
                }
            }
            resultSet.put(rowObject);
            cursor.moveToNext();
            index = index + 1;
        }
        cursor.close();
        return resultSet;
    }

    //시작 id부터 일정 갯수의 데이터를 json array로 얻어온다
    public JSONArray getResult(int start_index, int number_of_data) {
        // 읽기가 가능하게 DB 열기
        SQLiteDatabase db = getReadableDatabase();
        String result = "";

        // DB에 있는 데이터를 쉽게 처리하기 위해 Cursor를 사용하여 테이블에 있는 모든 데이터 출력
        Cursor cursor = db.rawQuery("SELECT * FROM " + tb_name, null);
        JSONArray resultSet = new JSONArray();
        cursor.moveToPosition(start_index);
        int index = 0;
        while (index < number_of_data) {

            int totalColumn = cursor.getColumnCount();
            JSONObject rowObject = new JSONObject();

            for (int i = 0; i < totalColumn; i++) {
                if (cursor.getColumnName(i) != null) {
                    try {
                        if (cursor.getString(i) != null) {
                            rowObject.put(cursor.getColumnName(i), cursor.getString(i));
                        } else {
                            rowObject.put(cursor.getColumnName(i), "");
                        }
                    } catch (Exception e) {

                    }
                }

            }
            resultSet.put(rowObject);
            cursor.moveToNext();
            index = index + 1;
        }

        cursor.close();
        return resultSet;
    }

    //최근 데이터로 부터 일정 갯수의 데이터를 json array 로 얻어온다.
    public JSONArray getRecentData(int number_of_data) {
        // 읽기가 가능하게 DB 열기
        SQLiteDatabase db = getReadableDatabase();
        String result = "";

        // DB에 있는 데이터를 쉽게 처리하기 위해 Cursor를 사용하여 테이블에 있는 모든 데이터 출력
        Cursor cursor = db.rawQuery("SELECT * FROM " + tb_name, null);
        JSONArray resultSet = new JSONArray();
        cursor.moveToLast();
        int index = 0;
        while (index < number_of_data) {

            int totalColumn = cursor.getColumnCount();
            JSONObject rowObject = new JSONObject();

            for (int i = 0; i < totalColumn; i++) {
                if (cursor.getColumnName(i) != null) {
                    try {
                        if (cursor.getString(i) != null) {
                            rowObject.put(cursor.getColumnName(i), cursor.getString(i));
                        } else {
                            rowObject.put(cursor.getColumnName(i), "");
                        }
                    } catch (Exception e) {

                    }
                }

            }
            resultSet.put(rowObject);
            cursor.moveToPrevious();
            index = index + 1;
        }

        cursor.close();
        return resultSet;
    }

    //Primary key인 id를 이용해서 검색 후 삭제
    public void delete(int item) {

        // 입력한 항목과 일치하는 행 삭제
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("DELETE FROM " + tb_name + " WHERE _id= " + item + ";");
        db.close();
    }

    //모든 데이터 삭제
    public void removeAll() {
        SQLiteDatabase db = getReadableDatabase(); // helper is object extends SQLiteOpenHelper
        db.execSQL("DROP TABLE " + tb_name);

        db.execSQL("CREATE TABLE IF NOT EXISTS " + tb_name + " (_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "menu_type INT, " +                 // 메뉴 타입 (상세촬영 / 무평가촬영)
                "car_id VARCHAR2(100), " +          // 차량ID
                "register_state INT, " +            // 등록상태(신규, 1장이상 촬영, 촬영완료, 업로드완료, 서버업로드),
                "shooted_photo_number INT, " +      // 촬영된 장수
                "total_needed_photo INT, " +        // 전체촬영필요장수
                "car_type INT);");                  // 차량타입 (세단, VAN 등)
    }
}
