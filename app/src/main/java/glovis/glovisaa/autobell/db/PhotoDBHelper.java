package glovis.glovisaa.autobell.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import glovis.glovisaa.autobell.util.Common;
import glovis.glovisaa.autobell.util.Util;

/**
 * 사진 DB관리 클래스
 */

public class PhotoDBHelper extends SQLiteOpenHelper {

    Context context;
    String tb_name;
    int version;
    SQLiteDatabase.CursorFactory factory;

    // PhotoDBHelper 생성자로 관리할 DB 이름과 버전 정보를 받음
    public PhotoDBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
        this.context = context;
        tb_name = Common.TABLE_PHOTO;
        this.version = version;
        this.factory = factory;

        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("CREATE TABLE IF NOT EXISTS " + tb_name + " (_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "car_id VARCHAR2(100), " +            // 차량ID
                "guide_code VARCHAR2(100), " +        // 촬영 가이드 코드
                "photo_path VARCHAR2(100), " +        // 사진 저장 파일명
                "photo_uri VARCHAR2(100)" +           // 사진 저장 Uri
                ");");
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS '" + tb_name + "'");
        onCreate(db);
    }

    //데이터 베이스 데이터의 갯수를 출력
    public long getDataSize() {
        SQLiteDatabase db = this.getReadableDatabase();
        long count = DatabaseUtils.queryNumEntries(db, tb_name);
        db.close();
        return count;
    }

    //새로운 데이터 입력
    public int insert(String car_id, String guide_code, String photo_path, String photo_uri) {

        // 읽고 쓰기가 가능하게 DB 열기
        SQLiteDatabase db = getWritableDatabase();
        // DB에 입력한 값으로 행 추가
        ContentValues values = new ContentValues();

        values.put("car_id", car_id);
        values.put("guide_code", guide_code);
        values.put("photo_path", photo_path);
        values.put("photo_uri", photo_uri);

        long lID = db.insert(tb_name, null, values);
        db.close();
        return (int) lID;
    }

    //기존에 존재하는 데이터 업데이트
    public int update(int id, String car_id, String guide_code, String photo_path, String photo_uri) {

        // 읽고 쓰기가 가능하게 DB 열기
        SQLiteDatabase db = getWritableDatabase();
        // DB에 입력한 값으로 행 추가
        ContentValues values = new ContentValues();

        values.put("car_id", car_id);
        values.put("guide_code", guide_code);
        values.put("photo_path", photo_path);
        values.put("photo_uri", photo_uri);

        int nRes = db.update(tb_name, values, "_id=" + id, null);
        //int nRes = db.update(tb_name, values, "_id=?", new String[]{String.valueOf(id)});
        db.close();
        return nRes;
    }

    /**
     *  차량 번호로 데이터를 json array로 얻어온다
     *  @param sCarID : 차량 ID
     *  @param nMenuType : 상세 / 무평가 차량 타입
     */
    public JSONArray getResultByCarNumberNMenuType(String sCarID, int nMenuType) {
        // 읽기가 가능하게 DB 열기
        SQLiteDatabase db = getReadableDatabase();

        // DB에 있는 데이터를 쉽게 처리하기 위해 Cursor를 사용하여 테이블에 있는 데이터 출력
        String query = "";
        // 촬영부위(guide_side)가 외부(0) 또는 내부(1) 인 경우
        if(nMenuType == Common.MENU_TYPE_DETAIL_SHOOT) {

            query = "SELECT * FROM " + tb_name + " INNER JOIN " + Common.TABLE_GUIDE + " ON " + tb_name + ".guide_code = " + Common.TABLE_GUIDE + ".guide_code" +
                    " WHERE car_id = ?" + " AND " + Common.TABLE_GUIDE + ".guide_side != ?";
        }
        // 촬영부위(guide_side)가 무평가(2) 인 경우
        else if(nMenuType == Common.MENU_TYPE_PASS_EVALUATION_SHOOT) {

            query = "SELECT * FROM " + tb_name + " INNER JOIN " + Common.TABLE_GUIDE + " ON " + tb_name + ".guide_code = " + Common.TABLE_GUIDE + ".guide_code" +
                    " WHERE car_id = ?" + " AND " + Common.TABLE_GUIDE + ".guide_side = ?";
        }
        String[] selectionArgs = {sCarID, String.valueOf(Common.PASS_EVALUATION_SHOOT_GUIDE)};

        Cursor cursor = db.rawQuery(query, selectionArgs);
        JSONArray resultSet = new JSONArray();
        cursor.moveToPosition(0);

        int nSize = cursor.getCount();
        int index = 0;
        while (index < nSize) {

            int totalColumn = cursor.getColumnCount();
            JSONObject rowObject = new JSONObject();

            for (int i = 0; i < totalColumn; i++) {
                if (cursor.getColumnName(i) != null) {
                    try {
                        if (cursor.getString(i) != null) {
                            String strColumnName = cursor.getColumnName(i);
                            if(false == rowObject.has(strColumnName))
                                rowObject.put(strColumnName, cursor.getString(i));
                        } else {
                            //rowObject.put(cursor.getColumnName(i), "");
                        }
                    } catch (Exception e) {
                    }
                }
            }
            resultSet.put(rowObject);
            cursor.moveToNext();
            index = index + 1;
        }

        cursor.close();
        return resultSet;
    }

    /**
     *  차량 번호로 360도 촬영 타입 데이터를 json array로 얻어온다
     *  @param sCarID : 차량 번호
     */
    public JSONArray getResultByCarNumberForOverallType(String sCarID) {
        // 읽기가 가능하게 DB 열기
        SQLiteDatabase db = getReadableDatabase();

        // DB에 있는 데이터를 쉽게 처리하기 위해 Cursor를 사용하여 테이블에 있는 데이터 출력
        String query = "";
        // 촬영부위(guide_side)가 360도 촬영(3) 인 경우
        query = "SELECT * FROM " + tb_name + " INNER JOIN " + Common.TABLE_GUIDE + " ON " + tb_name + ".guide_code = " + Common.TABLE_GUIDE + ".guide_code" +
                " WHERE car_id = ?" + " AND " + Common.TABLE_GUIDE + ".guide_side = ?";

        String[] selectionArgs = {sCarID, String.valueOf(Common.OVERALL_SHOOT_GUIDE)};

        Cursor cursor = db.rawQuery(query, selectionArgs);
        JSONArray resultSet = new JSONArray();
        cursor.moveToPosition(0);

        int nSize = cursor.getCount();
        int index = 0;
        while (index < nSize) {

            int totalColumn = cursor.getColumnCount();
            JSONObject rowObject = new JSONObject();

            for (int i = 0; i < totalColumn; i++) {
                if (cursor.getColumnName(i) != null) {
                    try {
                        if (cursor.getString(i) != null) {
                            String strColumnName = cursor.getColumnName(i);
                            if(false == rowObject.has(strColumnName))
                                rowObject.put(strColumnName, cursor.getString(i));
                        } else {
                            //rowObject.put(cursor.getColumnName(i), "");
                        }
                    } catch (Exception e) {
                    }
                }
            }
            resultSet.put(rowObject);
            cursor.moveToNext();
            index = index + 1;
        }

        cursor.close();
        return resultSet;
    }

    /**
     *  차량 번호로 첫번째 사진 저장 경로를 가져온다
     *  @param sCarNum : 차량 번호
     *  @param nMenuType : 상세 / 무평가 차량 타입
     */
    public String getPhotoPathByCarNumberNMenuType(String sCarNum, int nMenuType) {
        // 읽기가 가능하게 DB 열기
        SQLiteDatabase db = getReadableDatabase();

        // DB에 있는 데이터를 쉽게 처리하기 위해 Cursor를 사용하여 테이블에 있는 데이터 출력
        String query = "";
        // 촬영부위(guide_side)가 외부(0) 또는 내부(1) 인 경우
        if(nMenuType == Common.MENU_TYPE_DETAIL_SHOOT) {

            query = "SELECT * FROM " + tb_name + " INNER JOIN " + Common.TABLE_GUIDE + " ON " + tb_name + ".guide_code = " + Common.TABLE_GUIDE + ".guide_code" +
                    " WHERE car_id = ?" + " AND " + Common.TABLE_GUIDE + ".guide_side != ?";
        }
        // 촬영부위(guide_side)가 무평가(2) 인 경우
        else if(nMenuType == Common.MENU_TYPE_PASS_EVALUATION_SHOOT) {

            query = "SELECT * FROM " + tb_name + " INNER JOIN " + Common.TABLE_GUIDE + " ON " + tb_name + ".guide_code = " + Common.TABLE_GUIDE + ".guide_code" +
                    " WHERE car_id = ?" + " AND " + Common.TABLE_GUIDE + ".guide_side = ?";
        }
        String[] selectionArgs = {sCarNum, String.valueOf(Common.MENU_TYPE_PASS_EVALUATION_SHOOT)};

        Cursor cursor = db.rawQuery(query, selectionArgs);
        JSONArray resultSet = new JSONArray();
        cursor.moveToPosition(0);

        String photoPath = "";
        int nSize = cursor.getCount();
        if (nSize > 0) {
            int totalColumn = cursor.getColumnCount();
            JSONObject rowObject = new JSONObject();

            for (int i = 0; i < totalColumn; i++) {
                if (cursor.getColumnName(i) != null) {
                    try {
                        if (cursor.getString(i) != null) {
                            rowObject.put(cursor.getColumnName(i), cursor.getString(i));
                        } else {
                            rowObject.put(cursor.getColumnName(i), "");
                        }
                    } catch (Exception e) {
                    }
                }
            }
            try {
                String strPicFileName = rowObject.getString("photo_path");
                if(strPicFileName.length() > 0) {
                    photoPath = Util.getPhotoFilePath() + strPicFileName;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        cursor.close();
        return photoPath;
    }

    // 차량 번호와 가이드 코드로 사진 데이터를 JSONObject로 얻어온다
    public JSONObject getResultByCarNumberNGuideCode(String sCarNum, String sGuideCode) {
        // 읽기가 가능하게 DB 열기
        SQLiteDatabase db = getReadableDatabase();
        String result = "";

        // DB에 있는 데이터를 쉽게 처리하기 위해 Cursor를 사용하여 테이블에 있는 데이터 출력
        String query = "SELECT * FROM " + tb_name + " WHERE car_id = ?" + " AND guide_code = ?";
        Cursor cursor = db.rawQuery(query, new String[]{sCarNum, sGuideCode});
        cursor.moveToPosition(0);

        int totalColumn = cursor.getColumnCount();
        JSONObject rowObject = new JSONObject();

        for (int i = 0; i < totalColumn; i++) {
            if (cursor.getColumnName(i) != null) {
                try {
                    if (cursor.getString(i) != null) {
                        rowObject.put(cursor.getColumnName(i), cursor.getString(i));
                    } else {
                        rowObject.put(cursor.getColumnName(i), "");
                    }
                } catch (Exception e) {

                }
            }
        }
        cursor.close();
        db.close();
        return rowObject;
    }

    //Primary key인 id를 이용해서 검색 후 삭제
    public void delete(int item) throws JSONException {

        // 입력한 항목과 일치하는 행 삭제
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("DELETE FROM " + tb_name + " WHERE _id = " + item + ";");
        db.close();
    }

    //car_id 를 이용해서 검색 후 삭제
    public void deleteByCarNumber(String car_id) throws JSONException {

        // 입력한 항목과 일치하는 행 삭제
        SQLiteDatabase db = getWritableDatabase();
        /*String query = "DELETE * FROM " + tb_name + " WHERE car_id = ?";
        db.rawQuery(query, new String[] { car_id });*/
        db.execSQL("DELETE FROM " + tb_name + " WHERE car_id = '" + car_id + "';");
        db.close();
    }

    //모든 데이터 삭제
    public void removeAll() {
        SQLiteDatabase db = getReadableDatabase(); // helper is object extends SQLiteOpenHelper
        db.execSQL("DROP TABLE IF EXISTS " + tb_name);

        db.execSQL("CREATE TABLE IF NOT EXISTS " + tb_name + " (_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "car_id VARCHAR2(100), " +                    // 차량ID
                "guide_code VARCHAR2(100), " +                // 촬영 가이드 코드
                "photo_path VARCHAR2(100), " +                // 사진 저장 경로
                "photo_uri VARCHAR2(100)" +                   // 사진 저장 Uri
                ");");
    }
}
