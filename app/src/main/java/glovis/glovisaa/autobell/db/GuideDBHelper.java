package glovis.glovisaa.autobell.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import glovis.glovisaa.autobell.util.Common;

/**
 * 촬영 가이드 DB관리 클래스
 */

public class GuideDBHelper extends SQLiteOpenHelper {

    Context context;
    String tb_name;
    int version;
    SQLiteDatabase.CursorFactory factory;

    // GuideDBHelper 생성자로 관리할 DB 이름과 버전 정보를 받음
    public GuideDBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
        this.context = context;
        tb_name = Common.TABLE_GUIDE;
        this.version = version;
        this.factory = factory;
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("CREATE TABLE IF NOT EXISTS " + tb_name + " (_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "guide_side INT, " +                // 촬영 부위 (외/내부/무평가, 0:1:2)
                "guide_sort_no INT, " +             // 촬영 가이드 정렬 번호 (촬영 가이드 선택 스크롤에 보이는 순서)
                "guide_code VARCHAR2(100), " +      // 촬영 가이드 코드
                "guide_name VARCHAR2(100), " +      // 촬영 가이드 명
                "photo_code VARCHAR2(100), " +      // 촬영 사진 코드
                "evalu_sort_no INT);");             // 촬영 가이드 정렬 번호 (평가사 라이브샷 용)
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    //데이터 베이스 데이터의 갯수를 출력
    public long getDataSize() {
        SQLiteDatabase db = this.getReadableDatabase();
        long count = DatabaseUtils.queryNumEntries(db, tb_name);
        db.close();
        return count;
    }

    //새로운 데이터 입력
    public void insert(int guide_side, int guide_sort_no, String guide_code, String guide_name, String photo_code, int evalu_sort_no) {

        // 읽고 쓰기가 가능하게 DB 열기
        SQLiteDatabase db = getWritableDatabase();
        // DB에 입력한 값으로 행 추가
        ContentValues values = new ContentValues();
        values.put("guide_side", guide_side);
        values.put("guide_sort_no", guide_sort_no);
        values.put("guide_code", guide_code);
        values.put("guide_name", guide_name);
        values.put("photo_code", photo_code);
        values.put("evalu_sort_no", evalu_sort_no);

        db.insert(tb_name, null, values);
        db.close();
    }

    //기존에 존재하는 데이터 업데이트
    public void update(int id, int guide_side, int guide_sort_no, String guide_code, String guide_name, String photo_code, int evalu_sort_no) {

        // 읽고 쓰기가 가능하게 DB 열기
        SQLiteDatabase db = getWritableDatabase();
        // DB에 입력한 값으로 행 추가
        ContentValues values = new ContentValues();
        values.put("guide_side", guide_side);
        values.put("guide_sort_no", guide_sort_no);
        values.put("guide_code", guide_code);
        values.put("guide_name", guide_name);
        values.put("photo_code", photo_code);
        values.put("evalu_sort_no", evalu_sort_no);

        db.update(tb_name, values, "_id=" + id, null);
        db.close();
    }

    //특정 id를 갖는 데이터를 json으로 출력
    public JSONObject getData(int id) {
        Log.i("ID", String.valueOf(id));
        SQLiteDatabase db = getReadableDatabase();
        String query = "SELECT * FROM " + tb_name + " WHERE _id = ?";
        Cursor cursor = db.rawQuery(query, new String[]{String.valueOf(id)});
        cursor.moveToPosition(0);

        int totalColumn = cursor.getColumnCount();
        JSONObject rowObject = new JSONObject();

        for (int i = 0; i < totalColumn; i++) {
            if (cursor.getColumnName(i) != null) {
                try {
                    if (cursor.getString(i) != null) {
                        rowObject.put(cursor.getColumnName(i), cursor.getString(i));
                    } else {
                        rowObject.put(cursor.getColumnName(i), "");
                    }
                } catch (Exception e) {

                }
            }
        }
        cursor.close();
        db.close();
        return rowObject;
    }

    // 외부 / 내부 / 무평가 / 360도 촬영 가이드 데이터를 json array로 얻어온다 (셀프평가, 무평가 용)
    public JSONArray getGuideArrayByType(int nSide) {
        // 읽기가 가능하게 DB 열기
        SQLiteDatabase db = getReadableDatabase();
        String result = "";

        // DB에 있는 데이터를 쉽게 처리하기 위해 Cursor를 사용하여 테이블에 있는 모든 외부촬영 가이드 데이터 출력
        String query = "SELECT * FROM " + tb_name + " WHERE guide_side = ?" + "ORDER BY guide_sort_no";
        Cursor cursor = db.rawQuery(query, new String[]{String.valueOf(nSide)});

        JSONArray resultSet = new JSONArray();
        cursor.moveToPosition(0);
        int index = 0;

        int number_of_data = cursor.getCount();

        while (index < number_of_data) {

            int totalColumn = cursor.getColumnCount();
            JSONObject rowObject = new JSONObject();

            for (int i = 0; i < totalColumn; i++) {
                if (cursor.getColumnName(i) != null) {
                    try {
                        if (cursor.getString(i) != null) {
                            rowObject.put(cursor.getColumnName(i), cursor.getString(i));
                        } else {
                            rowObject.put(cursor.getColumnName(i), "");
                        }
                    } catch (Exception e) {
                    }
                }
            }
            resultSet.put(rowObject);
            cursor.moveToNext();
            index = index + 1;
        }

        cursor.close();
        return resultSet;
    }

    // 외부 / 내부 촬영 가이드 데이터를 json array로 얻어온다 (평가사 라이브샷 용)
    public JSONArray getGuideArrayByTypeForEval(int nSide) {
        // 읽기가 가능하게 DB 열기
        SQLiteDatabase db = getReadableDatabase();
        String result = "";

        // DB에 있는 데이터를 쉽게 처리하기 위해 Cursor를 사용하여 테이블에 있는 모든 외부촬영 가이드 데이터 출력
        String query = "SELECT * FROM " + tb_name + " WHERE guide_side = ?" + "ORDER BY evalu_sort_no";
        Cursor cursor = db.rawQuery(query, new String[]{String.valueOf(nSide)});

        JSONArray resultSet = new JSONArray();
        cursor.moveToPosition(0);
        int index = 0;

        int number_of_data = cursor.getCount();

        while (index < number_of_data) {

            int totalColumn = cursor.getColumnCount();
            JSONObject rowObject = new JSONObject();

            for (int i = 0; i < totalColumn; i++) {
                if (cursor.getColumnName(i) != null) {
                    try {
                        if (cursor.getString(i) != null) {
                            rowObject.put(cursor.getColumnName(i), cursor.getString(i));
                        } else {
                            rowObject.put(cursor.getColumnName(i), "");
                        }
                    } catch (Exception e) {
                    }
                }
            }
            resultSet.put(rowObject);
            cursor.moveToNext();
            index = index + 1;
        }

        cursor.close();
        return resultSet;
    }

    // 모든 데이터를 json array로 얻어온다
    public JSONArray getAllResult() {
        // 읽기가 가능하게 DB 열기
        SQLiteDatabase db = getReadableDatabase();
        String result = "";

        // DB에 있는 데이터를 쉽게 처리하기 위해 Cursor를 사용하여 테이블에 있는 모든 데이터 출력
        Cursor cursor = db.rawQuery("SELECT * FROM " + tb_name, null);
        JSONArray resultSet = new JSONArray();
        cursor.moveToPosition(0);
        int index = 0;

        int number_of_data = (int) DatabaseUtils.queryNumEntries(db, tb_name);

        while (index < number_of_data) {

            int totalColumn = cursor.getColumnCount();
            JSONObject rowObject = new JSONObject();

            for (int i = 0; i < totalColumn; i++) {
                if (cursor.getColumnName(i) != null) {
                    try {
                        if (cursor.getString(i) != null) {
                            rowObject.put(cursor.getColumnName(i), cursor.getString(i));
                        } else {
                            rowObject.put(cursor.getColumnName(i), "");
                        }
                    } catch (Exception e) {
                    }
                }
            }
            resultSet.put(rowObject);
            cursor.moveToNext();
            index = index + 1;
        }

        cursor.close();
        return resultSet;
    }

    //시작 id부터 일정 갯수의 데이터를 json array로 얻어온다
    public JSONArray getResult(int start_index, int number_of_data) {
        // 읽기가 가능하게 DB 열기
        SQLiteDatabase db = getReadableDatabase();
        String result = "";

        // DB에 있는 데이터를 쉽게 처리하기 위해 Cursor를 사용하여 테이블에 있는 모든 데이터 출력
        Cursor cursor = db.rawQuery("SELECT * FROM " + tb_name, null);
        JSONArray resultSet = new JSONArray();
        cursor.moveToPosition(start_index);
        int index = 0;
        while (index < number_of_data) {

            int totalColumn = cursor.getColumnCount();
            JSONObject rowObject = new JSONObject();

            for (int i = 0; i < totalColumn; i++) {
                if (cursor.getColumnName(i) != null) {
                    try {
                        if (cursor.getString(i) != null) {
                            rowObject.put(cursor.getColumnName(i), cursor.getString(i));
                        } else {
                            rowObject.put(cursor.getColumnName(i), "");
                        }
                    } catch (Exception e) {

                    }
                }

            }
            resultSet.put(rowObject);
            cursor.moveToNext();
            index = index + 1;
        }

        cursor.close();
        return resultSet;
    }

    //최근 데이터로 부터 일정 갯수의 데이터를 json array 로 얻어온다.
    public JSONArray getRecentData(int number_of_data) {
        // 읽기가 가능하게 DB 열기
        SQLiteDatabase db = getReadableDatabase();
        String result = "";

        // DB에 있는 데이터를 쉽게 처리하기 위해 Cursor를 사용하여 테이블에 있는 모든 데이터 출력
        Cursor cursor = db.rawQuery("SELECT * FROM " + tb_name, null);
        JSONArray resultSet = new JSONArray();
        cursor.moveToLast();
        int index = 0;
        while (index < number_of_data) {

            int totalColumn = cursor.getColumnCount();
            JSONObject rowObject = new JSONObject();

            for (int i = 0; i < totalColumn; i++) {
                if (cursor.getColumnName(i) != null) {
                    try {
                        if (cursor.getString(i) != null) {
                            rowObject.put(cursor.getColumnName(i), cursor.getString(i));
                        } else {
                            rowObject.put(cursor.getColumnName(i), "");
                        }
                    } catch (Exception e) {

                    }
                }

            }
            resultSet.put(rowObject);
            cursor.moveToPrevious();
            index = index + 1;
        }

        cursor.close();
        return resultSet;
    }

    //Primary key인 id를 이용해서 검색 후 삭제
    public void delete(int item) throws JSONException {

        // 입력한 항목과 일치하는 행 삭제
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("DELETE FROM " + tb_name + " WHERE _id= " + item + ";");
        db.close();
    }

    //모든 데이터 삭제
    public void removeAll() {
        SQLiteDatabase db = getReadableDatabase(); // helper is object extends SQLiteOpenHelper
        db.execSQL("DROP TABLE " + tb_name);

        db.execSQL("CREATE TABLE IF NOT EXISTS " + tb_name + " (_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "guide_side INT, " +                // 촬영 부위 (외/내부/무평가, 0:1:2)
                "guide_sort_no INT, " +             // 촬영 가이드 정렬 번호 (촬영 가이드 선택 스크롤에 보이는 순서)
                "guide_code VARCHAR2(100), " +      // 촬영 가이드 코드
                "guide_name VARCHAR2(100), " +      // 촬영 가이드 명
                "photo_code VARCHAR2(100), " +      // 촬영 사진 코드
                "evalu_sort_no INT);");             // 촬영 가이드 정렬 번호 (평가사 라이브샷 용)
    }

}
