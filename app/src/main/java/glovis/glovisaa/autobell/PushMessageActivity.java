package glovis.glovisaa.autobell;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import glovis.glovisaa.autobell.service.OhMyCallService;
import glovis.glovisaa.autobell.util.Common;
import m.client.push.library.PushManager;
import m.client.push.library.common.PushConstants;

public class PushMessageActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		String jsonMessage = this.getIntent().getStringExtra("JSON");
		String message = this.getIntent().getStringExtra("EXT");
		String title = this.getIntent().getStringExtra("TITLE");
		
		try {
			JSONObject jsonMsg = new JSONObject(jsonMessage);
	    	PushManager.getInstance().pushMessageReadConfirm(this, jsonMessage);
			
			ArrayList<String> params = null;
			if (message != null) {
				String[] paramArray = message.split("\\|");
				params = new ArrayList<String>(Arrays.asList(paramArray));
			}
			
			if (params != null && params.size() > 0) {
				if (params.get(0).equals("0")) {
					JSONObject jsonObject = new JSONObject(message);
					String webpage_url = jsonObject.getString("webpage_url");
					Intent dialogIntent = new Intent(getApplicationContext(), MainActivity.class);
					dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
					dialogIntent.putExtra("webpage_url", webpage_url);
					startActivity(dialogIntent);
//					Intent intent = new Intent(this, PushDetailActivity.class);
//					intent.putExtra("JSON", jsonMsg.toString());
//					intent.putExtra("type", params.get(0));
//					intent.putExtra("title", title);
//			    	intent.putExtra("message", params.get(1));
//			    	startActivity(intent);
				}
				else if (params.get(0).equals("1")) {
					try {

					}catch (Exception e){

					}

					String contentUrl = "";
					if(params.size() > 3){
						contentUrl = params.get(3);
					}
					if (TextUtils.isEmpty(contentUrl)) { // 단순 이미지만 존재하는 메세지
						JSONObject jsonObject = new JSONObject(message);
						String webpage_url = jsonObject.getString("webpage_url");
						Intent dialogIntent = new Intent(getApplicationContext(), MainActivity.class);
						dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
						dialogIntent.putExtra("webpage_url", webpage_url);
						startActivity(dialogIntent);
//						Intent intent = new Intent(this, PushDetailActivity.class);
//						intent.putExtra("JSON", jsonMsg.toString());
//						intent.putExtra("type", params.get(0));
//						intent.putExtra("title", title);
//				    	intent.putExtra("message", params.get(1));
//				    	intent.putExtra("img", params.get(2));
//				    	startActivity(intent);
					}
					else { // 컨텐츠가 있는 메세지 - 동영상/웹페이지
						Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(contentUrl));
						startActivity(intent);
					}
				}
				else if (params.get(0).contains("/")) {
					Intent dialogIntent = new Intent(getApplicationContext(), MainActivity.class);
					dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
					//rtnRoutingUrl
//					dialogIntent.putExtra("push_url", params.get(0));
					if(params.get(0).startsWith("http")){
						dialogIntent.putExtra("webpage_url", params.get(0));
					}else{
						dialogIntent.putExtra("webpage_url", Common.MOBILE_WEB_URL+params.get(0));
					}
					startActivity(dialogIntent);
				}
				else { // 타입이 없는 경우 사용자 정의

					JSONObject jsonObject = new JSONObject(message);
					String webpage_url = jsonObject.getString("webpage_url");
					Intent dialogIntent = new Intent(getApplicationContext(), MainActivity.class);
					dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
					dialogIntent.putExtra("webpage_url", webpage_url);
					startActivity(dialogIntent);
//					Intent intent = new Intent(this, PushDetailActivity.class);
//					intent.putExtra("JSON", jsonMsg.toString());
//					intent.putExtra("type", params.get(0));
//					intent.putExtra("title", title);
//			    	intent.putExtra("message", params.get(0));
//			    	startActivity(intent);
				}
			}
		} 
		catch (JSONException e) {
			e.printStackTrace();
		}
		
		finish();
	}
}
