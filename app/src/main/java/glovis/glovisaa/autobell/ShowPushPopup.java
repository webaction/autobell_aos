package glovis.glovisaa.autobell;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import glovis.glovisaa.autobell.notification.HttpGetStringThread;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;

import m.client.push.library.common.PushConstants;

public class ShowPushPopup extends Activity {
	@Override
	protected void onNewIntent(Intent intent) {
		System.out.println("ShowPushPopup onNewIntent()");
		setIntent(intent);
		init();
	}

	private Window window;
	private MediaPlayer mAudia = null;
	boolean isPlay = false;
	private String mMessage;
	private AlertDialog myAlertDialog;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		init();
	}

	private void init() {
		if (myAlertDialog != null && myAlertDialog.isShowing()) {
			myAlertDialog.dismiss();
		}
		window = getWindow();
		window.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
							/*| WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD*/
							| WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
		//PushWakeLock.acquireCpuWakeLock(this);
		
		final Intent intent = getIntent();
		final String title = intent.getStringExtra("TITLE");
		final String message = intent.getStringExtra("MESSAGE");
		
		final String extData = intent.getStringExtra("EXT");
		final String jsonData = intent.getStringExtra("JSON");
		final String psid = intent.getStringExtra("PSID");
		final String pushType = intent.getStringExtra("PUSH_TYPE");
		getMessageFromExtUrl(extData, jsonData);
		
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(ShowPushPopup.this);
		alertDialog.setPositiveButton("보기", new DialogInterface.OnClickListener() {
			@Override            
			public void onClick(DialogInterface dialog, int which) {
			    Intent intent = new Intent(ShowPushPopup.this, PushMessageActivity.class);
			    intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
			    intent.putExtra("JSON", jsonData);
			    intent.putExtra("PSID", psid);
			    intent.putExtra("EXT", mMessage);
			    intent.putExtra("TITLE", title);
			    intent.putExtra("PUSH_TYPE", pushType);
			    startActivity(intent);
			    
			    ShowPushPopup.this.finish(); 
			}
		});
		
		alertDialog.setNegativeButton("닫기", new DialogInterface.OnClickListener() {
			@Override            
			public void onClick(DialogInterface dialog, int which) {
				try {
					if (PushConstants.STR_UPNS_PUSH_TYPE.equals(pushType)) {
						//PushNotificationManager.createUpnsNotification(ShowPushPopup.this, intent);
					}
					else {
						//PushNotificationManager.createGcmNotification(ShowPushPopup.this, intent);
					}
				} 
				catch (Exception e) {
					e.printStackTrace();
				}
				
				ShowPushPopup.this.finish();     
			}
		}); 
	
		String alertMessage = title;
		alertDialog.setTitle(alertMessage);
		alertDialog.setMessage(mMessage); 
		
		myAlertDialog = alertDialog.create();
        myAlertDialog.show();
		
		//Vibrator vibe = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
		//vibe.vibrate(1000);
		
		int maner = 0;//Settings.System.getInt(getContentResolver(), Settings.System.VOLUME_ALARM, 15000);
		if (maner != 0) {
			play();
		}

		/*TimerTask task = new TimerTask() {                 
			@Override                
			public void run() {
				//PushWakeLock.releaseCpuLock();
				myAlertDialog.dismiss();
				ShowPushPopup.this.finish();
			}        
		};
		Timer timer = new Timer();
		timer.schedule(task, 8000);*/
	}
	
	public void setBright(float value) { 
        Window mywindow = getWindow(); 
        WindowManager.LayoutParams lp = mywindow.getAttributes(); 
        lp.screenBrightness = value; 
        mywindow.setAttributes(lp); 
    } 
	
	public void play() {
		try {
			mAudia = new MediaPlayer();

			Uri alert = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
			mAudia.setDataSource(alert.toString());

			mAudia.prepare();
			mAudia.setLooping(false);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		mAudia.start();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		
		int maner = 0;//Settings.System.getInt(getContentResolver(), Settings.System.VOLUME_SYSTEM, 1);
		if(maner != 0){
			mAudia.release();
		}
	}
	
	@SuppressLint("HandlerLeak") 
	public void getMessageFromExtUrl(String extUrl, final String jsonData) {
		mMessage = extUrl;
		if (extUrl.contains("http")) {
			Handler handler = new Handler() {
				@Override
				public void handleMessage(Message msg) {
	        		if(msg.what == 0) {
	        			String message = (String) msg.obj;
	        			System.out.println("getMessageFromExtUrl:: " + message);
	        			if (message != null) {
	        				message = message.replaceAll("https", "http");
	        				message = message.replaceAll("\\\\", "/");
	        				
	        				try { 
	        					// "TYPE":"R","VAR":"hoseok|2015-06-31|2015-07-30|104320|11"
	        					JSONObject jsonMsg = new JSONObject(jsonData);
	        					if (jsonMsg.has("TYPE") && jsonMsg.getString("TYPE").equals("R")) {
	        						String var = jsonMsg.getString("VAR");
	        						System.out.println("getMessageFromExtUrl:: var: " + var);
	        						HashMap<String, String> varMap = new HashMap<String, String>();
	        						if (var != null) {
	        							String[] varArray = var.split("\\|");
	        							for (int i=0; i<varArray.length; i++) {
	        								int idx = i+1;
	        								varMap.put("%VAR" + idx + "%", varArray[i]);
	        								Log.d("test", "%VAR" + idx + "%" + " " +  varArray[i]);
	        							}
	        							
	        							Iterator<?> keys = varMap.keySet().iterator();
	        							while (keys.hasNext()) {
	        								String key = (String) keys.next();
	        								message = message.replaceAll(key, varMap.get(key));
	        								Log.d("test", "message: " + message);
	        							}
	        						}
	        					}
	        					mMessage = message;
	        					
	        				} catch (JSONException e) {
								e.printStackTrace();
							}
						} 
	        		}
				}
			};
			new HttpGetStringThread(handler, extUrl).start();
		}
	}
}
