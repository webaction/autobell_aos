package glovis.glovisaa.autobell.bridge;

import static android.content.Context.CLIPBOARD_SERVICE;
import static android.content.Context.MODE_PRIVATE;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.UUID;

import glovis.glovisaa.autobell.BuildConfig;
import glovis.glovisaa.autobell.DetailShootActivity;
import glovis.glovisaa.autobell.MainActivity;
import glovis.glovisaa.autobell.OverallShootActivity;
import glovis.glovisaa.autobell.util.Common;
import glovis.glovisaa.autobell.util.Util;

public class AndroidBridge {

    private String TAG = Common.COMPANY + "AndroidBridge";
    final public Handler handler = new Handler();

    private WebView mWebView;
    private MainActivity mContext;

    private FirebaseAnalytics mFirebaseAnalytics;

    public AndroidBridge(WebView _mWebView, MainActivity _mContext) {

        mWebView = _mWebView;
        mContext = _mContext;
    }

    // 로그를 띄우는 메소드
    @JavascriptInterface
    public void call_log(final String _message) {
        Log.d(TAG, _message);
        Toast.makeText(mContext, "From Server : " + _message, Toast.LENGTH_SHORT);

        handler.post(new Runnable() {
            @Override
            public void run() {
                mWebView.loadUrl("javascript:alert('[" + _message + "] 라고 로그를 남겼습니다.')");
            }
        });
    }

    // uuid 리턴
    @JavascriptInterface
    public void getAppUuid() {
        SharedPreferences pref = mContext.getSharedPreferences(Common.PREFERENCE, MODE_PRIVATE);
        String uuid = pref.getString(Common.PARAM_UUID, "A"+ UUID.randomUUID().toString());
        SharedPreferences.Editor edit = pref.edit();
        edit.putString(Common.PARAM_UUID, uuid);
        edit.commit();
        edit.apply();
        mContext.registerPushServiceAndUser();
        mWebView.post(new Runnable() {
            @Override
            public void run() {
                mWebView.loadUrl("javascript:rtnGetUuid('" + uuid + "')");

//                android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(mContext);
//                alertDialog.setPositiveButton("클립보드로 복사", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        dialog.dismiss();
//
//                        ClipboardManager clipboard = (ClipboardManager) mContext.getSystemService(CLIPBOARD_SERVICE);
//                        ClipData clip = ClipData.newPlainText(uuid, uuid);
//                        clipboard.setPrimaryClip(clip);
//                    }
//                });
//                alertDialog.setTitle("uuid");
//                alertDialog.setMessage(uuid);
//                Dialog dialog = alertDialog.create();
//                dialog.setCancelable(false);
//                dialog.show();
            }
        });
    }

    @JavascriptInterface
    public void syncPushSignIn(final String _userID) {
        String phoneNum = _userID;
        if(phoneNum.length() > 1) {
            Log.d(TAG, "syncPushSignIn() _userID : " + phoneNum);
            SharedPreferences pref = mContext.getSharedPreferences(Common.PREFERENCE, MODE_PRIVATE);
            String user_ID = pref.getString(Common.PARAM_USERID, "");  // def 값은 테스트 ID : ClientUID001Android
            // 기존 등록 아이디(폰번호)와 같으면 (재로그인) 푸시 서버에 유저등록하지 않음
            if (user_ID.equals(phoneNum)) {
                Log.d(TAG, "syncPushSignIn() _userID equals push registerd CUID");
                return;
            } else {
                SharedPreferences.Editor editor = pref.edit();
                editor.putString(Common.PARAM_USERID, phoneNum);
                editor.commit();
                mContext.registerPushServiceAndUser();
                //Toast.makeText(mContext, "syncPushSignIn ID : " + _userID, Toast.LENGTH_LONG).show();
            }
        }else {
            Log.e(TAG, "syncPushSignIn() => Fail to ger phone number");
        }
    }

    // 로그아웃 시(로그아웃 완료시)
    @JavascriptInterface
    public void syncPushSignOut() {
        Log.d(TAG, "syncPushSignOut()");
        Toast.makeText(mContext, "syncPushSignOut()", Toast.LENGTH_LONG).show();
    }

    // 앱 버전 리턴
    @JavascriptInterface
    public void getVersionCode() {
        Log.d(TAG, "getVersionCode()");
        handler.post(new Runnable() {
            @Override
            public void run() {
                String version = BuildConfig.VERSION_NAME;
                try {
                    PackageInfo i = mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), 0);
                    version = i.versionName;
                } catch(PackageManager.NameNotFoundException e) {

                }
                mWebView.loadUrl("javascript:rtnVersionCode('" + version + "')");
            }
        });
    }

    // 촬영 화면으로 이동하는 메소드
    @JavascriptInterface
    public void openShootingAssistant(final String _user_type, final String _car_id, final String _car_type, final String _car_number, final String _shoot_type) {
        String strLog =  "user_type : " + _user_type + ", car_id : " + _car_id  + ", car_type : " + _car_type + ", car_number : " + _car_number + ", shoot_type : " + _shoot_type;
        Log.d(TAG, strLog);
        try {
            Intent intent = null;
            // 촬영종류 : detail (0) / 360 (3)
            int shootType = Integer.parseInt(_shoot_type);
            if(shootType == 0) {
                intent = new Intent(mContext, DetailShootActivity.class);
            }else if(shootType == 3) {
                intent = new Intent(mContext, OverallShootActivity.class);
            }
            int userType = Integer.parseInt(_user_type);
            intent.putExtra("user_type", userType);
            intent.putExtra("car_id", _car_id);
            int carType = Integer.parseInt(_car_type);
            intent.putExtra("car_type", carType);
            intent.putExtra("car_number", _car_number);
            mContext.startActivityForResult(intent, Common.REQ_SHOOT_ACT);

        }catch (NumberFormatException nfe) {
            System.out.println("Could not parse " + nfe);
            Log.e(TAG, "Could not parse " + nfe);
        }
    }

    @SuppressLint("InvalidAnalyticsName")
    @JavascriptInterface
    public void gaCall(String eventName,String userId, String carNo, String userType) {
        Log.d(TAG, "mFirebaseAnalytics" + eventName +"//"+ userId +"//"+ carNo +"//"+ userType);
        SharedPreferences pref = mContext.getSharedPreferences(Common.PREFERENCE, MODE_PRIVATE);
        String uuid = pref.getString(Common.PARAM_UUID, "A"+ UUID.randomUUID().toString());
        SharedPreferences.Editor edit = pref.edit();
        edit.putString(Common.PARAM_UUID, uuid);
        edit.commit();
        edit.apply();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(mContext);
        Bundle params = new Bundle();
        Log.d(TAG,"mFirebaseAnalytics/userId/"+ userId);
        if(null!=userId){
            if(null!=userType && !"".equals(userType)){
                if("null".equals(userId)||"undefined".equals(userId)){
                    mFirebaseAnalytics.setUserId(uuid);
                    mFirebaseAnalytics.setUserProperty("userId", uuid.replace("-",""));
                    Log.d(TAG,"mFirebaseAnalytics2/userId/"+ uuid.replace("-",""));
                }else{
                    mFirebaseAnalytics.setUserId(userId);
                    mFirebaseAnalytics.setUserProperty("userId", userId);
                    Log.d(TAG,"mFirebaseAnalytics3/userId/"+ userId);
                }
            }else{
                mFirebaseAnalytics.setUserId(uuid);
                mFirebaseAnalytics.setUserProperty("userId", uuid.replace("-",""));
                Log.d(TAG,"mFirebaseAnalytics1/userId/"+ uuid.replace("-",""));
            }
        }else{
            mFirebaseAnalytics.setUserId(uuid);
            mFirebaseAnalytics.setUserProperty("userId", uuid.replace("-",""));
            Log.d(TAG,"mFirebaseAnalytics4/userId/"+ uuid.replace("-",""));
        }
        if(null!=carNo && !"".equals(carNo)) {
            if(!"null".equals(carNo)){
                params.putString("carNo", carNo);
                mFirebaseAnalytics.setUserProperty("carNo", carNo);
                Log.d(TAG,"mFirebaseAnalytics2/carNo/"+ carNo);
            }
        }
        if(null!=userType && !"".equals(userType)) {
            if(!"null".equals(userType)){
                params.putString("userType", userType);
                Log.d(TAG,"mFirebaseAnalytics2/userType/"+ userType);
            }
        }
        mFirebaseAnalytics.logEvent(eventName/*+"_A"*/,params);
    }

    @SuppressLint("InvalidAnalyticsName")
    @JavascriptInterface
    public void gaCallPricing(String eventName,String userId, String userType, String carNo, String sellPrice) {
        Log.d(TAG, "mFirebaseAnalytics" + eventName +"//"+ userId +"//"+ carNo +"//"+ userType +"//"+ sellPrice);
        SharedPreferences pref = mContext.getSharedPreferences(Common.PREFERENCE, MODE_PRIVATE);
        String uuid = pref.getString(Common.PARAM_UUID, "A"+ UUID.randomUUID().toString());
        SharedPreferences.Editor edit = pref.edit();
        edit.putString(Common.PARAM_UUID, uuid);
        edit.commit();
        edit.apply();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(mContext);
        Bundle params = new Bundle();
        if(null!=userId){
            if(null!=userType && !"".equals(userType)){
                if("null".equals(userId)||"undefined".equals(userId)){
                    mFirebaseAnalytics.setUserId(uuid);
                    mFirebaseAnalytics.setUserProperty("userId", uuid.replace("-",""));
                    Log.d(TAG,"mFirebaseAnalytics2/userId/"+ uuid.replace("-",""));
                }else{
                    mFirebaseAnalytics.setUserId(userId);
                    mFirebaseAnalytics.setUserProperty("userId", userId);
                    Log.d(TAG,"mFirebaseAnalytics3/userId/"+ userId);
                }
            }else{
                mFirebaseAnalytics.setUserId(uuid);
                mFirebaseAnalytics.setUserProperty("userId", uuid.replace("-",""));
                Log.d(TAG,"mFirebaseAnalytics1/userId/"+ uuid.replace("-",""));
            }
        }else{
            mFirebaseAnalytics.setUserId(uuid);
            mFirebaseAnalytics.setUserProperty("userId", uuid.replace("-",""));
            Log.d(TAG,"mFirebaseAnalytics4/userId/"+ uuid.replace("-",""));
        }
        if(null!=carNo && !"".equals(carNo)) {
            if(!"null".equals(carNo)){
                params.putString("carNo", carNo);
                mFirebaseAnalytics.setUserProperty("carNo", carNo);
                Log.d(TAG,"mFirebaseAnalytics2/carNo/"+ carNo);
            }
        }
        if(null!=userType && !"".equals(userType)) {
            if(!"null".equals(userType)){
                params.putString("userType", userType);
                Log.d(TAG,"mFirebaseAnalytics2/userType/"+ userType);
            }
        }
        if(null!=sellPrice && !"".equals(sellPrice)) {
            if(!"null".equals(sellPrice)){
                params.putString("sellPrice", sellPrice);
                Log.d(TAG,"mFirebaseAnalytics2/sellPrice/"+ sellPrice);
            }
        }
        mFirebaseAnalytics.logEvent(eventName/*+"_A"*/,params);
    }

    @SuppressLint("InvalidAnalyticsName")
    @JavascriptInterface
    public void gaCallVisit(String eventName,String userId, String userType, String carNo, String rsNo, String adminNo) {
        Log.d(TAG, "mFirebaseAnalytics" + eventName +"//"+ userId +"//"+ carNo +"//"+ userType+"//"+ rsNo+"//"+ adminNo);
        SharedPreferences pref = mContext.getSharedPreferences(Common.PREFERENCE, MODE_PRIVATE);
        String uuid = pref.getString(Common.PARAM_UUID, "A"+ UUID.randomUUID().toString());
        SharedPreferences.Editor edit = pref.edit();
        edit.putString(Common.PARAM_UUID, uuid);
        edit.commit();
        edit.apply();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(mContext);
        Bundle params = new Bundle();
        if(null!=userId){
            if(null!=userType && !"".equals(userType)){
                if("null".equals(userId)||"undefined".equals(userId)){
                    mFirebaseAnalytics.setUserId(uuid);
                    mFirebaseAnalytics.setUserProperty("userId", uuid.replace("-",""));
                    Log.d(TAG,"mFirebaseAnalytics2/userId/"+ uuid.replace("-",""));
                }else{
                    mFirebaseAnalytics.setUserId(userId);
                    mFirebaseAnalytics.setUserProperty("userId", userId);
                    Log.d(TAG,"mFirebaseAnalytics3/userId/"+ userId);
                }
            }else{
                mFirebaseAnalytics.setUserId(uuid);
                mFirebaseAnalytics.setUserProperty("userId", uuid.replace("-",""));
                Log.d(TAG,"mFirebaseAnalytics1/userId/"+ uuid.replace("-",""));
            }
        }else{
            mFirebaseAnalytics.setUserId(uuid);
            mFirebaseAnalytics.setUserProperty("userId", uuid.replace("-",""));
            Log.d(TAG,"mFirebaseAnalytics4/userId/"+ uuid.replace("-",""));
        }
        if(null!=carNo && !"".equals(carNo)) {
            if(!"null".equals(carNo)){
                params.putString("carNo", carNo);
                mFirebaseAnalytics.setUserProperty("carNo", carNo);
                Log.d(TAG,"mFirebaseAnalytics2/carNo/"+ carNo);
            }
        }
        if(null!=userType && !"".equals(userType)) {
            if(!"null".equals(userType)){
                params.putString("userType", userType);
                Log.d(TAG,"mFirebaseAnalytics2/userType/"+ userType);
            }
        }
        if(null!=rsNo && !"".equals(rsNo)) {
            if(!"null".equals(rsNo)){
                params.putString("rsNo", rsNo);
                Log.d(TAG,"mFirebaseAnalytics2/rsNo/"+ rsNo);
            }
        }
        if(null!=adminNo && !"".equals(adminNo)) {
            if(!"null".equals(adminNo)){
                params.putString("adminNo", adminNo);
                Log.d(TAG,"mFirebaseAnalytics2/adminNo/"+ adminNo);
            }
        }
        mFirebaseAnalytics.logEvent(eventName/*+"_A"*/,params);
    }
    //231226
    @SuppressLint("InvalidAnalyticsName")
    @JavascriptInterface
    public void gaVisitReservation(String userType, String userId, String eventName, String funnel , String carNo, String itemName, String year, String mile) {
        Log.d(TAG, "mFirebaseAnalytics" + userType+"//"+ eventName+"//"+ funnel+"//"+ userId+"//"+ carNo+"//"+ itemName+"//"+ year+"//"+ mile);
        SharedPreferences pref = mContext.getSharedPreferences(Common.PREFERENCE, MODE_PRIVATE);
        String uuid = pref.getString(Common.PARAM_UUID, "A"+ UUID.randomUUID().toString());
        SharedPreferences.Editor edit = pref.edit();
        edit.putString(Common.PARAM_UUID, uuid);
        edit.commit();
        edit.apply();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(mContext);
        Bundle params = new Bundle();
        Log.d(TAG,"mFirebaseAnalytics0/userId/"+ userId);
        if(null!=userId){
            if(null!=userType && !"".equals(userType)){
                if("null".equals(userId)||"undefined".equals(userId)){
                    mFirebaseAnalytics.setUserId(uuid);
                    mFirebaseAnalytics.setUserProperty("userId", uuid.replace("-",""));
                    Log.d(TAG,"mFirebaseAnalytics2/userId/"+ uuid.replace("-",""));
                }else{
                    mFirebaseAnalytics.setUserId(userId);
                    mFirebaseAnalytics.setUserProperty("userId", userId);
                    Log.d(TAG,"mFirebaseAnalytics3/userId/"+ userId);
                }
            }else{
                mFirebaseAnalytics.setUserId(uuid);
                mFirebaseAnalytics.setUserProperty("userId", uuid.replace("-",""));
                Log.d(TAG,"mFirebaseAnalytics1/userId/"+ uuid.replace("-",""));
            }
        }else{
            mFirebaseAnalytics.setUserId(uuid);
            mFirebaseAnalytics.setUserProperty("userId", uuid.replace("-",""));
            Log.d(TAG,"mFirebaseAnalytics4/userId/"+ uuid.replace("-",""));
        }
        if(null!=carNo && !"".equals(carNo)) {
            if(!"null".equals(carNo)){
                params.putString("carNo", carNo);
                mFirebaseAnalytics.setUserProperty("carNo", carNo);
                Log.d(TAG,"mFirebaseAnalytics2/carNo/"+ carNo);
            }
        }
        if(null!=userType && !"".equals(userType)) {
            if(!"null".equals(userType)){
                params.putString("userType", userType);
                Log.d(TAG,"mFirebaseAnalytics2/userType/"+ userType);
            }
        }
        if(null!=funnel && !"".equals(funnel)) {
            if(!"null".equals(funnel)){
                params.putString("funnel", funnel);
                Log.d(TAG,"mFirebaseAnalytics2/funnel/"+ funnel);
            }
        }
        if(null!=itemName && !"".equals(itemName)) {
            if(!"null".equals(itemName)){
                params.putString("itemName", itemName);
                Log.d(TAG,"mFirebaseAnalytics2/itemName/"+ itemName);
            }
        }
        if(null!=year && !"".equals(year)) {
            if(!"null".equals(year)){
                params.putString("year", year);
                Log.d(TAG,"mFirebaseAnalytics2/year/"+ year);
            }
        }
        if(null!=mile && !"".equals(mile)) {
            if(!"null".equals(mile)){
                params.putString("mile", mile);
                Log.d(TAG,"mFirebaseAnalytics2/mile/"+ mile);
            }
        }
        params.putString("device", "APP");
        mFirebaseAnalytics.logEvent(eventName/*+"_A"*/,params);
        Log.d(TAG,"mFirebaseAnalytics/params/"+ params);
    }

    //memberType, eventName, funnel, userId, carNo, region, schedule
    @SuppressLint("InvalidAnalyticsName")
    @JavascriptInterface
    public void gaVisitReservEval(String userType, String userId, String eventName, String funnel, String carNo, String region, String schedule) {
        Log.d(TAG, "mFirebaseAnalytics" + userType+"//"+ eventName+"//"+ funnel+"//"+ userId+"//"+ carNo+"//"+ region+"//"+ schedule);
        SharedPreferences pref = mContext.getSharedPreferences(Common.PREFERENCE, MODE_PRIVATE);
        String uuid = pref.getString(Common.PARAM_UUID, "A"+ UUID.randomUUID().toString());
        SharedPreferences.Editor edit = pref.edit();
        edit.putString(Common.PARAM_UUID, uuid);
        edit.commit();
        edit.apply();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(mContext);
        Bundle params = new Bundle();
        if(null!=userId){
            if(null!=userType && !"".equals(userType)){
                if("null".equals(userId)||"undefined".equals(userId)){
                    mFirebaseAnalytics.setUserId(uuid);
                    mFirebaseAnalytics.setUserProperty("userId", uuid.replace("-",""));
                    Log.d(TAG,"mFirebaseAnalytics2/userId/"+ uuid.replace("-",""));
                }else{
                    mFirebaseAnalytics.setUserId(userId);
                    mFirebaseAnalytics.setUserProperty("userId", userId);
                    Log.d(TAG,"mFirebaseAnalytics3/userId/"+ userId);
                }
            }else{
                mFirebaseAnalytics.setUserId(uuid);
                mFirebaseAnalytics.setUserProperty("userId", uuid.replace("-",""));
                Log.d(TAG,"mFirebaseAnalytics1/userId/"+ uuid.replace("-",""));
            }
        }else{
            mFirebaseAnalytics.setUserId(uuid);
            mFirebaseAnalytics.setUserProperty("userId", uuid.replace("-",""));
            Log.d(TAG,"mFirebaseAnalytics4/userId/"+ uuid.replace("-",""));
        }
        if(null!=carNo && ""!=carNo) {
            if(!"null".equals(carNo)){
                params.putString("carNo", carNo);
                mFirebaseAnalytics.setUserProperty("carNo", carNo);
                Log.d(TAG,"mFirebaseAnalytics2/carNo/"+ carNo);
            }
        }
        if(null!=userType && !"".equals(userType)) {
            if(!"null".equals(userType)){
                params.putString("userType", userType);
                Log.d(TAG,"mFirebaseAnalytics2/userType/"+ userType);
            }
        }
        if(null!=funnel && !"".equals(funnel)) {
            if(!"null".equals(funnel)){
                params.putString("funnel", funnel);
                Log.d(TAG,"mFirebaseAnalytics2/funnel/"+ funnel);
            }
        }
        if(null!=region && !"".equals(region)) {
            if(!"null".equals(region)){
                params.putString("region", region);
                Log.d(TAG,"mFirebaseAnalytics2/region/"+ region);
            }
        }
        if(null!=schedule && !"".equals(schedule)) {
            if(!"null".equals(schedule)){
                params.putString("schedule", schedule);
                Log.d(TAG,"mFirebaseAnalytics2/schedule/"+ schedule);
            }
        }
        params.putString("device", "APP");
        mFirebaseAnalytics.logEvent(eventName/*+"_A"*/,params);
        Log.d(TAG,"mFirebaseAnalytics/params/"+ params);
    }

    //memberType, eventName, funnel, userId, carNo, itemName, rsNo, adminNo, affiliate
    @SuppressLint("InvalidAnalyticsName")
    @JavascriptInterface
    public void gaVisitReservComplete(String userType, String userId, String eventName, String funnel, String carNo, String itemName, String rsNo, String adminNo, String affiliate) {
        Log.d(TAG, "mFirebaseAnalytics" + userType+"//"+ eventName+"//"+ funnel+"//"+ userId+"//"+ carNo+"//"+ itemName+"//"+ rsNo+"//"+ adminNo+"//"+ affiliate);
        SharedPreferences pref = mContext.getSharedPreferences(Common.PREFERENCE, MODE_PRIVATE);
        String uuid = pref.getString(Common.PARAM_UUID, "A"+ UUID.randomUUID().toString());
        SharedPreferences.Editor edit = pref.edit();
        edit.putString(Common.PARAM_UUID, uuid);
        edit.commit();
        edit.apply();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(mContext);
        Bundle params = new Bundle();
        if(null!=userId){
            if(null!=userType && !"".equals(userType)){
                if("null".equals(userId)||"undefined".equals(userId)){
                    mFirebaseAnalytics.setUserId(uuid);
                    mFirebaseAnalytics.setUserProperty("userId", uuid.replace("-",""));
                    Log.d(TAG,"mFirebaseAnalytics2/userId/"+ uuid.replace("-",""));
                }else{
                    mFirebaseAnalytics.setUserId(userId);
                    mFirebaseAnalytics.setUserProperty("userId", userId);
                    Log.d(TAG,"mFirebaseAnalytics3/userId/"+ userId);
                }
            }else{
                mFirebaseAnalytics.setUserId(uuid);
                mFirebaseAnalytics.setUserProperty("userId", uuid.replace("-",""));
                Log.d(TAG,"mFirebaseAnalytics1/userId/"+ uuid.replace("-",""));
            }
        }else{
            mFirebaseAnalytics.setUserId(uuid);
            mFirebaseAnalytics.setUserProperty("userId", uuid.replace("-",""));
            Log.d(TAG,"mFirebaseAnalytics4/userId/"+ uuid.replace("-",""));
        }

        if(null!=carNo && !"".equals(carNo)) {
            if(!"null".equals(carNo)){
                params.putString("carNo", carNo);
                mFirebaseAnalytics.setUserProperty("carNo", carNo);
                Log.d(TAG,"mFirebaseAnalytics2/carNo/"+ carNo);
            }
        }
        if(null!=userType && !"".equals(userType)) {
            if(!"null".equals(userType)){
                params.putString("userType", userType);
                Log.d(TAG,"mFirebaseAnalytics2/userType/"+ userType);
            }
        }
        if(null!=funnel && !"".equals(funnel)) {
            if(!"null".equals(funnel)){
                params.putString("funnel", funnel);
                Log.d(TAG,"mFirebaseAnalytics2/funnel/"+ funnel);
            }
        }
        if(null!=itemName && !"".equals(itemName)) {
            if(!"null".equals(itemName)){
                params.putString("itemName", itemName);
                Log.d(TAG,"mFirebaseAnalytics2/itemName/"+ itemName);
            }
        }
        if(null!=rsNo && !"".equals(rsNo)) {
            if(!"null".equals(rsNo)){
                params.putString("rsNo", rsNo);
                Log.d(TAG,"mFirebaseAnalytics2/rsNo/"+ rsNo);
            }
        }
        if(null!=adminNo && !"".equals(adminNo)) {
            if(!"null".equals(adminNo)){
                params.putString("adminNo", adminNo);
                Log.d(TAG,"mFirebaseAnalytics2/adminNo/"+ adminNo);
            }
        }
        if(null!=affiliate && !"".equals(affiliate)) {
            if(!"null".equals(affiliate)){
                params.putString("affiliate", affiliate);
                Log.d(TAG,"mFirebaseAnalytics2/affiliate/"+ affiliate);
            }
        }
        params.putString("device", "APP");

        String sp_rsNo = pref.getString("rsNo", "null");
        //rsNo 비교 확인 후 1회만 호출 하도록
        if(!rsNo.equals(sp_rsNo)){
            edit.putString("rsNo", rsNo);
            edit.commit();
            edit.apply();
            mFirebaseAnalytics.logEvent(eventName/*+"_A"*/,params);
        }
//        mFirebaseAnalytics.logEvent(eventName/*+"_A"*/,params);
        Log.d(TAG,"mFirebaseAnalytics/params/"+ params);
    }
}
