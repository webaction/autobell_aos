package glovis.glovisaa.autobell.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import glovis.glovisaa.autobell.notification.PushNotificationManager;
import glovis.glovisaa.autobell.service.OhMyCallService;
import glovis.glovisaa.autobell.util.Common;
import m.client.push.library.common.Logger;
import m.client.push.library.common.PushConstants;
import m.client.push.library.common.PushLog;

/**
 * MessageArrivedReceiver
 * 메세지 수신 처리를 위해 등록된 리시버
 */
public class MessageArrivedReceiver extends BroadcastReceiver {

	public static final String TAG = Common.COMPANY + "MsgArrRev";

	@Override   
	public void onReceive(Context context, Intent intent) {
		PushLog.d("PushLog - onReceive", "[UPNSCallback] receive action");
		Log.d(TAG, "onReceive => [UPNSCallback] receive action");
		// UPNS 타입의 경우
		if (intent.getAction().equals(context.getPackageName() + PushConstants.ACTION_UPNS_MESSAGE_ARRIVED)) { 
			try {
				Log.d(TAG, "onReceive => ACTION_UPNS_MESSAGE_ARRIVED");
				/*String jsonData = intent.getExtras().getString(PushConstants.KEY_JSON);
				JSONObject jsonMsg = new JSONObject(jsonData);
				String msgID = intent.getExtras().getString(PushConstants.KEY_UPNSMESSAGEID);*/

				String rawData = intent.getExtras().getString(PushConstants.KEY_ORIGINAL_PAYLOAD_STRING);
				byte[] rawDataBytes = intent.getExtras().getByteArray(PushConstants.KEY_ORIGINAL_PAYLOAD_BYTES);

				Logger.i("PushLog - received raw data : " + rawData);
				Logger.i("PushLog - received bytes data : " + new String(rawDataBytes, "utf-8"));

				String jsonData = intent.getExtras().getString(PushConstants.KEY_JSON);
				String psid = intent.getExtras().getString(PushConstants.KEY_PSID);
				JSONObject jsonMsg = new JSONObject(jsonData);

				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				/*PushLog.d("PushLog - onReceive", "## received message: " + jsonData); // 수신 메세지 페이로드
				PushLog.d("PushLog - onReceive", "1. receive confirm msgIDForAck: " + msgID); // 수신 ACK를 위한 메세지 아이디
				PushLog.d("PushLog - onReceive", "2. received message SEQNO: " + jsonMsg.getString("SEQNO")); // 메세지 시퀀스 넘버
				PushLog.d("PushLog - onReceive", "3. received message MESSAGEID: " + jsonMsg.getString("MESSAGEID")); // UPNS 메세지 아이디
				//PushManager.getInstance().upnsMessageReceiveConfirm(context, msgID);
				//PushManager.getInstance().upnsSubscribe(context);*/
				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

				boolean hasExtData = false;
				String extData = "";
				String message = "";

				if (jsonMsg.has(PushConstants.KEY_EXT)) {
					extData = jsonMsg.getString(PushConstants.KEY_EXT);
					if (extData.length() > 1) {
						hasExtData = true;
					}else {
						hasExtData = false;
						message = jsonMsg.getString(PushConstants.KEY_MESSAGE);
					}

				}else {
					hasExtData = false;
					message = jsonMsg.getString(PushConstants.KEY_MESSAGE);
				}

				// 화면 온/오프 상태 체크
				if (PushNotificationManager.isRestrictedScreen(context)) {	// Lock screen 상태
					Log.d(TAG, "onReceive => ACTION_UPNS_MESSAGE_ARRIVED => Lock screen status");
					if (hasExtData) {
						// 오마이콜 팝업 창
						Log.d(TAG, "onReceive => showOhMyCallPopup()");
						showOhMyCallPopup(context, extData);
					}else {
						// 노티피케이션 생성
						Log.d(TAG, "onReceive => createNotification()");
						PushNotificationManager.createNotification(context, intent, PushConstants.STR_UPNS_PUSH_TYPE);
					}
				}
				else {		// Lock screen 아닌 상태
					Log.d(TAG, "onReceive => ACTION_UPNS_MESSAGE_ARRIVED => Non Lock screen status");
					// 토스트 발생
					//PushNotificationManager.showToast(context, intent, PushConstants.STR_UPNS_PUSH_TYPE);
					// 팝업 다이얼로그 예제
					//PushNotificationManager.showNotificationPopupDialog(context, intent, PushConstants.STR_UPNS_PUSH_TYPE);
					if (hasExtData) {
						// 오마이콜 팝업 창
						Log.d(TAG, "onReceive => showOhMyCallPopup()");
						showOhMyCallPopup(context, extData);
					}else {
						// 노티피케이션 생성
						Log.d(TAG, "onReceive => createNotification()");
						PushNotificationManager.createNotification(context, intent, PushConstants.STR_UPNS_PUSH_TYPE);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		// GCM 의 경우
		else if (intent.getAction().equals(context.getPackageName() + PushConstants.ACTION_GCM_MESSAGE_ARRIVED)) { 
			try {
				Log.d(TAG, "onReceive => ACTION_GCM_MESSAGE_ARRIVED");

				String jsonData = intent.getExtras().getString(PushConstants.KEY_JSON);
				JSONObject jsonMsg = new JSONObject(jsonData);
				if (jsonMsg.has(PushConstants.KEY_MPS.toLowerCase())){
					Log.d(TAG, "onReceive => ACTION_GCM_MESSAGE_ARRIVED + PushConstants.KEY_MPS.toLowerCase()");
					JSONObject mps = jsonMsg.getJSONObject(PushConstants.KEY_MPS.toLowerCase());
					if (mps.has(PushConstants.KEY_EXT.toLowerCase())) {
						//일반 푸시의 경우 이곳을 타도록 되어 있음
						Log.d(TAG, "onReceive => ACTION_GCM_MESSAGE_ARRIVED + PushConstants.KEY_MPS.toLowerCase() + PushConstants.KEY_EXT.toLowerCase()");
						String extData = mps.getString(PushConstants.KEY_EXT.toLowerCase());
						if(stringJsonObjectEnable(extData)){
							showOhMyCallPopup(context, extData);
						}else{
							PushNotificationManager.createNotification(context, intent, PushConstants.STR_GCM_PUSH_TYPE);
						}
					}else {
						Log.d(TAG, "onReceive => ACTION_GCM_MESSAGE_ARRIVED + PushConstants.KEY_MPS.toLowerCase() + PushConstants.KEY_EXT.toLowerCase() + else");
						// 노티피케이션 생성
						PushNotificationManager.createNotification(context, intent, PushConstants.STR_GCM_PUSH_TYPE);
					}
				}
				//showohmycall을 2회 호출하고 있어 주석처리함
//				if (jsonMsg.has(PushConstants.KEY_MPS)){
//					Log.d(TAG, "onReceive => ACTION_GCM_MESSAGE_ARRIVED + PushConstants.KEY_MPS");
//					JSONObject mps = jsonMsg.getJSONObject(PushConstants.KEY_MPS);
//					if (mps.has(PushConstants.KEY_EXT)) {
//						Log.d(TAG, "onReceive => ACTION_GCM_MESSAGE_ARRIVED + PushConstants.KEY_MPS + PushConstants.KEY_EXT");
//						String extData = mps.getString(PushConstants.KEY_EXT);
//						if(stringJsonObjectEnable(extData)){
//							showOhMyCallPopup(context, extData);
//						}
//					}else {
//						//일반 푸시의 경우 이곳을 타도록 되어 있음
//						Log.d(TAG, "onReceive => ACTION_GCM_MESSAGE_ARRIVED + PushConstants.KEY_MPS + PushConstants.KEY_EXT + else");
//						// 노티피케이션 생성
//						PushNotificationManager.createNotification(context, intent, PushConstants.STR_GCM_PUSH_TYPE);
//					}
//				}

				
				// 팝업 다이얼로그 예제
//				PushNotificationManager.showNotificationPopupDialog(context, intent, PushConstants.STR_GCM_PUSH_TYPE);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void showOhMyCallPopup(Context context, String extData) throws JSONException {

		new Handler().postDelayed(new Runnable()
		{
			@Override
			public void run()
			{
				Intent serviceIntent = new Intent(context, OhMyCallService.class);
				serviceIntent.putExtra(PushConstants.KEY_EXT, extData);

				Log.d(TAG, "showOhMyCallPopup() - extData = " + extData);
				context.startService(serviceIntent);
				//context.bindService(serviceIntent, null, Context.BIND_AUTO_CREATE);
				//딜레이 후 시작할 코드 작성
			}
		}, 1000);// 0.6초 정도 딜레이를 준 후 시작


	}

	// TODO [String 문자열 데이터를 Json Object 형식으로 변경 가능 한지 체크 실시]
	public static Boolean stringJsonObjectEnable(String data){
		boolean result = false;
		if (data != null
				&& data.length()>0
				&& data.trim().equals("") == false
				&& data.trim().equals("null") == false
				&& data.trim().equals("undefined") == false){ // [널이 아닌 경우]
			try {
				JSONObject checkJson = new JSONObject(String.valueOf(data));
				result = true;
			}
			catch (Exception e){
				result = false;
			}
		}
		else {
			result = false;
		}
		Log.d("//===========//","================================================");
		Log.i("","\n"+"[result :: "+String.valueOf(result)+"]");
		Log.d("//===========//","================================================");
		return result;
	}
}
