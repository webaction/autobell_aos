package glovis.glovisaa.autobell.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import glovis.glovisaa.autobell.util.Common;
import m.client.push.library.common.Logger;
import m.client.push.library.common.PushConstants;
import m.client.push.library.common.PushConstantsEx;

public class PushActionReceiver extends BroadcastReceiver {

	public static final String TAG = Common.COMPANY + "PushActRev";

	@Override   
	public void onReceive(Context context, Intent intent) {
		Log.d(TAG, "PushActionReceiver => onReceive()");
		String bundle = intent.getStringExtra(PushConstantsEx.KEY_BUNDLE);
		if(bundle == null){
			Logger.i("Empty Bundle !!" );
		}
		
		if(!intent.getAction().contains(context.getPackageName())){
			Logger.i("Not Support Action");
			return ;
		}
				
		if (bundle.equals(PushConstantsEx.COMPLETE_BUNDLE.REG_PUSHSERVICE)) { 
			String result = intent.getExtras().getString(PushConstants.KEY_RESULT);
			Log.d(TAG, "ACTION_REG_PUSHSERVICE_COMPLETELED RESULT: " + result);
		} 
		else if (bundle.equals(PushConstantsEx.COMPLETE_BUNDLE.REG_USER)) { 
			String result = intent.getExtras().getString(PushConstants.KEY_RESULT);
			Log.d(TAG, "ACTION_REG_USER_COMPLETELED RESULT: " + result);
		} 
		else if (bundle.equals(PushConstantsEx.COMPLETE_BUNDLE.SEND_MESSAGE)) { 
			String result = intent.getExtras().getString(PushConstants.KEY_RESULT);
			Log.d(TAG, "ACTION_SEND_MESSAGE_COMPLETELED RESULT: " + result);
		} 
		else if (bundle.equals(PushConstantsEx.COMPLETE_BUNDLE.READ_CONFIRM)) { 
			String result = intent.getExtras().getString(PushConstants.KEY_RESULT);
			Log.d(TAG, "ACTION_READ_CONFIRM_COMPLETELED RESULT: " + result);
		}
		else {
			String result = intent.getExtras().getString(PushConstants.KEY_RESULT);
			Log.d(TAG, "ACTION_OTHER RESULT: " + result);
		}
	}
}
