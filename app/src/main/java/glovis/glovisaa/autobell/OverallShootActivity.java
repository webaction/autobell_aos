package glovis.glovisaa.autobell;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.RectF;
import android.graphics.SurfaceTexture;
import android.graphics.drawable.ColorDrawable;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.Image;
import android.media.ImageReader;
import android.media.MediaActionSound;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.util.Log;
import android.util.Range;
import android.util.Size;
import android.util.SparseIntArray;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import glovis.glovisaa.autobell.data.OverallShootGuide;
import glovis.glovisaa.autobell.data.UploadImgData;
import glovis.glovisaa.autobell.db.CarDBHelper;
import glovis.glovisaa.autobell.db.GuideDBHelper;
import glovis.glovisaa.autobell.db.PhotoDBHelper;
import glovis.glovisaa.autobell.service.ApiService;
import glovis.glovisaa.autobell.util.Common;
import glovis.glovisaa.autobell.util.ProgressRequestBody;
import glovis.glovisaa.autobell.util.RoundImageView;
import glovis.glovisaa.autobell.util.Util;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * 설명 : 등록된 차량의 360도 촬영 기능 Activity
 * @fileOverview 360도(셀프등록) 촬영 화면
 * @author 나기도
 */
public class OverallShootActivity extends AppCompatActivity implements ProgressRequestBody.UploadCallbacks {

    private Context mContext;

    @BindView(R.id.cameraTextureView)
    TextureView mTextureView;

    private CameraDevice mCameraDevice;
    private CaptureRequest.Builder mPreviewBuiler;
    private CameraCaptureSession mPreviewSession;
    private CameraManager mCameraManager;
    // 카메라 설정에 관한 변수
    private Size mPreviewSize;
    private StreamConfigurationMap map;

    private Dialog mShootingDialog = null;
    private Dialog mLoadingDialog = null;

    // 유저 ID
    private  String mUser_ID;

    // 차량 ID
    private  int mCarDB_ID;
    private  String mCar_ID;
    private  int mUserType;
    // 차량 타입
    private int mCarType = 0;                  // 0 : 경차, 1 : 세단, 2: SUV, 3: RV
    private String mCarNumber;
    // 차량 사진 DB
    private PhotoDBHelper photo_db = null;
    private String mDBPath = "";

    // 촬영 종료 버튼
    @BindView(R.id.btn_exit)
    ImageView btn_exit;

    // 촬영 데이터 삭제(초기화)버튼
    @BindView(R.id.btn_delete)
    TextView btn_delete;

    // 가이드 선택 상태
    private boolean mGuideSelected = false;

    @BindView(R.id.btn_capture)
    ImageView btn_capture;                              // 촬영 버튼
    //private TextView tv_pic_count;                    // 사진 촬영 수 표시

    @BindView(R.id.iv_guide_line)
    ImageView iv_guide_line;                            // 촬영 가이드 라인
    private int mPhotoWidth = 0;                        // 촬영 사진 가로 사이즈
    private int mPhotoHeight = 0;                       // 촬영 사진 세로 사이즈

    @BindView(R.id.iv_shooted_picture)
    ImageView iv_shooted_picture;                       // 촬영된 사진 보기
    private int mTouchPosX;                             // 촬영된 사진의 X터치 좌표값
    @BindView(R.id.tv_question_use_pic)
    TextView tv_question_use_pic;                       // 촬영된 사진 확인 문구
    @BindView(R.id.tv_info_current_shoot)
    TextView tv_info_current_shoot;                     // 현재 촬영 부분 알림 문구
    @BindView(R.id.btn_show_guide)
    TextView  btn_show_guide;                           // 가이드 보이기/숨기기 버튼(촬영 확인 화면)
    @BindView(R.id.btn_reshooting)
    TextView  btn_reshooting;                           // 전체 재촬영 버튼
    @BindView(R.id.btn_upload_pic)
    TextView  btn_upload_pic;                           // 촬영 사진 업로드 버튼
    private boolean   mIsConfirm;                       // 촬영 확인 상태(true : 확인 대기, false : 취소)
    private boolean   mIsShowGuide = false;             // 가이드 라인 보기 상태값
    private String    mShootedPicName;                  // 촬영 사진 파일 명
    private Uri       mShootedPicUri = null;            // 촬영 사진 Uri (Android Q)
    private Bitmap    mBMCapturedPhoto = null;          // 촬영된 Image's Bitmap
    @BindView(R.id.iv_watermark)
    ImageView iv_watermark;                             // Watermark ImageView
    @BindView(R.id.iv_navi_guide)
    ImageView iv_navi_guide;                            // 촬영 방향(360도) Guide

    @BindView(R.id.ll_upload)
    LinearLayout ll_upload;                             // 업로드 화면
    @BindView(R.id.btn_upload_continue)
    TextView btn_upload_continue;                       // 촬영 데이터 업로드 계속 버튼
    @BindView(R.id.btn_upload_close)
    TextView btn_upload_close;                          // 촬영 데이터 업로드 닫기 버튼
    @BindView(R.id.progress_upload)
    ProgressBar progress_upload;                        // 촬영 데이터 업로드 프로그레스바
    @BindView(R.id.tv_upload_text)
    TextView tv_upload_text;                            // 촬영 데이터 업로드 문구
    @BindView(R.id.tv_upload_info)
    TextView tv_upload_info;                            // 촬영 데이터 업로드 상태 문구
    @BindView(R.id.tv_upload_status)
    TextView tv_upload_status;                          // 촬영 데이터 업로드 숫자 표시

    // 사용 가이드 화면
    private Boolean bDontShow = false;
    @BindView(R.id.rl_using_guide)
    RelativeLayout rl_using_guide;
    @BindView(R.id.check_not_show)
    CheckBox check_not_show;
    @BindView(R.id.btn_close_useguide)
    ImageButton btn_close_useguide;

    // 촬영 완료 후, 가이드 화면
    private Boolean bDontShowConfirm = false;
    @BindView(R.id.rl_confirm_guide)
    RelativeLayout rl_confirm_guide;
    @BindView(R.id.check_confirm_not_show)
    CheckBox check_confirm_not_show;
    @BindView(R.id.btn_close_confirm_useguide)
    ImageButton btn_close_confirm_useguide;

    // 가이드 보기/숨기기 효과 애니메이션
    private Animation animFadeIn;
    private Animation animFadeOut;
    private Animation animTranslucent;

    // 메시지 View 이동 애니메이션
    private Animation animMsgTransDown;
    private Animation animMsgTransUp;

    private int            mSelectPositon = 1;          // 촬영 가이드 선택 값 (가이드 sort no)
    private String         mShootedPicGuideCode;        // 촬영 사진 가이드 코드
    private int            mCntGuides     = 0;          // 촬영 가이드 개수
    private TypedArray     arrGuidesText = null;        // 촬영 가이드 문구

    private ArrayList<OverallShootGuide> overall_guides = new ArrayList();      // 외부 촬영 가이드 목록
    //private ArrayList<RoundImageView> overall_guide_images = new ArrayList(); // 외부 촬영 가이드 내 이미지
    private TypedArray arrOverallNaviGuides = null;
    boolean mIsExternalStorageLegacy = true;
    private ArrayList<UploadImgData> array_uploadData = new ArrayList();        // 업로드할 파일 리스트
    private int mCurUploadImgIndex = 0;                                         // 업로드할 파일 순서
    private int mUploadFilesTotalLength = 0;                                    // 업로드할 파일들의 총 용량
    private int mCurrentUploadedLength = 0;                                     // 현재 업로드된  파일들의 용량
    private boolean mIsCancelToUploading = false;                               // 업로드 중 취소 여부
    ApiService apiService;

    private boolean mIsLoadPhotos = false;

    private static final String TAG = Common.COMPANY + "OVERALL_SHOOT";

    /**
     * Conversion from screen rotation to JPEG orientation.
     */
    private static final SparseIntArray ORIENTATIONS = new SparseIntArray();

    static {
        ORIENTATIONS.append(Surface.ROTATION_0, 0);
        ORIENTATIONS.append(Surface.ROTATION_90, 90);
        ORIENTATIONS.append(Surface.ROTATION_180, 180);
        ORIENTATIONS.append(Surface.ROTATION_270, 270);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_overall_shoot);
        ButterKnife.bind(this);
        mContext = this;

        // 유저 ID
        SharedPreferences pref = getSharedPreferences(Common.PREFERENCE, MODE_PRIVATE);
        //mUser_ID = pref.getString(Common.PARAM_USERID, "ClientUID001Android");  // def 값은 테스트 ID : ClientUID001Android
        mUser_ID = "hg02";

        // 등록된 차량 ID
        mUserType = getIntent().getIntExtra("user_type", Common.USER_TYPE_EVALUATOR);
        mCar_ID = getIntent().getStringExtra("car_id");
        mCarType = getIntent().getIntExtra("car_type", Common.CAR_TYPE_SEDAN);
        mCarNumber = getIntent().getStringExtra("car_number");

        // Storage Mode 사용 여부 체크
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            if (false == Environment.isExternalStorageLegacy()) {
                mIsExternalStorageLegacy = false;
            }
        }

        initRetrofitClient();

        initLayout();

        if(bDontShow == true) {
            setGuideNShooting();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy()");
        if(mCameraDevice != null) {
            mCameraDevice.close();
        }
        if(mPreviewSession != null) {
            mPreviewSession.close();
        }
        mCameraManager = null;

        deleteOverallData();
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initLayout() {

        // Camera Preview 크기(16:9)로 지정
        Point p = getScreenSize(this);
        // 화면 세로 최대크기
        int nMaxHeight = p.y;
        // 세로 화면 대비 가로 화면 크기
        int nPreviewWidth = (nMaxHeight / 9) * 16;
        mTextureView.getLayoutParams().height = nMaxHeight;
        mTextureView.getLayoutParams().width  = nPreviewWidth;
        String log = "initLayout - Set TextuewView Size : Width = " + nPreviewWidth + ", Height = " + nMaxHeight;
        Log.d(TAG, log);
        mTextureView.requestLayout();

        mTextureView.setSurfaceTextureListener(mSurfaceTextureListener);

        // 촬영 가이드라인
        int nGuideLineWidth  = nPreviewWidth;
        int nGuideLineHeight = nMaxHeight;
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(nGuideLineWidth, nGuideLineHeight);
        layoutParams.setMargins( (nPreviewWidth - nGuideLineWidth) / 2, (nMaxHeight - nGuideLineHeight) / 2, 0, 0);
        iv_guide_line.setLayoutParams(layoutParams);

        // 촬영 진행 Dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(OverallShootActivity.this);
        builder.setView(R.layout.progress_loading);
        mShootingDialog = builder.create();
        mShootingDialog.setCancelable(false);
        mShootingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        // 기 촬영된 이미지 Loading Dialog
        AlertDialog.Builder builder2 = new AlertDialog.Builder(OverallShootActivity.this);
        builder2.setView(R.layout.progress_loading);
        mLoadingDialog = builder2.create();
        mLoadingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        // 촬영 종료 버튼
        btn_exit.setOnClickListener(mBtnClickListener);

        ll_upload.setClickable(true);
        btn_upload_continue.setOnClickListener(mBtnClickListener);
        btn_upload_close.setOnClickListener(mBtnClickListener);

        // 촬영 데이터 삭제 버튼
        btn_delete.setOnClickListener(mBtnClickListener);

        // 가이드 보기/숨김 애니메이션
        animFadeIn = AnimationUtils.loadAnimation(this, R.anim.anim_fade_in);
        animFadeOut = AnimationUtils.loadAnimation(this, R.anim.anim_fade_out);
        animFadeOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }
            @Override
            public void onAnimationEnd(Animation animation) {
                iv_guide_line.setVisibility(View.GONE);
            }
            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });

        // 메시지(촬영 확인 등)이동 Animation
        animMsgTransDown   = AnimationUtils.loadAnimation(this, R.anim.anim_translate_msg_down);
        animMsgTransUp     = AnimationUtils.loadAnimation(this, R.anim.anim_translate_msg_up);
        animMsgTransUp.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {}
            @Override
            public void onAnimationEnd(Animation animation) {
                tv_question_use_pic.setVisibility(View.GONE);
            }
            @Override
            public void onAnimationRepeat(Animation animation) {}
        });

        // 촬영 버튼
        btn_capture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mSelectPositon > 0 && mSelectPositon <= mCntGuides) {
                    takePicture();
                }else {
                    Log.e(TAG, "btn_capture Click : mSelectPositon is not Valid!!");
                }
            }
        });

        iv_shooted_picture.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                Log.d(TAG, "iv_shooted_picture onTouch()");
                // 촬영 이미지뷰에서 드래그 시, 사진 전환
                int action = motionEvent.getAction();
                if(action == MotionEvent.ACTION_DOWN) {
                    mTouchPosX = (int)motionEvent.getX();
                }
                else if(action == MotionEvent.ACTION_MOVE) {

                    // 오른쪽으로 드래그 시, 360도 촬영 중 좌측 회전 (역순)
                    if( mTouchPosX + 75 < motionEvent.getX() ) {
                        setOverallPhotoDragToRight();
                        mTouchPosX = (int)motionEvent.getX();
                    }
                    // 왼쪽으로 드래그 시, 360도 촬영 중 우측 회전 (정순)
                    else if( mTouchPosX - 75 > motionEvent.getX() ) {
                        setOverallPhotoDragToLeft();
                        mTouchPosX = (int)motionEvent.getX();
                    }
                }
                return false;
            }
        });

        btn_show_guide.setOnClickListener(mBtnClickListener);
        btn_reshooting.setOnClickListener(mBtnClickListener);
        btn_upload_pic.setOnClickListener(mBtnClickListener);

        // 촬영된 사진 수 / 최대 촬영 수 표시
        //tv_pic_count = findViewById(R.id.tv_pic_count);

        // 촬영된 사진 보여주는 ImageView 크기를 미리보기 화면과 맞춤
        iv_shooted_picture.getLayoutParams().height = nMaxHeight;
        iv_shooted_picture.getLayoutParams().width  = nPreviewWidth;
        iv_shooted_picture.requestLayout();

        // 사용 가이드 화면
        rl_using_guide.setClickable(true);

        // 촬영 완료 후 확인 가이드 화면
        rl_confirm_guide.setClickable(true);

        // SharedPreference 를 선언한다.
        SharedPreferences pref = getSharedPreferences(Common.PREFERENCE, MODE_PRIVATE);
        bDontShow = pref.getBoolean(Common.DONT_SHOT_OVERALL_USING_GUIDE, false);

        if(bDontShow == false) {
            rl_using_guide.setVisibility(View.VISIBLE);
        }else {
            rl_using_guide.setVisibility(View.GONE);
        }
        // 360 촬영 사용 가이드 닫기
        btn_close_useguide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (check_not_show.isChecked()) {
                    // SharedPreferences 의 데이터를 저장/편집 하기위해 Editor 변수를 선언한다.
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putBoolean(Common.DONT_SHOT_OVERALL_USING_GUIDE, true);
                    editor.commit();
                }
                rl_using_guide.setVisibility(View.GONE);
                setGuideNShooting();
            }
        });

        // 360 촬영 확인 가이드 닫기
        btn_close_confirm_useguide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (check_confirm_not_show.isChecked()) {
                    // SharedPreferences 의 데이터를 저장/편집 하기위해 Editor 변수를 선언한다.
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putBoolean(Common.DONT_SHOT_OVERALL_CONFIRM_GUIDE, true);
                    editor.commit();
                }
                rl_confirm_guide.setVisibility(View.GONE);

                tv_info_current_shoot.setText(getString(R.string.use_shooted_360pictures));
                tv_info_current_shoot.setVisibility(View.VISIBLE);
                tv_info_current_shoot.startAnimation(animMsgTransDown);
            }
        });
    }

    private void initRetrofitClient() {
        OkHttpClient client = new OkHttpClient.Builder().build();

        apiService = new Retrofit.Builder()
                .baseUrl(Common.MOBILE_WEB_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client).build().create(ApiService.class);
    }

    /**
     * 설명 : 가이드 구성 및 촬영 준비
     */
    public void setGuideNShooting() {

        mDBPath = Common.DB_NAME;

        if(photo_db == null) {
            photo_db = new PhotoDBHelper(this, mDBPath, null, 1);
        }

        // 촬영 차량을 DB 등록
        insertCarToDB();

        // 촬영 가이드 목록을 DB로부터 가져와 촬영 가이드 목록 생성
        setShootGuides();

        iv_guide_line.setVisibility(View.VISIBLE);

        arrGuidesText = getResources().obtainTypedArray(R.array.overall_guide_text);
        showShootInfoText();
    }

    // 촬영 완료된 360도 사진을 오른쪽으로 드래그 시, 보기 사진을 전환
    private  void setOverallPhotoDragToRight() {
        Log.d(TAG, "setOverallPhotoDragToRight()");
        if(mSelectPositon > 0 && mSelectPositon <= mCntGuides) {
            mSelectPositon = mSelectPositon + 1;
            if(mSelectPositon > mCntGuides) {
                mSelectPositon = 1;
            }
            Log.d(TAG, "setOverallPhotoDragToRight() : mSelectPositon = " + mSelectPositon);
            String strFileName = overall_guides.get(mSelectPositon - 1).getPhoto_file_name();
            if(strFileName.length() > 1) {
                if (mIsExternalStorageLegacy) {
                    String strPhotoPath = Util.getPhotoFilePath() + strFileName;
                    File imgFile = new File(strPhotoPath);
                    if (imgFile.exists()) {
                        Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                        iv_shooted_picture.setImageBitmap(myBitmap);
                    }
                    // Guide Image
                    if(mIsShowGuide) {
                        int resGuideID = getGuideLineResource();
                        iv_guide_line.setBackgroundResource(resGuideID);
                    }

                }else {
                    String strFileUri = overall_guides.get(mSelectPositon - 1).getPhoto_uri();
                    if(strFileUri.length()  == 0) {
                        Log.e(TAG, "setOverallPhotoDragToRight() getPhoto_uri() fail...");
                        return;
                    }
                    ContentResolver cr = getApplicationContext().getContentResolver();
                    Uri i = Uri.parse(strFileUri);
                    try {
                        InputStream is = cr.openInputStream(i);
                        Bitmap bitmap = BitmapFactory.decodeStream(is);
                        iv_shooted_picture.setImageBitmap(bitmap);
                        if (is != null)
                            is.close();
                        // Guide Image
                        if(mIsShowGuide) {
                            int resGuideID = getGuideLineResource();
                            iv_guide_line.setBackgroundResource(resGuideID);
                        }
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }else{
                Log.e(TAG, "setOverallPhotoDragToRight() getPhoto_file_name() fail...");
            }
        }
    }

    // 촬영 완료된 360도 사진을 왼쪽으로 드래그 시, 보기 사진을 전환
    private  void setOverallPhotoDragToLeft() {
        Log.d(TAG, "setOverallPhotoDragToLeft()");
        if(mSelectPositon > 0 && mSelectPositon <= mCntGuides) {
            mSelectPositon = mSelectPositon - 1;
            if(mSelectPositon == 0) {
                mSelectPositon = mCntGuides;
            }
            Log.d(TAG, "setOverallPhotoDragToLeft() : mSelectPositon = " + mSelectPositon);
            String strFileName = overall_guides.get(mSelectPositon - 1).getPhoto_file_name();
            if(strFileName.length() > 1) {
                if (mIsExternalStorageLegacy) {
                    String strPhotoPath = Util.getPhotoFilePath() + strFileName;
                    File imgFile = new File(strPhotoPath);
                    if (imgFile.exists()) {
                        Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                        iv_shooted_picture.setImageBitmap(myBitmap);
                    }
                    // Guide Image
                    if(mIsShowGuide) {
                        int resGuideID = getGuideLineResource();
                        iv_guide_line.setBackgroundResource(resGuideID);
                    }

                }else {
                    String strFileUri = overall_guides.get(mSelectPositon - 1).getPhoto_uri();
                    if(strFileUri.length()  == 0) {
                        Log.e(TAG, "setOverallPhotoDragToLeft() getPhoto_uri() fail...");
                        return;
                    }
                    ContentResolver cr = getApplicationContext().getContentResolver();
                    Uri i = Uri.parse(strFileUri);
                    try {
                        InputStream is = cr.openInputStream(i);
                        Bitmap bitmap = BitmapFactory.decodeStream(is);
                        iv_shooted_picture.setImageBitmap(bitmap);
                        if (is != null)
                            is.close();
                        // Guide Image
                        if(mIsShowGuide) {
                            int resGuideID = getGuideLineResource();
                            iv_guide_line.setBackgroundResource(resGuideID);
                        }
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }else{
                Log.e(TAG, "setOverallPhotoDragToLeft() getPhoto_file_name() fail...");
            }
        }
    }

    /**
     * 설명 : 촬영 차량 DB 등록
     */
    public void insertCarToDB() {

        // CREATE CAR DB TABLE
        CarDBHelper carDBHelper = new CarDBHelper(this, Common.DB_NAME, null, 1);

        JSONObject jo = carDBHelper.getData(mCarDB_ID);
        // 전달받은 차량 ID가 없다면(이미 DB에 등록된 차량이 아니라면) 차량 DB(tb_car) 에 추가(insert)
        if(jo == null) {
            carDBHelper.insert(Common.MENU_TYPE_OVERALL_SHOOT, mCar_ID, 0, 0, Common.OVERALL_SHOOT_NUM, 0);
        }
    }

    /**
     * DB에서 360도 촬영 가이드 목록을 가져와 가이드 목록을 생성한다
     */
    private void setShootGuides() {

        GuideDBHelper guide_db;
        guide_db = new GuideDBHelper(this,  mDBPath, null, 1);

        // 360 도촬영 가이드 설정
        try {
            JSONArray arrGuide = guide_db.getGuideArrayByType(Common.OVERALL_SHOOT_GUIDE);
            int nSize = arrGuide.length();
            mCntGuides = nSize;
            for(int i =0; i < nSize; i++) {
                JSONObject data = arrGuide.getJSONObject(i);
                int nGuideSide      = data.getInt("guide_side");
                int nGuideSortNo    = data.getInt("guide_sort_no");
                String sGuideCode   = data.getString("guide_code");
                String sGuideName   = data.getString("guide_name");

                overall_guides.add(new OverallShootGuide(nGuideSide, nGuideSortNo, sGuideCode, sGuideName, false, 0, "", ""));
            }

            arrOverallNaviGuides = getResources().obtainTypedArray(R.array.overall_navi_guides);

            int resID = arrOverallNaviGuides.getResourceId(0, 0);
            iv_navi_guide.setImageResource(resID);

            // 360 촬영 가이드
            int resGuideID = getGuideLineResource();
            iv_guide_line.setBackgroundResource(resGuideID);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * 촬영 가이드 버튼을 생성하여 하단 평행스크롤뷰에 추가한다
     *  @param nGuideSortNo 가이드 정렬 순서 번호
     *  @param sGuideCode 가이드 코드
     *  @param sGuideName 가이드 명
     */
    private void addGuideBtnLayout(int nGuideSortNo, String sGuideCode, String sGuideName) {

        // Creating a new RelativeLayout
        RelativeLayout relativeLayout = new RelativeLayout(this);

        // Defining the RelativeLayout layout parameters.
        // In this case I want to fill its parent
        int width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 143, getResources().getDisplayMetrics());
        int left_margin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20, getResources().getDisplayMetrics());
        //Log.d(TAG, "addGuideBtnLayout() Add RelativeLayout width = " + width);
        RelativeLayout.LayoutParams rlp = new RelativeLayout.LayoutParams(width, RelativeLayout.LayoutParams.MATCH_PARENT);
        // 각 외부/내부 부위별 첫번째 가이드가 아니면 left margin 추가
        if(nGuideSortNo != Common.GUIDE_SORT_FRONT_SIDE && nGuideSortNo != Common.GUIDE_SORT_INSTRUMENT_BOARD) {
            rlp.leftMargin = left_margin;
        }
        relativeLayout.setLayoutParams(rlp);
        relativeLayout.setBackgroundResource(R.drawable.guide_btn_rect);

        relativeLayout.setTag(nGuideSortNo);

        // Add RoundImageView
        RoundImageView rImgView = new RoundImageView(this);
        // Setting the parameters on the RoundImageView
        RelativeLayout.LayoutParams roundlp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        int bottomMar = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, getResources().getDisplayMetrics());
        roundlp.bottomMargin = bottomMar;
        roundlp.addRule(RelativeLayout.CENTER_IN_PARENT);
        rImgView.setScaleType(ImageView.ScaleType.FIT_CENTER);
        rImgView.setLayoutParams(roundlp);

        // Set RoundImageView backgound
        int resID = 0;
        String strImgResName = "";
        try {
            strImgResName = "icon_" + sGuideCode;
            resID = getResourceID(strImgResName, getApplicationContext());
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        rImgView.setImageResource(resID);

        // Add Guide name TextView
        TextView tvGuideName = new TextView(this);
        RelativeLayout.LayoutParams textlp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        int bottomMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5, getResources().getDisplayMetrics());
        textlp.bottomMargin = bottomMargin;
        textlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        textlp.addRule(RelativeLayout.CENTER_HORIZONTAL);
        tvGuideName.setLayoutParams(textlp);
        tvGuideName.setText(sGuideName);
        tvGuideName.setTextColor(Color.WHITE);

        relativeLayout.addView(rImgView);
        relativeLayout.addView(tvGuideName);
    }

    /**
     * DB에 저장된 사진을 체크하여 촬영 상태값을 저장한다
     */
    private void setGuidesState() {

        // 촬영된 이미지가 있을 경우
        //overall_guides.get(nPosition).setExistPhoto(true);
        //overall_guides.get(nPosition).setPhoto_id(nPhotoID);
    }

    /**
     * DB에 저장된 사진이 있다면(기 촬영본) 섬네일을 추출하여 가이드 선택 버튼에 넣는다
     */
    private void setPhotosToGuideBtns() {

        try {
            JSONArray arrPhotos = photo_db.getResultByCarNumberForOverallType(mCar_ID);
            int nSize = arrPhotos.length();
            Log.d(TAG, "setPhotosToGuideBtns Image Numbers : " + nSize);
            for(int i = 0; i < nSize; i++) {
                JSONObject data = arrPhotos.getJSONObject(i);
                int nPhotoID = data.getInt("_id");
                String strFileName    = data.getString("photo_path");
                String strGuideCode   = data.getString("guide_code");
                String strFilePath    = Util.getPhotoFilePath() + strFileName;

                setPhotoToGuideBtn(nPhotoID, strGuideCode, strFilePath);
            }

            // 촬영된 사진 수 / 최대 촬영 수 표시
            //updatePhotoCount();
            mIsLoadPhotos = true;
            setLoadingDialog(false);

        } catch (JSONException e) {
            e.printStackTrace();
            setLoadingDialog(false);
        }
    }

    /**
     * 설명 : DB에 저장된 사진이 있다면(기 촬영본) 파일을 추출하여 업로드 리스트에 넣는다
     */
    private void setPhotosToUploadList() {

        mIsCancelToUploading = false;
        mCurUploadImgIndex = 0;
        mUploadFilesTotalLength = 0;
        mCurrentUploadedLength = 0;
        array_uploadData.clear();
        try {
            JSONArray arrPhotos = photo_db.getResultByCarNumberForOverallType(mCar_ID);
            int nSize = arrPhotos.length();
            Log.d(TAG, "setPhotosToUploadList Image Numbers : " + nSize);
            for(int i =0; i < nSize; i++) {
                JSONObject data = arrPhotos.getJSONObject(i);
                int nPhotoID = data.getInt("_id");
                String strFileName    = data.getString("photo_path");
                String strGuideCode   = data.getString("guide_code");
                String strPhotoCode   = "";     // 360사진파일은 사진코드 없이 업로드한다
                String strFilePath = "";
                if (mIsExternalStorageLegacy) {
                    strFilePath = Util.getPhotoFilePath() + strFileName;
                    setFileToUploadLIst(nPhotoID, strGuideCode, strPhotoCode, strFilePath, null);
                }else {
                    String strFileUri    = data.getString("photo_uri");
                    setFileToUploadLIst(nPhotoID, strGuideCode, strPhotoCode, strFileName, strFileUri);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
            setLoadingDialog(false);
        } finally {
            Log.e(TAG, "setPhotosToUploadList() => progress_upload set : " + mUploadFilesTotalLength);
            setLoadingDialog(false);
            progress_upload.setMax(mUploadFilesTotalLength);
            progress_upload.setProgress(0);
        }
    }

    /**
     * 설명 : 촬영된 사진을 업로드 리스트에 추가
     * @param photo_id 촬영된 사진 DB id
     * @param photo_guide_code 촬영된 사진 가이드 코드
     * @param path 촬영된 사진 파일 경로
     */
    public void setFileToUploadLIst(int photo_id, String photo_guide_code, String photo_code, String path, String uri) {
        Log.d(TAG, "setFileToUploadLIst photo_id : " + photo_id + ", photo_guide_code : " + photo_guide_code + ", path : " + path + ", uri : " + uri);
        boolean isFileExist = false;
        // 기존 Storage Mode (Android P 이하)
        if (mIsExternalStorageLegacy) {

            File imgFile = new File(path);
            if (imgFile.exists()) {
                isFileExist = true;
            }
        }
        // Scoped Mode (Android Q 이상)
        else {
            if(uri.length() > 1) {
                Uri i = Uri.parse(uri);
                isFileExist = isExistPhotoUri(i);
            }
        }

        if (isFileExist == true) {
            // 파일 추출
            File uploadFile = null;
            if (mIsExternalStorageLegacy) {
                uploadFile = new File(path);
            }else {
                ContentResolver cr = getApplicationContext().getContentResolver();
                Uri i = Uri.parse(uri);
                try {
                    InputStream is = cr.openInputStream(i);
                    uploadFile = getFileFromInputstream(path, is);
                    if (is != null)
                        is.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            // File을 업로드 리스트에 추가
            if(uploadFile != null) {
                UploadImgData uploadImgData = new UploadImgData(photo_id, photo_code, uploadFile, uri);
                Log.d(TAG, "setFileToUploadLIst() => uploadFile.length() : " + uploadFile.length());
                mUploadFilesTotalLength += (int)uploadFile.length();
                array_uploadData.add(uploadImgData);
            }
        }
    }

    /**
     * 설명 : 서버에 촬영 이미지 파일을 업로드
     */
    private void FileUploadToServer() {

        UploadImgData uploadImgData = array_uploadData.get(mCurUploadImgIndex);
        if(uploadImgData == null) {
            Log.e(TAG, "FileUploadToServer() => uploadImgData is null");
            return;
        }
        // 차량 이미지 전송시 필요한 값
        //1. 유저 ID => userId    2. 차량 매물 ID => prdId    3. 사진 위치 코드 => phCode    4. 일반/라이브샷 'M' / "L" => phType    5. 사진파일 => file
        // Request에 쓰이는 Map
        Map<String, RequestBody> rqMap = new HashMap<>();
        RequestBody rqUserID = RequestBody.Companion.create(mUser_ID, Common.TEXT_PLAIN);
        RequestBody rqCarID = RequestBody.Companion.create(mCar_ID, Common.TEXT_PLAIN);
        RequestBody rqPhCode = RequestBody.Companion.create(uploadImgData.getPhCode(), Common.TEXT_PLAIN);
        RequestBody rqPhType = RequestBody.Companion.create("L", Common.TEXT_PLAIN);
        rqMap.put(Common.PARAM_USERID, rqUserID);
        rqMap.put(Common.PARAM_CARID, rqCarID);
        rqMap.put(Common.PARAM_PHCODE, rqPhCode);
        rqMap.put(Common.PARAM_PHTYPE, rqPhType);

        File uploadFile = uploadImgData.getUploadFile();
        ProgressRequestBody fileBody = new ProgressRequestBody(uploadFile, Common.CONTENT_TYPE_MULTIPART, this);

        // 또 다른 업로드 방식
        //FileUploadUtils.imageUploadToServer(fileBody, uploadFile.getName(), mUser_ID, mCar_ID, "0010", "M");

        MultipartBody.Part filePart = MultipartBody.Part.createFormData(Common.PARAM_FILE, uploadFile.getName(), fileBody);  // 킷값, 파일 이름, 데이터
        Log.d(TAG, "FileUploadToServer() => Upload File Index : " + mCurUploadImgIndex + ", name : " + uploadFile.getName());
        Call<JSONObject> request = apiService.uploadImage(filePart, rqMap);

        request.enqueue(new Callback<JSONObject>() {
            @Override
            public void onResponse(Call<JSONObject> call, Response<JSONObject> response) {
                if (response.isSuccessful()) {
                    /* Here we can equally assume the file has been downloaded successfully because for some reasons the onFinish method might not be called, I have tested it myself and it really not consistent, but the onProgressUpdate is efficient and we can use that to update our progress on the UIThread, and we can then set our progress to 100% right here because the file already downloaded finish. */
                    if (response.code() == 200) {
                        Log.d(TAG, "FileUploadToServer() => Success Image File Upload");
                        mCurrentUploadedLength = mCurrentUploadedLength + (int)array_uploadData.get(mCurUploadImgIndex).getUploadFile().length();
                        mCurUploadImgIndex++;
                        tv_upload_status.setText(mCurUploadImgIndex + " / " + array_uploadData.size());

                        // 업로드 취소(사용자) 체크
                        if(mIsCancelToUploading) {
                            tv_upload_text.setText(R.string.detailshoot_upload);
                            progress_upload.setProgress(0);
                            tv_upload_status.setText("0 / " + array_uploadData.size());
                        }
                        // 업로드 완료 체크
                        else if(mCurUploadImgIndex == array_uploadData.size()) {
                            progress_upload.setProgress(mUploadFilesTotalLength);
                            tv_upload_text.setText(getString(R.string.success_upload_photos));
                            btn_upload_continue.setVisibility(View.GONE);

                            // 업로드 완료된 이미지 파일 및 DB 삭제
                            deleteOverallData();
                        }
                        // 다음 순서 사진 파일 업로드
                        else {
                            FileUploadToServer();
                        }
                    }
                }
                // 파일 업로드 실패
                else {
                    Log.d(TAG, "FileUploadToServer() => Failed Image File Upload : response.code() = " + response.code());
                    tv_upload_text.setText(getString(R.string.fail_upload_photos));
                    mIsCancelToUploading = true;
                    mCurUploadImgIndex = 0;
                    progress_upload.setProgress(0);
                    tv_upload_status.setText("0 / " + array_uploadData.size());
                }
            }

            @Override
            public void onFailure(Call<JSONObject> call, Throwable t) {
                /* we can also stop our progress update here, although I have not check if the onError is being called when the file could not be downloaded, so I will just use this as a backup plan just in case the onError did not get called. So I can stop the progress right here. */
                if (t instanceof IOException) {
                    Log.e(TAG, "FileUploadToServer() => onFailure() : this is an actual network failure :( inform the user and possibly retry");
                }
                else {
                    // todo log to some central bug tracking service
                    Log.e(TAG, "FileUploadToServer() => onFailure() : conversion issue! big problems :(");
                }
                tv_upload_text.setText(getString(R.string.fail_upload_photos));
                mIsCancelToUploading = true;
                mCurUploadImgIndex = 0;
                progress_upload.setProgress(0);
                tv_upload_status.setText("0 / " + array_uploadData.size());
            }
        });
    }

    @Override
    public void onProgressUpdate(int percentage) {
        // set current progress
        //Log.d(TAG, "Image File Upload mCurrentUploadedLength : " + percentage + mCurrentUploadedLength);
        progress_upload.setProgress(percentage + mCurrentUploadedLength);
    }

    @Override
    public void onError() {
        // do something on error
        Log.d(TAG, "Image File Upload onError()");
        tv_upload_text.setText(getString(R.string.fail_upload_photos));
        mIsCancelToUploading = true;
        mCurUploadImgIndex = 0;
        progress_upload.setProgress(0);
        tv_upload_status.setText("0 / " + array_uploadData.size());
    }

    @Override
    public void onFinish() {
        Log.d(TAG, "Image File Upload onFinish()");
        // do something on upload finished,
        // for example, start next uploading at a queue
    }

    /**
     * 설명 : 촬영된 사진의 Uri 존재 여부 반환
     * @return {uri} 촬영 사진의 URI
     */
    public boolean isExistPhotoUri(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null, null);
        boolean bExist = false;
        try {
            if (cursor != null && cursor.moveToFirst()) {
                String displayName = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                if (displayName != null) {
                    if (displayName.length() > 0) {
                        bExist = true;
                    }
                }
            }
        } finally {
            cursor.close();
            return bExist;
        }
    }

    /**
     * 설명 : InputSteram으로부터 File 객체를 변환 (Upload 용 in Anroid Q)
     */
    private File getFileFromInputstream(String strFileName, InputStream input) {

        File convFile = null;
        try {
            File file = new File(getCacheDir(), strFileName);
            try (OutputStream output = new FileOutputStream(file)) {
                byte[] buffer = new byte[4 * 1024]; // or other buffer size
                int read;

                while ((read = input.read(buffer)) != -1) {
                    output.write(buffer, 0, read);
                }
                output.flush();
                convFile = file;
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } finally {
            try {
                input.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return convFile;
    }

    /**
     * 설명 : 촬영된 사진 및 DB를 삭제 (업로드 완료 이후)
     */
    public void deleteOverallData() {
        Log.d(TAG, "deleteOverallData() Start!");
        // tb_car & tb_photo 에서 차량 ID 및 차량 번호에 해당 값 삭제
        /*try {
            // DELETE CAR DB DATA
            CarDBHelper carDBHelper = new CarDBHelper(this, Common.DB_NAME, null, 1);
            carDBHelper.delete(mCarDB_ID);
            // DELETE PHOTO DB DATA
            photo_db.deleteByCarNumber(mCar_ID);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }*/

        // 360도 촬영된 사진 DB 삭제
        try {
            JSONArray arrPhotos = photo_db.getResultByCarNumberForOverallType(mCar_ID);
            int nSize = arrPhotos.length();
            Log.d(TAG, "deleteOverallData Image Numbers : " + nSize);
            for(int i = 0; i < nSize; i++) {
                JSONObject data = arrPhotos.getJSONObject(i);
                int nPhotoID = data.getInt("_id");
                Log.d(TAG, "deleteOverallData Delete Image DB _id : " + nPhotoID);
                photo_db.delete(nPhotoID);
            }

        } catch (JSONException e) {
            e.printStackTrace();
            Log.e(TAG, "deleteOverallData() DB Delete Failed...");
        }

        // 360도 촬영된 사진 파일 삭제
        for(OverallShootGuide guideData : overall_guides) {
            if(guideData.getExistPhoto()) {
                String deleteFileName = guideData.getPhoto_file_name();
                if (deleteFileName.length() > 2) {
                    if (mIsExternalStorageLegacy) {
                        try {
                            String strFilePath = Util.getPhotoFilePath() + deleteFileName;
                            File f = new File(strFilePath);
                            if (f.delete()) {
                                //Log.d(TAG, "deleteOverallData() Success");
                            } else {
                                Log.e(TAG, "deleteOverallData() Failed...");
                            }
                        }catch (Exception e) {
                            Log.e(TAG, "deleteOverallData() File Delete Exception : " + e.toString());
                        }
                    }else {
                        try {
                            // 촬영 사진 삭제
                            String uri = guideData.getPhoto_uri();
                            if (uri != null) {
                                if (uri.length() > 1) {
                                    Uri parseUri = Uri.parse(uri);
                                    int nDelCnt = getContentResolver().delete(parseUri, null, null);
                                    if (nDelCnt > 0) {
                                        Log.d(TAG, "deleteOverallData() File Delete Success");
                                    } else {
                                        Log.e(TAG, "deleteOverallData() File Delete Failed...");
                                    }
                                }
                            }
                        }catch (SecurityException e) {
                            Log.e(TAG, "deleteOverallData() File Delete SecurityException : " + e.toString());
                        }
                    }
                }else {
                    Log.e(TAG, "deleteOverallData() Failed get Pic Filename");
                }
            }
        }
        mSelectPositon = 0;
        Log.d(TAG, "deleteOverallData() End");
    }

    View.OnClickListener mBtnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            int id = v.getId();
            // 촬영 종료
            if (id == R.id.btn_exit) {
                Log.d(TAG, "Click btn_exit");
                showExitDialog();
            }
            // 재촬영
            else if (id == R.id.btn_reshooting) {

                new androidx.appcompat.app.AlertDialog.Builder(OverallShootActivity.this)
                        .setMessage(R.string.overall_again_shoot_inform)
                        .setPositiveButton(android.R.string.ok,
                                new AlertDialog.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // 이미지 파일 및 DB 삭제
                                        deleteOverallData();
                                        mSelectPositon = 0;
                                        mIsConfirm = false;
                                        capturedPictureConfirm();
                                    }
                                })
                        .setNegativeButton(android.R.string.cancel, null)
                        .setCancelable(false)
                        .create()
                        .show();
            }
            // 촬영된 사진 업로드
            else if (id == R.id.btn_upload_pic) {
                Log.d(TAG, "btn_upload_pic Click");
                tv_upload_info.setText(R.string.overallshoot_upload_info);
                tv_upload_status.setText("0 / " + Common.OVERALL_SHOOT_NUM);
                ll_upload.setVisibility(View.VISIBLE);
                setLoadingDialog(true);
                // 업로드 목록 구성
                setPhotosToUploadList();
                //showUploadDialog();
            }
            // 가이드 보이기 / 숨기기
            else if (id == R.id.btn_show_guide) {
                if (mIsShowGuide == false) {
                    mIsShowGuide = true;
                    iv_guide_line.setVisibility(View.VISIBLE);
                    iv_guide_line.startAnimation(animFadeIn);
                    btn_show_guide.setText(R.string.guide_hide);
                    btn_show_guide.setBackgroundResource(R.drawable.btn_unactive);

                }else {
                    mIsShowGuide = false;
                    iv_guide_line.startAnimation(animFadeOut);
                    btn_show_guide.setText(R.string.guide_show);
                    btn_show_guide.setBackgroundResource(R.drawable.btn_active);
                }

            }
            // 촬영 데이터 삭제
            else if (id == R.id.btn_delete) {
                Log.d(TAG, "Click btn_delete");
                showDeleteDialog();
            }
            // 촬영 데이터 업로드 계속
            else if (id == R.id.btn_upload_continue) {
                Log.d(TAG, "Click Upload continue");

                // 업로드 중 체크
                if(mCurUploadImgIndex < array_uploadData.size() && progress_upload.getProgress() > 0) {
                    Toast.makeText(mContext, getString(R.string.uploading_photos), Toast.LENGTH_SHORT).show();
                    return;
                }

                tv_upload_text.setText(R.string.detailshoot_upload);

                boolean isConnected = Util.checkNetworkConnection(mContext);
                if(false == isConnected) {
                    Toast.makeText(mContext, getString(R.string.fail_connect_network), Toast.LENGTH_SHORT).show();
                }else {
                    if(mIsCancelToUploading) {  // 업로드 중단 후, 재시작인 경우
                        // 업로드 목록 구성
                        setPhotosToUploadList();
                    }
                    // 촬영 사진 개수 만큼 업로드
                    FileUploadToServer();
                }
            }
            // 촬영 데이터 업로드 닫기
            else if (id == R.id.btn_upload_close) {
                // 업로드 완료 체크 (업로드 완료 시, 종료)
                if(mCurUploadImgIndex == array_uploadData.size()) {
                    Log.d(TAG, "Click Upload close - Upload Complete!");
                    // 호출했던(전달 받는) 액티비티에 전달할(돌려줄) 데이터를 지정한다.
                    setResult(RESULT_OK);
                    finish();
                }
                // 업로드 중 중단
                else if(mCurUploadImgIndex < array_uploadData.size() && progress_upload.getProgress() > 0 && mIsCancelToUploading == false) {
                    Log.d(TAG, "Click Upload close - Cancel to upload");
                    mIsCancelToUploading = true;
                    tv_upload_text.setText(getString(R.string.cancel_upload_photos));
                }
                // 업로드 창 닫기
                else {
                    Log.d(TAG, "Click Upload close - Close upload window~");
                    mIsCancelToUploading = false;
                    ll_upload.setVisibility(View.GONE);
                    btn_upload_continue.setVisibility(View.VISIBLE);
                }
            }
        }
    };

    @Override
    public void onBackPressed() {
        showExitDialog();
    }

    private void showExitDialog() {

        deleteOverallData();
        finish();

        /*new androidx.appcompat.app.AlertDialog.Builder(OverallShootActivity.this)
                .setMessage(R.string.exit_app)
                .setPositiveButton(android.R.string.ok,
                        new AlertDialog.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                deleteOverallData();
                                finish();
                            }
                        })
                .setNegativeButton(android.R.string.cancel, null)
                .setCancelable(false)
                .create()
                .show();*/
    }

    /**
     * 설명 : 데이터 삭제 Dialog를 띄움
     */
    private void showDeleteDialog() {
        new androidx.appcompat.app.AlertDialog.Builder(OverallShootActivity.this)
                .setMessage(R.string.delete_text)
                .setPositiveButton(android.R.string.ok,
                        new androidx.appcompat.app.AlertDialog.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                deleteOverallData();
                            }
                        })
                .setNegativeButton(android.R.string.cancel, null)
                .setCancelable(false)
                .create()
                .show();
    }

    // 촬영된 사진들을 업로드할지 묻는다
    private void showUploadDialog() {

        ll_upload.setVisibility(View.VISIBLE);
        /*new androidx.appcompat.app.AlertDialog.Builder(OverallShootActivity.this)
                .setMessage(R.string.overall_shoot_upload_inform)
                .setPositiveButton(android.R.string.ok,
                        new AlertDialog.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Toast.makeText(OverallShootActivity.this, R.string.success_upload_photos, Toast.LENGTH_SHORT).show();
                                deleteOverallData();
                                //finish();
                            }
                        })
                .setNegativeButton(android.R.string.cancel, null)
                .setCancelable(false)
                .create()
                .show();*/
    }

    private int getGuideLineResource() {

        if(mSelectPositon <= 0) {
            Log.e(TAG, "getGuideLineResource mSelectPositon : " + mSelectPositon);
            return 0;
        }

        int resID = 0;
        String strCarType = "";
        String strImgResName = "";

        // 차량 타입명을 붙여서 구분
        if (mCarType == Common.CAR_TYPE_LIGHT) {           // 경차
            strCarType = "light_manual_360_";
        } else if (mCarType == Common.CAR_TYPE_SEDAN) {    // 세단
            strCarType = "sedan_manual_360_";
        } else if (mCarType == Common.CAR_TYPE_SUV) {      // SUV
            strCarType = "suv_manual_360_";
        } else if (mCarType == Common.CAR_TYPE_RV) {       // RV
            strCarType = "rv_manual_360_";
        }
        strImgResName = strCarType + (mSelectPositon-1);

        try {
            resID = getResourceID(strImgResName, getApplicationContext());
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(TAG, "getGuideLineResource NameNotFoundException strImgResName : " + strImgResName);
            e.printStackTrace();
            return 0;
        }
        return resID;
    }

    /**
     *  미리보기와 달리 사진 이미지 캡처는 별도 CaptureSession을 생성해서 작업
     **/
    private void takePicture() {

        try {
            // set Sutter Sound
            MediaActionSound sound = new MediaActionSound();
            sound.play(MediaActionSound.SHUTTER_CLICK);

            mBMCapturedPhoto = null;

            btn_capture.setVisibility(View.GONE);

            setShootingDialog(true);

            Size[] jpegSizes = null;

            if (map != null)
                jpegSizes = map.getOutputSizes(ImageFormat.JPEG);

            int width = 640;
            int height = 360;
            if (jpegSizes != null && 0 < jpegSizes.length) {

                for (Size size : jpegSizes) {
                    width = size.getWidth();
                    height = size.getHeight();
                    // 가로 사이즈 기준으로 16:9 비율로 조정
                    int nNewHeight = (width / 16) * 9;
                    // 절대값이 20 이내의 차이인 세로사이즈 값을 구한다
                    int nInterval = Math.abs(height - nNewHeight);
                    if ( nInterval < 20 && (width > 3000 && width < 4000) ) {       // 일단 해상도를 3840x2160 에 맞춘다 (!!!타기종 확인 필!!!)
                        break;
                    }
                }
                Log.d(TAG, "takePicture Size : width = " + width + ", height = " + height);
                mPhotoWidth = width;
                mPhotoHeight = height;
            }

            final ImageReader imageReader = ImageReader.newInstance(width, height, ImageFormat.JPEG, 1);

            List<Surface> outputSurfaces = new ArrayList<Surface>(2);
            outputSurfaces.add(imageReader.getSurface());
            outputSurfaces.add(new Surface(mTextureView.getSurfaceTexture()));

            // ImageCapture를 위한 CaputureRequest.Builder 객체
            final CaptureRequest.Builder captureBuilder = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_STILL_CAPTURE);
            captureBuilder.addTarget(imageReader.getSurface());

            // 이전 카메라 api는 이 기능 지원X
            // 이미지를 캡처하는 순간에 제대로 사진 이미지가 나타나도록 3A를 자동으로 설정
            captureBuilder.set(CaptureRequest.CONTROL_AF_MODE, CameraMetadata.CONTROL_AF_MODE_CONTINUOUS_PICTURE);  //old : CONTROL_MODE, CONTROL_MODE_AUTO

            int rotation = getWindowManager().getDefaultDisplay().getRotation();
            captureBuilder.set(CaptureRequest.JPEG_ORIENTATION, ORIENTATIONS.get(rotation));

            // Guide 버튼 선택값(mSelectPositon)으로 촬영된 사진이 저장될 이름을 가져온다
            String strPicFileName = getPicFileName();
            if(strPicFileName.length() < 2 ) {
                Log.e(TAG, "takePicture() getPicFileName() fail...");
                return;
            }

            mShootedPicName = strPicFileName;
            mShootedPicGuideCode = getGuideCode(mSelectPositon);

            // 이미지를 캡처할 때 자동으로 호출된다.
            ImageReader.OnImageAvailableListener readerListener = new ImageReader.OnImageAvailableListener() {
                @Override
                public void onImageAvailable(ImageReader reader) {
                    Log.d(TAG, "takePicture() => readerListener : onImageAvailable()");
                    Image capturedImage = null;
                    try {
                        capturedImage = imageReader.acquireLatestImage();
                        if(capturedImage == null) {
                            Log.e(TAG, "takePicture() => readerListener : capturedImage == null");
                            //setShootingDialog(false);
                            return;
                        }
                        mBMCapturedPhoto = Util.getBitmapFromImage(capturedImage);
                        if(mBMCapturedPhoto == null) {
                            Log.e(TAG, "takePicture() => readerListener : mBMCapturedPhoto == null");
                            //setShootingDialog(false);
                            return;
                        }
                        else {
                            Log.d(TAG, "takePicture() => readerListener : save image and insert db and Next step");
                            // 촬영 사진 File 저장
                            saveCapturedPhoto();
                            // 촬영 사진 DB 입력
                            insertPhotoToDB();
                            // 촬영 정보 세팅
                            setGuidesState();
                            mIsConfirm = false;
                            //mIsShowGuide = false;
                            capturedPictureConfirm();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.e(TAG, "takePicture() => readerListener : Exception");
                        setShootingDialog(false);
                        return;
                    }
                }
            };

            // 이미지를 캡처하는 작업은 메인 스레드가 아닌 스레드 핸들러로 수행한다.
            HandlerThread thread = new HandlerThread("CameraPicture");
            thread.start();
            final Handler backgroundHandler = new Handler(thread.getLooper());

            // ImageReader와 ImageReader.OnImageAvailableListener객체를 서로 연결시켜주기 위해 설정
            imageReader.setOnImageAvailableListener(readerListener, backgroundHandler);

            // 사진 이미지를 캡처한 이후 호출되는 메소드
            final CameraCaptureSession.CaptureCallback captureCallback = new CameraCaptureSession.CaptureCallback() {
                @Override
                public void onCaptureCompleted(@NonNull CameraCaptureSession session,
                                               @NonNull CaptureRequest request, @NonNull TotalCaptureResult result) {
                    super.onCaptureCompleted(session, request, result);
                    Log.d(TAG, "takePicture() => CaptureCallback : onCaptureCompleted()");
                    //setShootingDialog(false);
                    //Toast.makeText(DetailShootActivity.this, "saved:" + file, Toast.LENGTH_LONG).show();
                    if(mBMCapturedPhoto != null) {
                        Log.d(TAG, "takePicture() => onCaptureCompleted : save image and insert db and Next step");
                        // 촬영 사진 File 저장
                        saveCapturedPhoto();
                        // 촬영 사진 DB 입력
                        insertPhotoToDB();
                        // 촬영 정보 세팅
                        setGuidesState();
                        mIsConfirm = false;
                        //mIsShowGuide = false;
                        capturedPictureConfirm();
                     }
                }
            };
            /*
            사진 이미지를 캡처하는데 사용하는 CameraCaptureSession을 생성한다.
            이미 존재하면 기존 세션은 자동으로 종료
            */
            mCameraDevice.createCaptureSession(outputSurfaces, new CameraCaptureSession.StateCallback() {
                @Override
                public void onConfigured(@NonNull CameraCaptureSession session) {
                    try {
                        session.capture(captureBuilder.build(), captureCallback, backgroundHandler);

                    } catch (CameraAccessException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onConfigureFailed(@NonNull CameraCaptureSession session) {

                }
            }, backgroundHandler);

        } catch (CameraAccessException e) {
            Log.e(TAG, "takePicture() createCaptureRequest fail");
            e.printStackTrace();
            setShootingDialog(false);
        }
    }

    /**
     * 캡처된 사진을 File로 SD Card에 저장
     **/
    private void saveCapturedPhoto() {

        if(mBMCapturedPhoto == null) {
            Log.e(TAG, "saveCapturedPhoto() : mBMCapturedPhoto == null");
            setShootingDialog(false);
            return;
        }

        // 기존 Storage Mode (Android P 이하)
        if (mIsExternalStorageLegacy) {
            saveCapturedPhotoUnderAndroidVer9();
        }
        // Scoped Mode (Android Q 이상)
        else {
            saveCapturedPhotoMoreAndroidVer10();
        }
    }

    private void save(byte[] bytes, File savedFile) throws IOException {

        OutputStream output = null;
        try {
            Log.d(TAG, "save() :  " + savedFile.getAbsolutePath());
            output = new FileOutputStream(savedFile);
            output.write(bytes);
        } finally {
            if (output != null) output.close();
        }
    }

    /**
     * 설명 : 촬영된 사진을 메모리에 저장 (Android 9이하 Storage Mode 방식)
     */
    private void saveCapturedPhotoUnderAndroidVer9() {
        try {
            File storeDir = new File(Environment.getExternalStorageDirectory(), Common.PHOTO_FILE_PATH);
            if (!storeDir.exists()) {
                if (!storeDir.mkdirs()) {
                    Log.e(TAG, "saveCapturedPhoto() : failed to create directory");
                    setShootingDialog(false);
                    return;
                }
            }

            String strPicPath = storeDir.getPath() + File.separator + mShootedPicName;
            File savedFile = new File(strPicPath);

            Bitmap bmUseWaterMark = getBitmatOverlayWaterMark();

            byte[] bytesUseWaterMark = Util.bitmapToByteArray(bmUseWaterMark);
            save(bytesUseWaterMark, savedFile);

            // Exif tag 쓰기
            Util.writeExifTag(strPicPath);
            mBMCapturedPhoto = null;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Log.e(TAG, "saveCapturedPhotoUnderAndroidVer9() : FileNotFoundException");
            setShootingDialog(false);
        } catch (IOException e) {
            e.printStackTrace();
            Log.e(TAG, "saveCapturedPhotoUnderAndroidVer9() : IOException");
            setShootingDialog(false);
        }
    }

    /**
     * 설명 : 촬영된 사진을 메모리에 저장 (Scoped Mode (Android Q 이상))
     */
    private void saveCapturedPhotoMoreAndroidVer10() {

        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.DISPLAY_NAME, mShootedPicName);
        values.put(MediaStore.Images.Media.MIME_TYPE, "image/*");
        String relativeLocation = Environment.DIRECTORY_DCIM + File.pathSeparator + Common.PHOTO_FILE_PATH;
        values.put(MediaStore.Images.Media.RELATIVE_PATH, "DCIM/images");
        // 파일을 write중이라면 다른곳에서 데이터요구를 무시하겠다는 의미입니다.
        values.put(MediaStore.Images.Media.IS_PENDING, 1);

        ContentResolver contentResolver = getContentResolver();
        //Uri collection = MediaStore.Images.Media.getContentUri(MediaStore.VOLUME_EXTERNAL_PRIMARY);
        Uri uriItem = contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        mShootedPicUri = uriItem;
        try {
            ParcelFileDescriptor pdf = contentResolver.openFileDescriptor(uriItem, "w", null);
            if (pdf == null) {
                Log.e(TAG, "saveCapturedPhotoMoreAndroidVer10() : ParcelFileDescriptor = null");
            } else {
                Bitmap bmUseWaterMark = getBitmatOverlayWaterMark();
                byte[] bytesUseWaterMark = Util.bitmapToByteArray(bmUseWaterMark);
                //byte[] strToByte = getBytes(inputStream);
                FileOutputStream fos = new FileOutputStream(pdf.getFileDescriptor());
                fos.write(bytesUseWaterMark);
                fos.close();
                pdf.close();
                contentResolver.update(uriItem, values, null, null);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Log.e(TAG, "saveCapturedPhotoMoreAndroidVer10() : FileNotFoundException");
            setShootingDialog(false);
        } catch (IOException e) {
            e.printStackTrace();
            Log.e(TAG, "saveCapturedPhotoMoreAndroidVer10() : IOException");
            setShootingDialog(false);
        }
        values.clear();
        // 파일을 모두 write하고 다른곳에서 사용할 수 있도록 0으로 업데이트를 해줍니다.
        values.put(MediaStore.Images.Media.IS_PENDING, 0);
        contentResolver.update(uriItem, values, null, null);

        /*try {
            ParcelFileDescriptor fd = contentResolver.openFileDescriptor(uriItem, "w", null);
            // Exif tag 쓰기
            Util.writeExifTag(fd.getFileDescriptor());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }*/

        mBMCapturedPhoto = null;
    }

    /**
     * 설명 : 촬영된 사진에 로고 워커마크를 추가한 Bitmap을 반환
     */
    private Bitmap getBitmatOverlayWaterMark() {

        Bitmap bmCapture = mBMCapturedPhoto;
        Bitmap bmOriginal = BitmapFactory.decodeResource(getResources(), R.drawable.logo_autobell);
        int nWatermarkWidth = (((mPhotoWidth * 100) / mTextureView.getWidth()) / 100) * 133;
        int width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, nWatermarkWidth, getResources().getDisplayMetrics());
        Bitmap bmWatermark = Util.resizeBitmap(bmOriginal, width);
        int margin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 15, getResources().getDisplayMetrics());
        Bitmap bmUseWaterMark = null;
        bmUseWaterMark = Util.overlayMark(bmCapture, bmWatermark, mPhotoWidth - bmWatermark.getWidth() - margin, mPhotoHeight - bmWatermark.getHeight() - margin);

        return bmUseWaterMark;
    }

    /**
     *  캡처된 사진을 DB에 입력
     **/
    private void insertPhotoToDB() {

        if(mCar_ID.length() > 0 ) {

            if(mSelectPositon > 0 && mSelectPositon <= mCntGuides) {

                int nPhotoID = 0;
                for(OverallShootGuide guide : overall_guides) {
                    if(guide.getGuide_sort_no() == mSelectPositon) {
                        nPhotoID = guide.getPhoto_id();
                    }
                }

                String strPicUri = "";
                if (mShootedPicUri != null) {
                    strPicUri = mShootedPicUri.toString();
                }

                if(nPhotoID > 0) {  // 이미 사진이 존재 (수정) 인 경우
                    Log.d(TAG, "updatePhotoToDB() => nPhotoID " + nPhotoID + ", mCar_ID = " + mCar_ID + ", mShootedPicGuideCode = " + mShootedPicGuideCode
                                 + ", mShootedPicName = " + mShootedPicName + ", mShootedPicUri = " + strPicUri);
                    photo_db.update(nPhotoID, mCar_ID, mShootedPicGuideCode, mShootedPicName, strPicUri);
                }else {             // 최초 사진 등록인 경우
                    Log.d(TAG, "insertPhotoToDB() => mCar_ID = " + mCar_ID + ", mShootedPicGuideCode = " + mShootedPicGuideCode
                                 + ", mShootedPicName = " + mShootedPicName + ", mShootedPicUri = " + strPicUri);
                    photo_db.insert(mCar_ID, mShootedPicGuideCode, mShootedPicName, strPicUri);
                }

                overall_guides.get(mSelectPositon-1).setExistPhoto(true);
                overall_guides.get(mSelectPositon-1).setPhoto_file_name(mShootedPicName);
                overall_guides.get(mSelectPositon-1).setPhoto_uri(strPicUri);

            }else {
                Log.e(TAG, "insertPhotoToDB() : mSelectPositon = " + mSelectPositon + " ERROR !!!");
                setShootingDialog(false);
                return;
            }

            mShootedPicName = "";
            mShootedPicGuideCode = "";

            // 저장된 사진 개수에 따라 차량등록상태(register_state)값 업데이트
            CarDBHelper car_db;
            car_db = new CarDBHelper(this, mDBPath, null, 1);

            JSONArray arrPhotos = photo_db.getResultByCarNumberForOverallType(mCar_ID);
            int nSize = arrPhotos.length();
            if(nSize == mCntGuides) {                       // 촬영 가이드 개수 만큼 촬영을 했다면 촬영 완료 상태로 업데이트
                car_db.update(mCarDB_ID, Common.MENU_TYPE_OVERALL_SHOOT, mCar_ID, Common.DETAIL_SHOOT_COMPLETE, nSize, mCntGuides, mCarType);
            }else if(nSize > 0 && nSize < mCntGuides) {     // 촬영 가이드 개수에 미치치 않으면 촬영중 상태로 업데이트
                car_db.update(mCarDB_ID, Common.MENU_TYPE_OVERALL_SHOOT, mCar_ID, Common.DETAIL_SHOOT_PROGRESS, nSize, mCntGuides, mCarType);
            }
        }
    }

    /**
     *  촬영 완료된 사진을 확인 또는 다음 촬영으로 전환(미완료) (Preview 전환)
     **/
    private void capturedPictureConfirm() {

        new Thread() {
            public void run() {
                Message msg = handler.obtainMessage();
                handler.sendMessage(msg);
            }
        }.start();
    }

    @SuppressLint("HandlerLeak")
    final Handler handler = new Handler() {

        public void handleMessage(Message msg) {

            // 확인 화면으로 전환 (360도 필요 촬영이 모두 캡쳐된 경우)
            if (mIsConfirm) {
                // 처음 촬영된 이미지를 로드한다
                String strFileName = overall_guides.get(0).getPhoto_file_name();
                if(strFileName.length() > 1) {
                    if (mIsExternalStorageLegacy) {
                        String strPhotoPath = Util.getPhotoFilePath() + strFileName;
                        File imgFile = new File(strPhotoPath);
                        if (imgFile.exists()) {
                            Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                            iv_shooted_picture.setImageBitmap(myBitmap);
                        }
                    }else {
                        String strFileUri = overall_guides.get(0).getPhoto_uri();
                        if(strFileUri.length() == 0) {
                            Log.e(TAG, "capturedPictureConfirm() getPhoto_uri() fail...");
                            return;
                        }
                        ContentResolver cr = getApplicationContext().getContentResolver();
                        Uri i = Uri.parse(strFileUri);
                        try {
                            InputStream is = cr.openInputStream(i);
                            Bitmap bitmap = BitmapFactory.decodeStream(is);
                            iv_shooted_picture.setImageBitmap(bitmap);
                            if (is != null)
                                is.close();
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }else{
                    Log.e(TAG, "capturedPictureConfirm() getPhoto_file_name() fail...");
                }

                iv_shooted_picture.setVisibility(View.VISIBLE);
                btn_reshooting.setVisibility(View.VISIBLE);
                btn_upload_pic.setVisibility(View.VISIBLE);
                btn_show_guide.setVisibility(View.VISIBLE);

                btn_exit.setVisibility(View.GONE);
                iv_watermark.setVisibility(View.GONE);
                iv_navi_guide.setVisibility(View.GONE);
                mTextureView.setVisibility(View.GONE);
                btn_capture.setVisibility(View.GONE);

                // SharedPreference 를 선언한다.
                SharedPreferences pref = getSharedPreferences(Common.PREFERENCE, MODE_PRIVATE);
                bDontShowConfirm = pref.getBoolean(Common.DONT_SHOT_OVERALL_CONFIRM_GUIDE, false);

                if(bDontShowConfirm == false) {
                    rl_confirm_guide.setVisibility(View.VISIBLE);
                }else {
                    rl_confirm_guide.setVisibility(View.GONE);

                    tv_info_current_shoot.setText(getString(R.string.use_shooted_360pictures));
                    tv_info_current_shoot.setVisibility(View.VISIBLE);
                    tv_info_current_shoot.startAnimation(animMsgTransDown);
                }
            }
            // 다음 가이드 촬영 준비
            else {
                iv_shooted_picture.setVisibility(View.GONE);
                btn_reshooting.setVisibility(View.GONE);
                btn_upload_pic.setVisibility(View.GONE);
                btn_show_guide.setVisibility(View.GONE);

                btn_exit.setVisibility(View.VISIBLE);
                iv_navi_guide.setVisibility(View.VISIBLE);
                iv_guide_line.setVisibility(View.VISIBLE);
                mTextureView.setVisibility(View.VISIBLE);
                btn_capture.setVisibility(View.VISIBLE);

                // 다음 촬영 체크 후, 진행
                setContinuousShooting();

                startPreview();
            }
        }
    };

    private void updatePhotoCount() {
        JSONArray arrPhotos = photo_db.getResultByCarNumberForOverallType(mCar_ID);
        int nSize = arrPhotos.length();
        // 촬영된 사진 수 / 최대 촬영 수 표시
        Log.d(TAG, "updatePhotoCount() => Shooted Photos = " + nSize + ", Max Photos = " + mCntGuides);
        String sCount = String.format("%02d / %02d", nSize, mCntGuides);
        //tv_pic_count.setText(sCount);
    }

    // textureView가 화면에 정상적으로 출력되면 onSurfaceTextureAvailable()호출
    private TextureView.SurfaceTextureListener mSurfaceTextureListener =
            new TextureView.SurfaceTextureListener() {
                @Override
                public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
                    // cameraManager생성하는 메소드
                    Log.d(TAG, "mSurfaceTextureListener onSurfaceTextureAvailable : Width = " + width + ", Height = " + height);
                    openCamera(width, height);
                }

                @Override
                public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
                    Log.d(TAG, "mSurfaceTextureListener onSurfaceTextureSizeChanged : Width = " + width + ", Height = " + height);
                    configureTransform(width, height);
                }

                @Override
                public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
                    return false;
                }

                @Override
                public void onSurfaceTextureUpdated(SurfaceTexture surface) {

                }
            }; //TextureView.SurfaceTextureListener

    /**
     * 1. CameraManager 생성
     * 2. 카메라에 관한 정보 얻기
     * 3. openCamera() 호출 → CameraDevice객체 생성
     */
    private void openCamera(int width, int height) {
        // CameraManager 객체 생성
        mCameraManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        try {
            // default 카메라를 선택한다.
            String cameraId = mCameraManager.getCameraIdList()[0];

            // 카메라 특성 알아보기
            CameraCharacteristics characteristics = mCameraManager.getCameraCharacteristics(cameraId);
            int level = characteristics.get(CameraCharacteristics.INFO_SUPPORTED_HARDWARE_LEVEL);
            Range<Integer> fps[] = characteristics.get(CameraCharacteristics.CONTROL_AE_AVAILABLE_TARGET_FPS_RANGES);
            Log.d(TAG, "maximum frame rate is :" + fps[fps.length - 1] + "hardware level = " + level);

            // StreamConfigurationMap 객체에는 카메라의 각종 지원 정보가 담겨있다.
            map = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);

            // 미리보기용 textureview 화면크기용을 설정 <- 제공할 수 있는 최대크기를 가리킨다.
            mPreviewSize = map.getOutputSizes(SurfaceTexture.class)[0];
            Size maxSize = map.getOutputSizes(SurfaceTexture.class)[0];
            // 가로 사이즈 기준으로 16:9 비율로 조정
            int nWidth = maxSize.getWidth();
            // 가로 화면 대비 세로 화면 크기
            int nHeight = (nWidth / 16) * 9;
            Size settedSize = new Size(nWidth, nHeight);
            Log.d(TAG, "openCamera mPreviewSize : Width = " + nWidth + ", Height = " + nHeight);
            mPreviewSize = settedSize;

            Range<Integer> fpsForVideo[] = map.getHighSpeedVideoFpsRanges();
            //Log.e(TAG, "for video :" + fpsForVideo[fpsForVideo.length - 1] + " preview Size width:" + mPreviewSize.getWidth() + ", height" + height);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                // 권한에 대한
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                        ContextCompat.checkSelfPermission(this, Manifest.permission.READ_MEDIA_IMAGES) != PackageManager.PERMISSION_GRANTED ||
                        ContextCompat.checkSelfPermission(this, Manifest.permission.READ_MEDIA_VIDEO) != PackageManager.PERMISSION_GRANTED ||
                        ContextCompat.checkSelfPermission(this, Manifest.permission.READ_MEDIA_AUDIO) != PackageManager.PERMISSION_GRANTED) {

                    Toast.makeText(OverallShootActivity.this, "권한 획득 실패", Toast.LENGTH_SHORT).show();
                } else {
                    configureTransform(width, height);
                    // CameraDevice생성
                    mCameraManager.openCamera(cameraId, mStateCallback, null);
                }
            }else {
                // 권한에 대한
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                        ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                        ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                    Toast.makeText(OverallShootActivity.this, "권한 획득 실패", Toast.LENGTH_SHORT).show();
                } else {
                    configureTransform(width, height);
                    // CameraDevice생성
                    mCameraManager.openCamera(cameraId, mStateCallback, null);
                }
            }

        } catch (CameraAccessException e) {
            Log.e(TAG, "openCamera() :카메라 디바이스에 정상적인 접근이 안됩니다.");
        }
    }

    private CameraDevice.StateCallback mStateCallback
            = new CameraDevice.StateCallback() {
        @Override
        public void onOpened(@NonNull CameraDevice camera) {
            // CameraDevice 객체 생성
            mCameraDevice = camera;
            // CaptureRequest.Builder객체와 CaptureSession 객체 생성하여 미리보기 화면을 실행시킨다.
            startPreview();

            if (null != mTextureView) {
                configureTransform(mTextureView.getWidth(), mTextureView.getHeight());
            }
        }

        @Override
        public void onDisconnected(@NonNull CameraDevice camera) {
        }

        @Override
        public void onError(@NonNull CameraDevice camera, int error) {
        }
    };

    /**
     * startPreview() > CaptureRequest.Builder 객체 와 CaptureSession객체 생성
     */
    private void startPreview() {
        if( mCameraDevice==null ||
                !mTextureView.isAvailable() || mPreviewSize == null) {
            Log.e(TAG, "startPreview() fail , return ");
            return;
        }

        SurfaceTexture texture = mTextureView.getSurfaceTexture();
        Surface surface = new Surface(texture);
        try {
            mPreviewBuiler = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
        } catch (CameraAccessException e) {
            Log.e(TAG, "CaptureRequest 객체 생성 실패");
            e.printStackTrace();
        }
        mPreviewBuiler.addTarget(surface);

        try {
            mCameraDevice.createCaptureSession(Arrays.asList(surface),  // / 미리보기용으로 위에서 생성한 surface객체 사용
                    new CameraCaptureSession.StateCallback() {
                        @Override
                        public void onConfigured(@NonNull CameraCaptureSession session) {
                            mPreviewSession = session;
                            updatePreview();
                        }

                        @Override
                        public void onConfigureFailed(@NonNull CameraCaptureSession session) {

                        }
                    }, null );

        } catch (CameraAccessException e) {
            Log.e(TAG, "CaptureSession 객체 생성 실패");
            e.printStackTrace();
        }
    }

    /**
     * updatePreview()에서 CaptureRequest객체를 카메라 시스템에 전달
     */
    private void updatePreview() {
        if(mCameraDevice == null) {
            return;
        }
        mPreviewBuiler.set(CaptureRequest.CONTROL_AF_MODE, CameraMetadata.CONTROL_AF_MODE_CONTINUOUS_PICTURE); //old : CONTROL_MODE, CONTROL_MODE_AUTO

       /* HandlerThread thread = new HandlerThread("CameraPreview");
        thread.start();
        Handler backgroundHandler = new Handler(thread.getLooper());
        try {
            mPreviewSession.setRepeatingRequest(mPreviewBuiler.build(), null, backgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }*/

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    mPreviewSession.setRepeatingRequest(mPreviewBuiler.build(),
                            null, null);
                    setShootingDialog(false);
                    //btn_capture.setVisibility(View.VISIBLE);
                } catch (CameraAccessException e) {
                    Log.e(TAG, "Failed to start camera preview because it couldn't access camera", e);
                } catch (IllegalStateException e) {
                    Log.e(TAG, "Failed to start camera preview.", e);
                }
            }
        }, 500);
    }

    /**
     * Configures the necessary {@link Matrix} transformation to `mTextureView`.
     * This method should not to be called until the camera preview size is determined in
     * openCamera, or until the size of `mTextureView` is fixed.
     *
     * @param viewWidth  The width of `mTextureView`
     * @param viewHeight The height of `mTextureView`
     */
    private void configureTransform(int viewWidth, int viewHeight) {

        if (null == mTextureView || null == mPreviewSize) {
            return;
        }
        Log.d(TAG, "configureTransform viewWidth = " + viewWidth + ", viewHeight = " + viewHeight);
        int rotation = getWindowManager().getDefaultDisplay().getRotation();
        Matrix matrix = new Matrix();
        RectF viewRect = new RectF(0, 0, viewWidth, viewHeight);
        RectF bufferRect = new RectF(0, 0, mPreviewSize.getHeight(), mPreviewSize.getWidth());
        float centerX = viewRect.centerX();
        float centerY = viewRect.centerY();
        if (Surface.ROTATION_90 == rotation || Surface.ROTATION_270 == rotation) {
            bufferRect.offset(centerX - bufferRect.centerX(), centerY - bufferRect.centerY());
            matrix.setRectToRect(viewRect, bufferRect, Matrix.ScaleToFit.FILL);
            float scale = Math.max(
                    (float) viewHeight / mPreviewSize.getHeight(),
                    (float) viewWidth / mPreviewSize.getWidth());
            matrix.postScale(scale, scale, centerX, centerY);
            matrix.postRotate(90 * (rotation - 2), centerX, centerY);
        }
        mTextureView.setTransform(matrix);
    }

    /**
     * 촬영 완료된 가이드(차량 부위) 이후 연속으로 다음 가이드 및 촬영 모드 설정
     */
    public void setContinuousShooting() {

        // 가이드 마지막 촬영 순번까지 촬영 여부
        if (mSelectPositon == mCntGuides) {

            mSelectPositon = 1;
            int resGuideID = getGuideLineResource();
            iv_guide_line.setBackgroundResource(resGuideID);
            iv_guide_line.setVisibility(View.GONE);

            Log.d(TAG, "setContinuousShooting() - Finished!");
            mIsConfirm = true;
            capturedPictureConfirm();
            return;

        } else {
            mSelectPositon = mSelectPositon + 1;
            Log.d(TAG, "setContinuousShooting() - Next Shoot Position = " + mSelectPositon);
        }

        // 360도 방향 가이드 설정
        int getNaviGuidePos = mSelectPositon - 1;
        int resID = arrOverallNaviGuides.getResourceId(getNaviGuidePos, 0);
        iv_navi_guide.setImageResource(resID);

        // 360 촬영 가이드
        int resGuideID = getGuideLineResource();
        iv_guide_line.setBackgroundResource(resGuideID);

        // 다음 촬영 안내 메시지 출력
        showShootInfoText();
    }

    /**
     * 촬영 안내 Toast를 출력
     */
    private void showShootInfoToast(String strText, int nDuration) {

        LayoutInflater inflater = getLayoutInflater();
        View toastDesign = inflater.inflate(R.layout.toast_next_shoot_info, (ViewGroup)findViewById(R.id.toast_design_root)); //toast_design.xml 파일의 toast_design_root 속성을 로드
        TextView text = toastDesign.findViewById(R.id.tv_toast_design);
        text.setText(strText);

        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER, 0, - (Util.getScreenHeight() / 3)); // CENTER를 기준으로 0, 상단 위치에 메시지 출력
        toast.setDuration(nDuration);
        toast.setView(toastDesign);
        toast.show();
    }

    /**
     * 설명 : 현재 촬영 부위 안내 문구 출력
     */
    private void showShootInfoText() {

        //String strGuideName = getGuideName(mSelectPositon);
        String strGuideText = arrGuidesText.getString(mSelectPositon-1);
        tv_info_current_shoot.setText(strGuideText);
        tv_info_current_shoot.setVisibility(View.VISIBLE);
        tv_info_current_shoot.startAnimation(animMsgTransDown);
    }

    /**
     * @return 아직 촬영되지 않은 가장 빠른 촬영 가이드 순번 (0 : 모든 촬영 완료)
     */
    private int getNoPhotoPosition() {

        // 촬영 가이드 중 촬영되지 않는 곳이 있는지 체크
        for(int i = 0; i < overall_guides.size(); i++) {
            boolean existPhoto = overall_guides.get(i).getExistPhoto();
            if( false == existPhoto ) {
                return overall_guides.get(i).getGuide_sort_no();
            }
        }
        return 0;
    }

    private void setShootingDialog(boolean show){

        if(mShootingDialog != null) {
            if (show) {
                mShootingDialog.show();
            } else {
                mShootingDialog.dismiss();
            }
        }
    }

    private void setLoadingDialog(boolean show){

        if(mLoadingDialog != null) {
            if (show) {
                Log.d(TAG, "setLoadingDialog() => SHOW");
                mLoadingDialog.show();
            } else {
                Log.d(TAG, "setLoadingDialog() => DISMISS");
                mLoadingDialog.dismiss();
            }
        }
    }

    public Point getScreenSize(AppCompatActivity activity) {
        Display display = activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return  size;
    }

    /**
     * 현재 선택된 가이드(차량 부위)를 기준으로 저장할 사진 파일명을 설정
     * 선택된 가이드 순번으로 촬영가이드코드를 읽어온다.
     * @return 저장할 사진 파일 명
     */
    public String getPicFileName() {

        if (mSelectPositon == 0) {
            return "";
        }

        String strPicFileName = "";
        String strGuideCode = getGuideCode(mSelectPositon);
        if (strGuideCode.length() < 2) {
            return "";
        }
        strPicFileName = strGuideCode + Common.PHOTO_FILE_EXTENSION;
        //strPicFileName = mCar_ID + "_" + strGuideCode + Common.PHOTO_FILE_EXTENSION;

        return strPicFileName;
    }

    /**
     * 선택된 가이드 위치값으로 촬영가이드코드를 읽어온다.
     * @return 선택된 촬영가이드 코드
     */
    public String getGuideCode(int nSelPos) {

        for(OverallShootGuide guide : overall_guides) {
            if(guide.getGuide_sort_no() == nSelPos) {
                return guide.getGuide_code();
            }
        }
        return "";
    }

    /**
     * 선택된 가이드 위치값으로 촬영가이드명을 읽어온다.
     * @return 선택된 촬영가이드 명
     */
    public String getGuideName(int nSelPos) {

        for(OverallShootGuide guide : overall_guides) {
            if(guide.getGuide_sort_no() == nSelPos) {
                return guide.getGuide_name();
            }
        }
        return "";
    }

    /**
     * 현재 선택된 가이드(차량 부위) 버튼에 촬영된 사진을 배경으로 설정
     * 설정 순서는 외부촬영가이드 다음 내부촬영가이드 순으로 한다 (따라서 내부 Position은 (외부 개수 + 내부순서)이다)
     * @param photo_id 촬영된 사진 DB id
     * @param photo_guide_code 촬영된 사진 가이드 코드
     * @param path 촬영된 사진 파일 경로
     */
    public void setPhotoToGuideBtn(int photo_id, String photo_guide_code, String path) {

        File imgFile = new File(path);
        if (imgFile.exists()) {
            // 썸네일 추출
            Bitmap thumbnail = ThumbnailUtils.extractThumbnail(BitmapFactory.decodeFile(path), 178, 100);
            Log.d(TAG, "setPhotoToGuideBtn Image Path : " + path);
            // bitmap 타입을 drawable로 변경
            //Drawable drThumb = new BitmapDrawable(getResources(), thumbnail);

            int nPosition = 0;
            // 외부 촬영 가이드의 사진인 경우
            for(OverallShootGuide guideData : overall_guides) {
                String sGuideCode = guideData.getGuide_code();
                if(photo_guide_code.equals(sGuideCode)) {
                    // 촬영된 이미지가 있을 경우 배경에 가득 채운다
                    RelativeLayout.LayoutParams roundlp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
                    roundlp.addRule(RelativeLayout.CENTER_IN_PARENT);

                    overall_guides.get(nPosition).setExistPhoto(true);
                    //overall_guides.get(nPosition).setPhoto_id(photo_id);
                    return;
                }else {
                    nPosition++;
                }
            }
        }
    }

    /**
     * 설명 : 촬영된 사진이 존재하는 순서 순번을 가져오기
     * @param photo_id 촬영된 사진 DB id
     * @param photo_guide_code 촬영된 사진 가이드 코드
     * @param path 촬영된 사진 파일 경로
     * @param file_name 촬영된 사진 파일 명
     */
    public int getExistShootPosition(int photo_id, String photo_guide_code, String path, String  file_name) {

        int nPosition = 0;
        File imgFile = new File(path);
        if (imgFile.exists()) {

            for(OverallShootGuide guideData : overall_guides) {
                String sGuideCode = guideData.getGuide_code();
                if(photo_guide_code.equals(sGuideCode)) {
                    overall_guides.get(nPosition).setExistPhoto(true);
                    overall_guides.get(nPosition).setPhoto_file_name(file_name);
                    return nPosition + 1;
                }else {
                    nPosition++;
                }
            }
        }else {
            return -1;
        }
        return -1;
    }

    /**
     * 설명 : 촬영된 사진의 Uri 로 사진 파일 삭제
     * @return {uri} 촬영 사진의 URI
     */
    public void deletePhotoByUri(Uri uri) {
        try {
            getContentResolver().delete(uri, null, null);
        }catch (Exception e) {
            e.printStackTrace();
        }

    }

    public class LoadingPhotos extends Thread {
        @Override
        public void run() {
            setPhotosToGuideBtns();
        }
    }

    private static int getResourceID(String resName, Context context) throws PackageManager.NameNotFoundException {

        Context resContext = context.createPackageContext(context.getPackageName(), 0);
        Resources res = resContext.getResources();

        int id = res.getIdentifier(resName, "drawable", context.getPackageName());
        if (id == 0) {
            return 0;
        } else
            return id;
    }
}