package glovis.glovisaa.autobell.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.Build;
import android.os.IBinder;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import glovis.glovisaa.autobell.MainActivity;
import glovis.glovisaa.autobell.R;
import glovis.glovisaa.autobell.util.Common;
import glovis.glovisaa.autobell.util.Util;
import m.client.push.library.common.PushConstants;

public class OhMyCallService extends Service {

    public static final String TAG = Common.COMPANY + "OhMyCallSer";

    public static Intent serviceIntent = null;

    public static final String EXTRA_CALL_NUMBER = "call_number";

    protected View rootView;

    public static final String NOTIFICATION_CHANNEL_ID = "10001";

    @BindView(R.id.iv_car_img)
    ImageView iv_car_img;
    @BindView(R.id.tv_model_name)
    TextView tv_model_name;
    @BindView(R.id.tv_mileage)
    TextView tv_mileage;
    @BindView(R.id.tv_region)
    TextView tv_region;
    @BindView(R.id.tv_reg_date)
    TextView tv_reg_date;
    @BindView(R.id.tv_fuel_type)
    TextView tv_fuel_type;
    @BindView(R.id.tv_selling_price)
    TextView tv_selling_price;

    /*@BindView(R.id.webview)
    WebView mWebview;*/

    String str_ext = "";
    String car_number;  String car_img_url;  String webpage_url;
    WindowManager.LayoutParams params;
    private WindowManager windowManager;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        // Not used
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "OhMyCallService 호출됨");

        windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);

        DisplayMetrics disp = getApplicationContext().getResources().getDisplayMetrics();
        //int deviceWidth = (disp.widthPixels * 90) / 100;    // Device 넓이 사이즈의 90%
        //int deviceHeight = (disp.heightPixels * 50) / 100;  // Device 높이 사이즈의 50%
        int deviceWidth = disp.widthPixels;    // Device 넓이 사이즈의 100%
        int deviceHeight = (disp.heightPixels * 65) / 100;  // Device 높이 사이즈의 65%

        params = new WindowManager.LayoutParams(
                deviceWidth,
                deviceHeight,/*WindowManager.LayoutParams.WRAP_CONTENT,*/
                WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                        | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                        | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
                        | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
                        | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH
                        | WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                PixelFormat.TRANSLUCENT);

        params.gravity = Gravity.TOP;

        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        rootView = layoutInflater.inflate(R.layout.ohmycall_popup_top, null);
        ButterKnife.bind(this, rootView);

        setDraggable();
    }

    private void setDraggable() {

        rootView.setOnTouchListener(new View.OnTouchListener() {
            private int initialX;
            private int initialY;
            private float initialTouchX;
            private float initialTouchY;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {

                    case MotionEvent.ACTION_DOWN:
                        initialX = params.x;
                        initialY = params.y;
                        initialTouchX = event.getRawX();
                        initialTouchY = event.getRawY();
                        return true;

                    case MotionEvent.ACTION_UP:
                        Log.d(TAG, "MotionEvent.ACTION_UP => webpage_url : " + webpage_url);
                        if(webpage_url.length() > 1) {
                            Intent dialogIntent = new Intent(getApplicationContext(), MainActivity.class);
                            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            dialogIntent.putExtra("webpage_url", webpage_url);
                            startActivity(dialogIntent);

                            removePopup();
                        }
                        return true;

                    /*case MotionEvent.ACTION_MOVE:
                        params.x = initialX + (int) (event.getRawX() - initialTouchX);
                        params.y = initialY + (int) (event.getRawY() - initialTouchY);

                        if (rootView != null)
                            windowManager.updateViewLayout(rootView, params);
                        return true;*/
                }
                return false;
            }
        });
    }

    @Override public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand() 호출됨");

        serviceIntent = intent;

        if (rootView.getWindowToken() == null) {
            if ( Util.checkDrawOverlayPermission(getApplicationContext()) ) {
                windowManager.addView(rootView, params);
            }
        }

        setExtra(intent);

        return START_STICKY;
    }

    public void initializeNotification() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            NotificationCompat.Builder builder = new NotificationCompat.Builder(this, "1");
            builder.setSmallIcon(R.mipmap.ic_launcher);
            NotificationCompat.BigTextStyle style = new NotificationCompat.BigTextStyle();
            style.bigText("설정을 보려면 누르세요.");
            style.setBigContentTitle(null);
            style.setSummaryText("서비스 동작중");
            builder.setContentText(null);
            builder.setContentTitle(null);
            builder.setOngoing(true);
            builder.setStyle(style);
            builder.setWhen(0);
            builder.setShowWhen(false);
            Intent notificationIntent = new Intent(this, MainActivity.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
            builder.setContentIntent(pendingIntent);
            NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            manager.createNotificationChannel(new NotificationChannel("1", "calling_service", NotificationManager.IMPORTANCE_NONE));

            Notification notification = builder.build();
            startForeground(1, notification);
        }
    }

    private void setExtra(Intent intent) {

        if (intent == null) {
            removePopup();
            return;
        }

        str_ext = intent.getStringExtra(PushConstants.KEY_EXT);
        if(str_ext == null) {
            removePopup();
            return;
        }

        try {
            JSONObject jsonObject = new JSONObject(str_ext);
            car_img_url = jsonObject.getString("image_url");
            new Util.DrawUrlImageTask(iv_car_img).execute(car_img_url);

            tv_model_name.setText(jsonObject.getString("model_name"));
            tv_mileage.setText(jsonObject.getString("mileage"));
            tv_region.setText(jsonObject.getString("region"));
            tv_reg_date.setText(jsonObject.getString("reg_date"));
            tv_fuel_type.setText(jsonObject.getString("fuel_type"));
            tv_selling_price.setText(jsonObject.getString("selling_price"));
            webpage_url = jsonObject.getString("webpage_url");

        }catch (JSONException e) {
            e.printStackTrace();
            removePopup();
            return;
        }
    }

    @Override public void onDestroy() {
        Log.d(TAG, "called onDestroy");
        super.onDestroy();
        serviceIntent = null;
        removePopup();
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
    }

    @OnClick(R.id.btn_close)
    public void removePopup() {
        Log.d(TAG, "removePopup()");
        try {
            if (rootView != null && windowManager != null) {
                windowManager.removeView(rootView);
            }
        }catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG, "removePopup() catch...");
        }

    }

}
