package glovis.glovisaa.autobell.service;

import org.json.JSONObject;

import java.util.Map;

import glovis.glovisaa.autobell.util.Common;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;

public interface ApiService {
    @Multipart
    @POST(Common.MOBILE_UPLOAD_URL)
    Call<JSONObject> uploadImage(@Part MultipartBody.Part file, @PartMap Map<String, RequestBody> info);
    //Call<JSONObject> uploadImage(@Part MultipartBody.Part[] files, @PartMap Map<String, RequestBody>[] infos);
}