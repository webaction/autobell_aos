package glovis.glovisaa.autobell;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.RectF;
import android.graphics.SurfaceTexture;
import android.graphics.drawable.BitmapDrawable;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.Image;
import android.media.ImageReader;
import android.media.MediaActionSound;
import android.media.ThumbnailUtils;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.util.Log;
import android.util.Range;
import android.util.Size;
import android.util.SparseIntArray;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import glovis.glovisaa.autobell.data.ShootGuide;
import glovis.glovisaa.autobell.db.CarDBHelper;
import glovis.glovisaa.autobell.db.GuideDBHelper;
import glovis.glovisaa.autobell.db.PhotoDBHelper;
import glovis.glovisaa.autobell.util.Common;
import glovis.glovisaa.autobell.util.RoundImageView;
import glovis.glovisaa.autobell.util.Util;

/**
 * 무평가 촬영 Activity
 */
public class PassEvaluationShootActivity extends AppCompatActivity {

    private TextureView mTextureView;
    private CameraDevice mCameraDevice;
    private CaptureRequest.Builder mPreviewBuiler;
    private CameraCaptureSession mPreviewSession;
    private CameraManager mCameraManager;
    // 카메라 설정에 관한 변수
    private Size mPreviewSize;
    private StreamConfigurationMap map;

    private Dialog mDialog = null;

    // 차량 ID
    private int mCarID;
    private String mCarNumber;
    private String mCarModel;
    // 차량 사진 DB
    private PhotoDBHelper photo_db = null;
    private String mDBPath = "";

    // 차량 타입 선택 값
    private int mCurCarType = 1;                        // 1 : 세단, 2: VAN A, 3: VAN B, 4 : 경차
    private boolean mIsCarTypeSelecting = false;        // 차량 타입 선택 중 여부
    // 차량 타입 선택 버튼
    private ImageView btn_car_type_1;
    private ImageView btn_car_type_2;
    private ImageView btn_car_type_3;
    private ImageView btn_car_type_4;
    // 촬영 타입 선택 값
    private int mCurShootType = 1;                      // 1 : 연속, 2: 수동
    private boolean mIsShootTypeSelecting = false;      // 촬영 타입 선택 중 여부
    // 촬영 타입(연속/수동) 선택 버튼
    private ImageView btn_shoot_type_1;
    private ImageView btn_shoot_type_2;
    // 무평가 촬영 선택 상태
    private int mShootSelectState = 0;                  // 0: 선택해제(초기), 1 : 촬영 GuideLine 선택 상태
    private ImageView btn_shoot_select;                 // 무평가 촬영 가이드 선택 버튼
    private ImageView btn_capture;                      // 촬영 버튼
    private TextView tv_pic_count;                      // 사진 촬영 수 표시
    private HorizontalScrollView scroll_shoot_select;   // 촬영 가이드 선택 화면
    private LinearLayout ll_guide_select;               // 촬영 가이드 선택 버튼을 담는 뷰
    private ImageView iv_guide_line;                    // 촬영 가이드 라인
    private ImageView btn_blind;                        // 가리기 도구 버튼
    private ImageView iv_blind;                         // 가리기 도구
    private int mPhotoWidth = 0;                        // 촬영 사진 가로 사이즈
    private int mPhotoHeight = 0;                       // 촬영 사진 세로 사이즈

    private ImageView iv_shooted_picture;               // 촬영된 사진 보기
    private TextView tv_question_use_pic;               // 촬영된 사진 확인 문구
    private TextView tv_info_current_shoot;             // 현재 촬영 부분 알림 문구
    private ImageView btn_reshooting;                   // 재촬영 버튼
    private ImageView btn_use_pic;                      // 촬영 사진 사용 버튼
    private boolean mIsConfirm;                         // 촬영 확인 상태(true : 확인 대기, false : 취소)
    private boolean mIsReShooting = false;              // 재촬영 상태
    private boolean mUseBlind = false;                  // 가리기 도구 사용 상태(true : 가리기, false : 가리기 취소)
    private int mBlindPositionX = 0;                    // 가리기 도구 X 위치
    private int mBlindPositionY = 0;                    // 가리기 도구 Y 위치
    private int mLastBlindPosX  = 0;                    // 가리기 도구 X 위치
    private int mLastBlindPosY  = 0;                    // 가리기 도구 Y 위치
    private String mShootedPicName;                     // 촬영 사진 파일 명
    private Bitmap mBMCapturedPhoto = null;             // 촬영된 Image's Bitmap

    // 버튼 이동 애니메이션
    private Animation animTransRight_2;
    private Animation animTransRight_3;
    private Animation animTransRight_4;
    private Animation animTransLeft_2;
    private Animation animTransLeft_3;
    private Animation animTransLeft_4;

    // 촬영 선택창 이동 애니메이션
    private Animation animTransUp;
    private Animation animTransDown;

    // 메시지 View 이동 애니메이션
    private Animation animMsgTransDown;
    private Animation animMsgTransUp;

    private int mSelectPositon = 0;          // 촬영 가이드 선택 값
    private String mShootedPicGuideCode;        // 촬영 사진 가이드 코드
    private int mCntGuides = 0;          // 촬영 가이드 개수

    private ArrayList<ShootGuide> shoot_guides = new ArrayList();           // 촬영 가이드 목록
    private ArrayList<RelativeLayout> guide_btns = new ArrayList();         // 촬영 가이드 버튼
    private ArrayList<RoundImageView> guide_btn_images = new ArrayList();   // 촬영 가이드 내 이미지

    private static final String TAG = Common.COMPANY + "PASSEVAL_SHOOT";

    static final int REQUEST_CAMERA = 100;

    /**
     * Conversion from screen rotation to JPEG orientation.
     */
    private static final SparseIntArray ORIENTATIONS = new SparseIntArray();

    static {
        ORIENTATIONS.append(Surface.ROTATION_0, 0);
        ORIENTATIONS.append(Surface.ROTATION_90, 90);
        ORIENTATIONS.append(Surface.ROTATION_180, 180);
        ORIENTATIONS.append(Surface.ROTATION_270, 270);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        // 등록된 차량 ID
        mCarID = getIntent().getIntExtra("car_id", 0);
        mCarNumber = getIntent().getStringExtra("car_number");
        mCarModel = getIntent().getStringExtra("car_model");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            // 권한이 있을 경우에만 layout을 전개한다.
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(this, Manifest.permission.READ_MEDIA_IMAGES) != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(this, Manifest.permission.READ_MEDIA_VIDEO) != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(this, Manifest.permission.READ_MEDIA_AUDIO) != PackageManager.PERMISSION_GRANTED) {
                // 권한이 없으면 권한을 요청한다.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.CAMERA,
                                Manifest.permission.READ_MEDIA_IMAGES,
                                Manifest.permission.READ_MEDIA_VIDEO,
                                Manifest.permission.READ_MEDIA_AUDIO},
                        REQUEST_CAMERA);
                //Toast.makeText(DetailShootActivity.this, "권한 획득 실패", Toast.LENGTH_SHORT).show();
            } else {
                initLayout();

                mDBPath = Common.DB_NAME;

                if (photo_db == null) {
                    photo_db = new PhotoDBHelper(this, mDBPath, null, 1);
                }

                // Set Test DB Data
                setTestData();

                // 촬영 가이드 목록을 DB로부터 가져와 외/내부 촬영 가이드 버튼 생성
                setShootGuides();

                // 이전에 촬영한 사진이 있다면 불러와서 외/내부 버튼에 넣는다
                setPhotosToGuideBtns();
            }
        }else {
            // 권한이 있을 경우에만 layout을 전개한다.
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                // 권한이 없으면 권한을 요청한다.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.CAMERA,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE},
                        REQUEST_CAMERA);
                //Toast.makeText(DetailShootActivity.this, "권한 획득 실패", Toast.LENGTH_SHORT).show();
            } else {
                initLayout();

                mDBPath = Common.DB_NAME;

                if (photo_db == null) {
                    photo_db = new PhotoDBHelper(this, mDBPath, null, 1);
                }

                // Set Test DB Data
                setTestData();

                // 촬영 가이드 목록을 DB로부터 가져와 외/내부 촬영 가이드 버튼 생성
                setShootGuides();

                // 이전에 촬영한 사진이 있다면 불러와서 외/내부 버튼에 넣는다
                setPhotosToGuideBtns();
            }
        }
    }

    private void initLayout() {

        // 소프트키 숨기기
        View decorView = getWindow().getDecorView();
        // Hide both the navigation bar and the status bar.
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
        // 소프트키 숨기기 End

        setContentView(R.layout.activity_passevaluation_shoot);
        mTextureView = findViewById(R.id.cameraTextureView);

        // Camera Preview 크기(16:9)로 지정
        Point p = getScreenSize(this);
        // 화면 세로 최대크기
        int nMaxHeight = p.y;
        // 세로 화면 대비 가로 화면 크기
        int nPreviewWidth = (nMaxHeight / 9) * 16;
        mTextureView.getLayoutParams().height = nMaxHeight;
        mTextureView.getLayoutParams().width = nPreviewWidth;
        String log = "initLayout - Set TextuewView Size : Width = " + nPreviewWidth + ", Height = " + nMaxHeight;
        Log.d(TAG, log);
        mTextureView.requestLayout();

        mTextureView.setSurfaceTextureListener(mSurfaceTextureListener);

        // 촬영 가이드라인
        iv_guide_line = findViewById(R.id.iv_guide_line);
        int nGuideLineWidth = nPreviewWidth/*(nPreviewWidth * 68) / 100*/;
        int nGuideLineHeight = (nGuideLineWidth * 47) / 100;
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(nGuideLineWidth, nGuideLineHeight);
        layoutParams.setMargins((nPreviewWidth - nGuideLineWidth) / 2, (nMaxHeight - nGuideLineHeight) / 2, 0, 0);
        iv_guide_line.setLayoutParams(layoutParams);

        // 촬영 진행 Dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(R.layout.progress_shooting);
        mDialog = builder.create();

        // 차량 타입 선택
        btn_car_type_1 = findViewById(R.id.btn_car_type_1);
        btn_car_type_2 = findViewById(R.id.btn_car_type_2);
        btn_car_type_3 = findViewById(R.id.btn_car_type_3);
        btn_car_type_4 = findViewById(R.id.btn_car_type_4);

        btn_car_type_1.setOnClickListener(mBtnClickListener);
        btn_car_type_2.setOnClickListener(mBtnClickListener);
        btn_car_type_3.setOnClickListener(mBtnClickListener);
        btn_car_type_4.setOnClickListener(mBtnClickListener);

        // 촬영 타입 선택
        btn_shoot_type_1 = findViewById(R.id.btn_shoot_type_1);
        btn_shoot_type_2 = findViewById(R.id.btn_shoot_type_2);

        btn_shoot_type_1.setOnClickListener(mBtnClickListener);
        btn_shoot_type_2.setOnClickListener(mBtnClickListener);

        // 버튼 이동 애니메이션
        animTransRight_2 = AnimationUtils.loadAnimation(this, R.anim.anim_translate_right_2);
        animTransRight_3 = AnimationUtils.loadAnimation(this, R.anim.anim_translate_right_3);
        animTransRight_4 = AnimationUtils.loadAnimation(this, R.anim.anim_translate_right_4);
        animTransLeft_2 = AnimationUtils.loadAnimation(this, R.anim.anim_translate_left_2);
        animTransLeft_2.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                btn_car_type_2.clearAnimation();
                btn_car_type_2.setVisibility(View.GONE);

                btn_shoot_type_2.clearAnimation();
                btn_shoot_type_2.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });

        animTransLeft_3 = AnimationUtils.loadAnimation(this, R.anim.anim_translate_left_3);
        animTransLeft_3.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                btn_car_type_3.clearAnimation();
                btn_car_type_3.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });

        animTransLeft_4 = AnimationUtils.loadAnimation(this, R.anim.anim_translate_left_4);
        animTransLeft_4.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                btn_car_type_4.clearAnimation();
                btn_car_type_4.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });

        // 촬영 선택 버튼
        btn_shoot_select = findViewById(R.id.btn_shoot_select);
        btn_shoot_select.setOnClickListener(mBtnClickListener);

        // 촬영 선택 ScrollView Animation
        animTransUp = AnimationUtils.loadAnimation(this, R.anim.anim_translate_up);
        animTransDown = AnimationUtils.loadAnimation(this, R.anim.anim_translate_down);
        animTransDown.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mShootSelectState = 0;
                scroll_shoot_select.clearAnimation();
                scroll_shoot_select.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });

        // 메시지(촬영 확인 등)이동 Animation
        animMsgTransDown = AnimationUtils.loadAnimation(this, R.anim.anim_translate_msg_down);
        animMsgTransUp = AnimationUtils.loadAnimation(this, R.anim.anim_translate_msg_up);
        animMsgTransUp.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                tv_question_use_pic.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });

        // 촬영 버튼
        btn_capture = findViewById(R.id.btn_capture);
        btn_capture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (mShootSelectState != 0 || iv_guide_line.getVisibility() != View.VISIBLE) {
                    //Toast.makeText(PassEvaluationShootActivity.this, getString(R.string.select_car_guide), Toast.LENGTH_SHORT).show();
                    showShootInfoToast(getString(R.string.select_car_guide), Toast.LENGTH_SHORT);
                    return;
                } else {
                    takePicture();
                }
            }
        });

        // 촬영 선택 Scroll
        scroll_shoot_select = findViewById(R.id.scroll_shoot_select);
        // 촬영 가이드 버튼의 parent
        ll_guide_select = findViewById(R.id.ll_guide_select);

        iv_shooted_picture = findViewById(R.id.iv_shooted_picture);
        tv_question_use_pic = findViewById(R.id.tv_question_use_pic);
        tv_info_current_shoot = findViewById(R.id.tv_info_current_shoot);
        btn_reshooting = findViewById(R.id.btn_reshooting);
        btn_use_pic = findViewById(R.id.btn_use_pic);
        btn_blind = findViewById(R.id.btn_blind);
        iv_blind = findViewById(R.id.iv_blind);
        iv_blind.setImageResource(R.drawable.license_blind);
        iv_blind.setOnTouchListener(mBlindTouchListener);

        btn_reshooting.setOnClickListener(mBtnClickListener);
        btn_use_pic.setOnClickListener(mBtnClickListener);
        btn_blind.setOnClickListener(mBtnClickListener);

        // 촬영된 사진 수 / 최대 촬영 수 표시
        tv_pic_count = findViewById(R.id.tv_pic_count);

        // 촬영된 사진 보여주는 ImageView 크기를 미리보기 화면과 맞춤
        iv_shooted_picture.getLayoutParams().height = nMaxHeight;
        iv_shooted_picture.getLayoutParams().width = nPreviewWidth;
        iv_shooted_picture.requestLayout();
    }

    // Set DB Sample data
    public void setTestData() {

        // Test CODE prayna // 임의의 촬영가이드 DB 생성//////////////////////////////////////////////////////////////////////
        /*GuideDBHelper guide_db;
        guide_db = new GuideDBHelper(this, Common.DB_NAME, null, 1);

        guide_db.removeAll();
        // 촬영 가이드
        guide_db.insert(0, 1, "front_side",          "차량 전면(측)", 0);
        guide_db.insert(0, 2, "back_side",           "차량 후면(측)", 0);
        guide_db.insert(0, 3, "front",               "차량 전면", 0);
        guide_db.insert(0, 4, "back",                "차량 후면", 0);
        guide_db.insert(0, 5, "left_side",           "차량 측면(운전석)", 0);
        guide_db.insert(0, 6, "right_side",          "차량 측면(조수석)", 0);

        guide_db.insert(1, 7, "instrument_board",    "내부 계기판", 0);

        guide_db.insert(0, 8, "wheel",               "휠&타이어", 0);
        guide_db.insert(0, 9, "bonnet",              "엔진룸", 0);

        guide_db.insert(1, 10, "drivers_seat",       "운전석 앞 문", 0);
        guide_db.insert(1, 11, "drivers_back_seat",  "운전석 뒷 문", 0);
        guide_db.insert(1, 12, "passenger_seat",     "조수석 앞 문", 0);
        guide_db.insert(1, 13, "passenger_back_seat","조수석 뒷 문", 0);
        guide_db.insert(1, 14, "dashboard",          "내부(앞)", 0);
        guide_db.insert(1, 15, "center_fascia",      "센터페시아", 0);
        guide_db.insert(1, 16, "overhead_console",   "룸미러", 0);

        guide_db.insert(0, 17, "top",                "차량 상단", 0);
        guide_db.insert(0, 18, "trunk",              "트렁크", 0);

        guide_db.insert(1, 19, "gearbox",            "기어박스", 0);
        guide_db.insert(1, 20, "back_camera",        "후방카메라", 0);

        guide_db.insert(0, 21, "option",             "기타", 0);

        // 무평가 촬영 가이드
        guide_db.insert(2, 1, "front",               "차량 전면", 0);
        guide_db.insert(2, 2, "instrument_board",    "내부 계기판", 0);
        guide_db.insert(2, 3, "license",             "차량등록증", 0);
        guide_db.insert(2, 4, "option",              "기타", 0);*/
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /*// Test Car Data 생성
        CarDBHelper car_db;
        car_db = new CarDBHelper(this, mDBPath, null, 1);
        car_db.insert(Common.MENU_TYPE_DETAIL_SHOOT, mCarNumber, 0, 0, Common.DETAIL_PHOTO_MAX, 0);
        //car_db.removeAll();*/
    }

    /**
     * DB에서 가이드 목록을 가져와 가이드 선택 버튼을 생성한다
     */
    private void setShootGuides() {

        GuideDBHelper guide_db;
        guide_db = new GuideDBHelper(this, mDBPath, null, 1);

        // 촬영 가이드 설정
        try {
            JSONArray arrGuide = guide_db.getGuideArrayByType(Common.PASS_EVALUATION_SHOOT_GUIDE);
            int nSize = arrGuide.length();
            mCntGuides = nSize;
            for (int i = 0; i < nSize; i++) {
                JSONObject data = arrGuide.getJSONObject(i);
                int nGuideSide = data.getInt("guide_side");
                int nGuideSortNo = data.getInt("guide_sort_no");
                String sGuideCode = data.getString("guide_code");
                String sGuideName = data.getString("guide_name");
                String sPhotoCode   = data.getString("photo_code");

                shoot_guides.add(new ShootGuide(nGuideSide, nGuideSortNo, sGuideCode, sGuideName, sPhotoCode, false, 0, ""));

                addGuideBtnLayout(nGuideSide, nGuideSortNo, sGuideName);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * 촬영 가이드 버튼을 생성하여 하단 평행스크롤뷰에 추가한다
     *
     * @param nGuideSide   촬영 외부(0)/내부(1) 값
     * @param nGuideSortNo 가이드 정렬 순서 번호
     * @param sGuideName   가이드 명
     */
    private void addGuideBtnLayout(int nGuideSide, int nGuideSortNo, String sGuideName) {

        // Creating a new RelativeLayout
        RelativeLayout relativeLayout = new RelativeLayout(this);

        // Defining the RelativeLayout layout parameters.
        // In this case I want to fill its parent
        int width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 178, getResources().getDisplayMetrics());
        int left_margin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, getResources().getDisplayMetrics());
        //Log.d(TAG, "addGuideBtnLayout() Add RelativeLayout width = " + width);
        RelativeLayout.LayoutParams rlp = new RelativeLayout.LayoutParams(width, RelativeLayout.LayoutParams.MATCH_PARENT);
        if (nGuideSortNo > 1) {
            rlp.leftMargin = left_margin;
        }
        relativeLayout.setLayoutParams(rlp);
        relativeLayout.setBackgroundResource(R.drawable.guide_btn_rect);

        // Set Tag
        relativeLayout.setTag(nGuideSortNo);

        // Add RoundImageView
        RoundImageView rImgView = new RoundImageView(this);
        // Setting the parameters on the RoundImageView
        RelativeLayout.LayoutParams roundlp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        roundlp.addRule(RelativeLayout.CENTER_IN_PARENT);
        rImgView.setScaleType(ImageView.ScaleType.FIT_XY);
        rImgView.setLayoutParams(roundlp);

        // Set RoundImageView backgound
        Resources res = getResources();
        TypedArray arrGuideBackgrounds = null;

        if (mCurCarType == 1) {          // 세단
            arrGuideBackgrounds = res.obtainTypedArray(R.array.btn_pass_evaluation_guide_lines);
        } else if (mCurCarType == 2) {    // VAN A
            arrGuideBackgrounds = res.obtainTypedArray(R.array.btn_pass_evaluation_guide_lines);
        } else if (mCurCarType == 3) {    // VAN B
            arrGuideBackgrounds = res.obtainTypedArray(R.array.btn_pass_evaluation_guide_lines);
        } else if (mCurCarType == 4) {    // 경차
            arrGuideBackgrounds = res.obtainTypedArray(R.array.btn_pass_evaluation_guide_lines);
        }

        int getGuidePos = nGuideSortNo - 1;
        int resID = arrGuideBackgrounds.getResourceId(getGuidePos, 0);
        rImgView.setImageResource(resID);

        // Add Guide name TextView
        TextView tvGuideName = new TextView(this);
        RelativeLayout.LayoutParams textlp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        int bottomMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5, getResources().getDisplayMetrics());
        textlp.bottomMargin = bottomMargin;
        textlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        textlp.addRule(RelativeLayout.CENTER_HORIZONTAL);
        tvGuideName.setLayoutParams(textlp);
        tvGuideName.setText(sGuideName);
        tvGuideName.setTextColor(Color.WHITE);

        relativeLayout.addView(rImgView);
        relativeLayout.addView(tvGuideName);

        relativeLayout.setOnClickListener(mGuideBtnClickListener);
        ll_guide_select.addView(relativeLayout);
        guide_btns.add(relativeLayout);
        guide_btn_images.add(rImgView);
    }

    /**
     * DB에 저장된 사진이 있다면(기 촬영본) 섬네일을 추출하여 가이드 선택 버튼에 넣는다
     */
    private void setPhotosToGuideBtns() {

        try {
            JSONArray arrPhotos = photo_db.getResultByCarNumberNMenuType(mCarNumber, Common.MENU_TYPE_PASS_EVALUATION_SHOOT);
            int nSize = arrPhotos.length();
            for (int i = 0; i < nSize; i++) {
                JSONObject data = arrPhotos.getJSONObject(i);
                int nPhotoID = data.getInt("_id");
                String strFileName = data.getString("photo_path");
                String strGuideCode = data.getString("guide_code");
                String strFilePath = Util.getPhotoFilePath() + strFileName;
                setPhotoToGuideBtn(nPhotoID, strGuideCode, strFilePath);
            }

            // 촬영된 사진 수 / 최대 촬영 수 표시
            updatePhotoCount();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    View.OnClickListener mBtnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            int id = v.getId();
            if (id == R.id.btn_car_type_1) {
                Log.d(TAG, "Click btn_car_type_1");
                if (mIsCarTypeSelecting == true) {  // 차량 타입 선택인 경우 펼쳐진 버튼 접기
                    mIsCarTypeSelecting = false;
                    mCurCarType = 1;
                    btn_car_type_1.setBackgroundResource(R.drawable.btn_car_type_1);
                    btn_car_type_2.startAnimation(animTransLeft_2);
                    btn_car_type_3.startAnimation(animTransLeft_3);
                    btn_car_type_4.startAnimation(animTransLeft_4);
                } else {
                    mIsCarTypeSelecting = true;     // 차량 타입 선택 버튼 펼기치
                    btn_car_type_1.setBackgroundResource(R.drawable.btn_car_type_1);
                    btn_car_type_1.setVisibility(View.VISIBLE);
                    btn_car_type_2.setVisibility(View.VISIBLE);
                    btn_car_type_3.setVisibility(View.VISIBLE);
                    btn_car_type_4.setVisibility(View.VISIBLE);
                    btn_car_type_2.startAnimation(animTransRight_2);
                    btn_car_type_3.startAnimation(animTransRight_3);
                    btn_car_type_4.startAnimation(animTransRight_4);
                }

            } else if (id == R.id.btn_car_type_2) {
                Log.d(TAG, "Click btn_car_type_2");
                if (mIsCarTypeSelecting == true) {   // 차량 타입 선택인 경우 펼쳐진 버튼 접기
                    mIsCarTypeSelecting = false;
                    mCurCarType = 2;
                    btn_car_type_1.setBackgroundResource(R.drawable.btn_car_type_2);
                    btn_car_type_2.startAnimation(animTransLeft_2);
                    btn_car_type_3.startAnimation(animTransLeft_3);
                    btn_car_type_4.startAnimation(animTransLeft_4);
                }

            } else if (id == R.id.btn_car_type_3) {
                Log.d(TAG, "Click btn_car_type_3");
                if (mIsCarTypeSelecting == true) {    // 차량 타입 선택인 경우 펼쳐진 버튼 접기
                    mIsCarTypeSelecting = false;
                    mCurCarType = 3;
                    btn_car_type_1.setBackgroundResource(R.drawable.btn_car_type_3);
                    btn_car_type_2.startAnimation(animTransLeft_2);
                    btn_car_type_3.startAnimation(animTransLeft_3);
                    btn_car_type_4.startAnimation(animTransLeft_4);
                }

            } else if (id == R.id.btn_car_type_4) {
                Log.d(TAG, "Click btn_car_type_4");
                if (mIsCarTypeSelecting == true) {    // 차량 타입 선택인 경우 펼쳐진 버튼 접기
                    mIsCarTypeSelecting = false;
                    mCurCarType = 4;
                    btn_car_type_1.setBackgroundResource(R.drawable.btn_car_type_4);
                    btn_car_type_2.startAnimation(animTransLeft_2);
                    btn_car_type_3.startAnimation(animTransLeft_3);
                    btn_car_type_4.startAnimation(animTransLeft_4);
                }

            } else if (id == R.id.btn_shoot_type_1) {
                Log.d(TAG, "Click btn_shoot_type_1");
                if (mIsShootTypeSelecting == true) {    // 촬영 타입 선택인 경우 펼쳐진 버튼 접기
                    mIsShootTypeSelecting = false;
                    mCurShootType = 1;
                    btn_shoot_type_1.setBackgroundResource(R.drawable.btn_shoot_type_1);
                    btn_shoot_type_2.startAnimation(animTransLeft_2);
                } else {
                    mIsShootTypeSelecting = true;       // 촬영 타입 선택 버튼 펼기치
                    btn_shoot_type_1.setBackgroundResource(R.drawable.btn_shoot_type_1);
                    btn_shoot_type_1.setVisibility(View.VISIBLE);
                    btn_shoot_type_2.setVisibility(View.VISIBLE);
                    btn_shoot_type_2.startAnimation(animTransRight_2);
                }

            } else if (id == R.id.btn_shoot_type_2) {
                Log.d(TAG, "Click btn_shoot_type_2");
                if (mIsShootTypeSelecting == true) {    // 촬영 타입 선택인 경우 펼쳐진 버튼 접기
                    mIsShootTypeSelecting = false;
                    mCurShootType = 2;
                    btn_shoot_type_1.setBackgroundResource(R.drawable.btn_shoot_type_2);
                    btn_shoot_type_2.startAnimation(animTransLeft_2);
                }

            } else if (id == R.id.btn_shoot_select) {    // 촬영 가이드 보기 버튼 으로 가이드 목록 위로 올리기
                Log.d(TAG, "Click btn_shoot_select");
                if (mShootSelectState == 0) {
                    mShootSelectState = 1;
                    btn_shoot_select.setBackgroundResource(R.drawable.btn_outside_select);
                    scroll_shoot_select.setVisibility(View.VISIBLE);
                    scroll_shoot_select.startAnimation(animTransUp);
                } else {
                    ShootSelectScrollDown(false); // 촬영 가이드 보기 버튼 으로 가이드 목록 내리기
                }

            // 재촬영
            } else if (id == R.id.btn_reshooting) {
                // 촬영된 사진 삭제
                //deleteShootedPic();
                mIsConfirm = false;
                mIsReShooting = true;
                capturedPictureConfirm();

            // 촬영된 사진 사용
            } else if (id == R.id.btn_use_pic) {
                Log.d(TAG, "save photo : " + mShootedPicName);
                // 촬영 사진 File 저장
                saveCapturedPhoto();
                // 촬영 사진 DB 입력
                insertPhotoToDB();
                // 사진 저장 및 하단 스크롤에 반영 처리
                mIsConfirm = false;
                capturedPictureConfirm();

            // 가리기 도구
            } else if (id == R.id.btn_blind) {
                Log.d(TAG, "Click btn_blind");
                if (mUseBlind) {     // 가리기 취소
                    mUseBlind = false;
                    btn_blind.setBackgroundResource(R.drawable.btn_blind);
                    iv_blind.setVisibility(View.GONE);
                } else {             // 가리기 도구 사용
                    mUseBlind = true;
                    btn_blind.setBackgroundResource(R.drawable.btn_blind_cancel);
                    iv_blind.setVisibility(View.VISIBLE);
                }
            }
        }
    };

    View.OnClickListener mGuideBtnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            mSelectPositon = (Integer) v.getTag();

            showShootInfoMessage(true);

            int resID = getGuideBtnBackgroundResource(Common.MENU_TYPE_PASS_EVALUATION_SHOOT);
            iv_guide_line.setBackgroundResource(resID);
            ShootSelectScrollDown(true);
        }
    };

    private int getGuideBtnBackgroundResource(int side) {

        Resources res = getResources();
        TypedArray arrGuide = null;
        int resID = 0;

        // 무평가 가이드 선택
        if (side == Common.MENU_TYPE_PASS_EVALUATION_SHOOT) {

            int getGuidePos = mSelectPositon - 1;

            if (mCurCarType == 1) {          // 세단
                arrGuide = res.obtainTypedArray(R.array.passeval_guide_lines_type_1);
            } else if (mCurCarType == 2) {    // VAN A
                arrGuide = res.obtainTypedArray(R.array.passeval_guide_lines_type_1);
            } else if (mCurCarType == 3) {    // VAN B
                arrGuide = res.obtainTypedArray(R.array.passeval_guide_lines_type_1);
            } else if (mCurCarType == 4) {    // 경차
                arrGuide = res.obtainTypedArray(R.array.passeval_guide_lines_type_1);
            }
            resID = arrGuide.getResourceId(getGuidePos, 0);
            return resID;
        }
        return resID;
    }

    /**
     * 내부/외부 차량 촬영 선택/취소로 스크롤을 내림
     * isShoot => true : 촬영 부위 선택, false : 선택 취소
     **/
    private void ShootSelectScrollDown(boolean isShoot) {

        btn_shoot_select.setBackgroundResource(R.drawable.btn_outside);

        if (isShoot) {
            iv_guide_line.setVisibility(View.VISIBLE);
        } else {
            mSelectPositon = 0;
            iv_guide_line.setVisibility(View.GONE);
        }

        if (mShootSelectState == 1) {
            scroll_shoot_select.startAnimation(animTransDown);
        }
    }

    /**
     * 미리보기와 달리 사진 이미지 캡처는 별도 CaptureSession을 생성해서 작업
     **/
    private void takePicture() {

        try {
            // set Sutter Sound
            MediaActionSound sound = new MediaActionSound();
            sound.play(MediaActionSound.SHUTTER_CLICK);

            mBMCapturedPhoto = null;

            setDialog(true);

            Size[] jpegSizes = null;

            if (map != null)
                jpegSizes = map.getOutputSizes(ImageFormat.JPEG);

            int width = 640;
            int height = 360;
            if (jpegSizes != null && 0 < jpegSizes.length) {
                /*width = jpegSizes[0].getWidth();
                height = jpegSizes[0].getHeight();*/

                for (Size size : jpegSizes) {
                    width = size.getWidth();
                    height = size.getHeight();
                    // 가로 사이즈 기준으로 16:9 비율로 조정
                    int nNewHeight = (width / 16) * 9;
                    // 절대값이 20 이내의 차이인 세로사이즈 값을 구한다
                    int nInterval = Math.abs(height - nNewHeight);
                    if ( nInterval < 20 && (width > 3000 && width < 4000) ) {       // 일단 해상도를 3840x2160 에 맞춘다 (!!!타기종 확인 필!!!)
                        break;
                    }
                }
                Log.d(TAG, "takePicture Size : width = " + width + ", height = " + height);
                mPhotoWidth = width;
                mPhotoHeight = height;
            }

            final ImageReader imageReader = ImageReader.newInstance(width, height, ImageFormat.JPEG, 1);

            List<Surface> outputSurfaces = new ArrayList<Surface>(2);
            outputSurfaces.add(imageReader.getSurface());
            outputSurfaces.add(new Surface(mTextureView.getSurfaceTexture()));

            // ImageCapture를 위한 CaputureRequest.Builder 객체
            final CaptureRequest.Builder captureBuilder = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_STILL_CAPTURE);
            captureBuilder.addTarget(imageReader.getSurface());

            // 이전 카메라 api는 이 기능 지원X
            // 이미지를 캡처하는 순간에 제대로 사진 이미지가 나타나도록 3A를 자동으로 설정
            captureBuilder.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO);

            int rotation = getWindowManager().getDefaultDisplay().getRotation();
            captureBuilder.set(CaptureRequest.JPEG_ORIENTATION, ORIENTATIONS.get(rotation));

            // Guide 버튼 선택값(mSelectPositon)으로 촬영된 사진이 저장될 이름을 가져온다
            String strPicFileName = getPicFileName();
            if (strPicFileName.length() < 2) {
                Log.e(TAG, "takePicture() getPicFileName() fail...");
                return;
            }

            mShootedPicName = strPicFileName;
            mShootedPicGuideCode = getGuideCode(mSelectPositon);

            // 이미지를 캡처할 때 자동으로 호출된다.
            ImageReader.OnImageAvailableListener readerListener = new ImageReader.OnImageAvailableListener() {
                @Override
                public void onImageAvailable(ImageReader reader) {
                    Image capturedImage = null;
                    try {
                        capturedImage = imageReader.acquireLatestImage();
                        mBMCapturedPhoto = Util.getBitmapFromImage(capturedImage);
                    } catch (Exception e) {
                        e.printStackTrace();
                        setDialog(false);
                    }
                }
            };

            // 이미지를 캡처하는 작업은 메인 스레드가 아닌 스레드 핸들러로 수행한다.
            HandlerThread thread = new HandlerThread("CameraPicture");
            thread.start();
            final Handler backgroundHandler = new Handler(thread.getLooper());

            // ImageReader와 ImageReader.OnImageAvailableListener객체를 서로 연결시켜주기 위해 설정
            imageReader.setOnImageAvailableListener(readerListener, backgroundHandler);

            // 사진 이미지를 캡처한 이후 호출되는 메소드
            final CameraCaptureSession.CaptureCallback captureCallback = new CameraCaptureSession.CaptureCallback() {
                @Override
                public void onCaptureCompleted(@NonNull CameraCaptureSession session,
                                               @NonNull CaptureRequest request, @NonNull TotalCaptureResult result) {
                    super.onCaptureCompleted(session, request, result);

                    setDialog(false);

                    // 이미지가 성공적으로 캡처되면 사진을 확인
                    mIsConfirm = true;
                    capturedPictureConfirm();
                }
            };
            /*
            사진 이미지를 캡처하는데 사용하는 CameraCaptureSession을 생성한다.
            이미 존재하면 기존 세션은 자동으로 종료
            */
            mCameraDevice.createCaptureSession(outputSurfaces, new CameraCaptureSession.StateCallback() {
                @Override
                public void onConfigured(@NonNull CameraCaptureSession session) {
                    try {
                        session.capture(captureBuilder.build(), captureCallback, backgroundHandler);

                    } catch (CameraAccessException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onConfigureFailed(@NonNull CameraCaptureSession session) {

                }
            }, backgroundHandler);

        } catch (CameraAccessException e) {
            Log.e(TAG, "takePicture() createCaptureRequest fail");
            e.printStackTrace();
        }
    }

    /**
     * 캡처된 사진을 File로 SC Card에 저장
     **/
    private void saveCapturedPhoto() {

        try {

            if(mBMCapturedPhoto == null) {
                Log.e(TAG, "saveCapturedPhoto() : mBMCapturedPhoto == null");
                return;
            }

            File storeDir = new File(Environment.getExternalStorageDirectory(), Common.PHOTO_FILE_PATH);
            if (!storeDir.exists()) {
                if (!storeDir.mkdirs()) {
                    Log.e(TAG, "saveCapturedPhoto() : failed to create directory");
                    return;
                }
            }

            String strPicPath = storeDir.getPath() + File.separator + mShootedPicName;
            File savedFile = new File(strPicPath);

            Bitmap bmWaterMark = BitmapFactory.decodeResource(getResources(), R.drawable.logo_autobell);

            if (mUseBlind) {    // 가리기 도구 사용 시(차량등록증 촬영)

                // 차량등록증 촬영 가리기 도구 초기화
                mUseBlind = false;
                btn_blind.setBackgroundResource(R.drawable.btn_blind);
                iv_blind.setVisibility(View.GONE);
            }

            Bitmap bmUseWaterMark = Util.overlayMark(mBMCapturedPhoto, bmWaterMark, mPhotoWidth - bmWaterMark.getWidth() - 15, mPhotoHeight - bmWaterMark.getHeight() - 15);
            byte[] bytesUseWaterMark = Util.bitmapToByteArray(bmUseWaterMark);
            save(bytesUseWaterMark, savedFile);

            // Exif tag 쓰기
            Util.writeExifTag(strPicPath);
            mBMCapturedPhoto = null;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void save(byte[] bytes, File savedFile) throws IOException {

        OutputStream output = null;
        try {
            output = new FileOutputStream(savedFile);
            output.write(bytes);
        } finally {
            if (output != null) output.close();
        }
    }

    /**
     * 캡처된 사진을 DB에 입력
     **/
    private void insertPhotoToDB() {

        if (mCarNumber.length() > 0) {

            if(mSelectPositon > 0) {

                int nPhotoID = shoot_guides.get(mSelectPositon - 1).getPhoto_id();

                if(nPhotoID > 0) {  // 이미 사진이 존재 (수정) 인 경우
                    photo_db.update(nPhotoID, mCarNumber, mShootedPicGuideCode, mShootedPicName, null);
                }else {             // 최초 사진 등록인 경우
                    int nInsertedID = photo_db.insert(mCarNumber, mShootedPicGuideCode, mShootedPicName, null);
                    if(nInsertedID != -1) {
                        shoot_guides.get(mSelectPositon - 1).setPhoto_id(nInsertedID);
                    }
                }

            }else {
                Log.e(TAG, "insertPhotoToDB() : mSelectPositon = " + mSelectPositon + " ERROR !!!");
                return;
            }

            mShootedPicName = "";
            mShootedPicGuideCode = "";

            // 저장된 사진 개수에 따라 차량등록상태(register_state)값 업데이트
            CarDBHelper car_db;
            car_db = new CarDBHelper(this, mDBPath, null, 1);

            JSONArray arrPhotos = photo_db.getResultByCarNumberNMenuType(mCarNumber, Common.MENU_TYPE_PASS_EVALUATION_SHOOT);
            int nSize = arrPhotos.length();
            if (nSize == mCntGuides) {                       // 촬영 가이드 개수 만큼 촬영을 했다면 촬영 완료 상태로 업데이트
                car_db.update(mCarID, Common.MENU_TYPE_PASS_EVALUATION_SHOOT, mCarNumber, Common.DETAIL_SHOOT_COMPLETE, nSize, mCntGuides, mCurCarType);
            } else if (nSize > 0 && nSize < mCntGuides) {     // 촬영 가이드 개수에 미치치 않으면 촬영중 상태로 업데이트
                car_db.update(mCarID, Common.MENU_TYPE_PASS_EVALUATION_SHOOT, mCarNumber, Common.DETAIL_SHOOT_PROGRESS, nSize, mCntGuides, mCurCarType);
            }
        }
    }

    /**
     * 캡처된 사진을 확인 또는 취소(Preview 전환)
     **/
    private void capturedPictureConfirm() {

        new Thread() {
            public void run() {
                Message msg = handler.obtainMessage();
                handler.sendMessage(msg);
            }
        }.start();
    }

    final Handler handler = new Handler() {

        public void handleMessage(Message msg) {

            String strPicFileName = getPicFileName();
            if (strPicFileName.length() < 2) {
                Log.e(TAG, "capturedPictureConfirm() getPicFileName() fail...");
            }

            // 원래 하고싶었던 일들 (UI변경작업 등...)
            if (mIsConfirm) {                   // 이미지가 성공적으로 캡쳐되면 확인 화면으로 전환
                // 캡쳐된 이미지를 로드한다
                if (mBMCapturedPhoto != null) {

                    if (mUseBlind) {    // 가리기 도구 사용 시(차량등록증 촬영)

                        BitmapDrawable drawable = (BitmapDrawable) iv_blind.getDrawable();
                        Bitmap bmBlind = drawable.getBitmap();

                        // Texture View(촬영 뷰)와 촬영된 사진 사이즈 비율 적용
                        int nPreviewWidth   = mTextureView.getLayoutParams().width;
                        int nPreviewHeight  = mTextureView.getLayoutParams().height;
                        double dRatioW = (double)mPhotoWidth / (double)nPreviewWidth;
                        Log.d(TAG, "Ratio Width : " + dRatioW);
                        Log.d(TAG, "Before Ratio Blind Position X : " + mBlindPositionX + ", Position Y : " + mBlindPositionY);
                        int nRatioPositionX = (int) (mBlindPositionX * dRatioW);
                        double dRatioH = (double)mPhotoHeight / (double)nPreviewHeight;
                        Log.d(TAG, "Ratio Height : " + dRatioH);
                        int nRatioPositionY = (int) (mBlindPositionY * dRatioH);

                        Log.d(TAG, "Set Ratio Blind Position X : " + nRatioPositionX + ", Position Y : " + nRatioPositionY);
                        //Bitmap bmUseBlind = Util.overlayMark(mBMCapturedPhoto, bmBlind, nRatioPositionX, nRatioPositionY);
                        Bitmap bmUseBlind = Util.overlayMark(mBMCapturedPhoto, bmBlind, nRatioPositionX, nRatioPositionY);
                        mBMCapturedPhoto = bmUseBlind;
                    }

                    iv_shooted_picture.setImageBitmap(mBMCapturedPhoto);
                }
                showShootInfoMessage(false);
                iv_shooted_picture.setVisibility(View.VISIBLE);
                tv_question_use_pic.setVisibility(View.VISIBLE);
                tv_question_use_pic.startAnimation(animMsgTransDown);

                btn_reshooting.setVisibility(View.VISIBLE);
                btn_use_pic.setVisibility(View.VISIBLE);

                iv_guide_line.setVisibility(View.GONE);
                mTextureView.setVisibility(View.GONE);
                btn_car_type_1.setVisibility(View.GONE);
                btn_shoot_type_1.setVisibility(View.GONE);
                btn_shoot_select.setVisibility(View.GONE);
                btn_capture.setVisibility(View.GONE);
                tv_pic_count.setVisibility(View.GONE);
            } else {
                if (mIsReShooting == false) {    // 촬영된 사진 확인 (재촬영이 아닌 상태)
                    // 외부/내부 촬영 버튼 (하단 스클로)에 촬영된 사진 썸네일 출력
                    if (strPicFileName.length() > 2) {
                        String strFilePath = Util.getPhotoFilePath() + strPicFileName;
                        if(mSelectPositon > 0) {
                            String strGuideCode = getGuideCode(mSelectPositon);
                            int nPhotoID = shoot_guides.get(mSelectPositon - 1).getPhoto_id();
                            setPhotoToGuideBtn(nPhotoID, strGuideCode, strFilePath);
                        }else {
                            Log.e(TAG, "capturedPictureConfirm() : mSelectPositon = " + mSelectPositon + " ERROR !!!");
                            return;
                        }
                    }
                    // 촬영된 사진 수 / 최대 촬영 수 표시
                    updatePhotoCount();
                }
                tv_question_use_pic.startAnimation(animMsgTransUp);

                iv_shooted_picture.setVisibility(View.GONE);
                btn_reshooting.setVisibility(View.GONE);
                btn_use_pic.setVisibility(View.GONE);

                iv_guide_line.setVisibility(View.VISIBLE);
                mTextureView.setVisibility(View.VISIBLE);
                btn_car_type_1.setVisibility(View.VISIBLE);
                btn_shoot_type_1.setVisibility(View.VISIBLE);
                btn_shoot_select.setVisibility(View.VISIBLE);
                btn_capture.setVisibility(View.VISIBLE);
                tv_pic_count.setVisibility(View.VISIBLE);

                // 재촬영인 경우
                if (mIsReShooting) {
                    mIsReShooting = false;
                    showShootInfoMessage(true);
                }
                // 촬영된 사진 확인
                else {
                    if (mCurShootType == 1) {    // 연속촬영이면 다음 촬영 체크 후, 진행
                        setContinuousShooting();
                    } else {                     // 수동촬영이면 직전 선택한 가이드 선택창 띄우고 가이드 해제 처리

                        showShootInfoMessage(false);
                        iv_guide_line.setVisibility(View.GONE);
                        mShootSelectState = 1;
                        btn_shoot_select.setBackgroundResource(R.drawable.btn_outside_select);
                        scroll_shoot_select.setVisibility(View.VISIBLE);
                        scroll_shoot_select.startAnimation(animTransUp);

                        mSelectPositon = 0;
                    }
                }
                startPreview();
            }
        }
    };

    /**
     * 촬영된 사진 수 / 최대 촬영 수 표시
     **/
    private void updatePhotoCount() {
        JSONArray arrPhotos = photo_db.getResultByCarNumberNMenuType(mCarNumber, Common.MENU_TYPE_PASS_EVALUATION_SHOOT);
        int nSize = arrPhotos.length();
        // 촬영된 사진 수 / 최대 촬영 수 표시
        String sCount = String.format("%02d / %02d", nSize, mCntGuides);
        tv_pic_count.setText(sCount);
    }

    /**
     * 가리기 도구 터치 이벤트 Listener
     **/
    View.OnTouchListener mBlindTouchListener = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {

            // 가리기 도구 터치의 경우
            if (view.getId() == R.id.iv_blind) {

                //int parentWidth = ((ViewGroup) view.getParent()).getWidth();      // 부모 View's Width
                //int parentHeight = ((ViewGroup) view.getParent()).getHeight();    // 부모 View's Height
                int parentWidth     = mTextureView.getLayoutParams().width;         // 부모 View's Width
                int parentHeight    = mTextureView.getLayoutParams().height;        // 부모 View's Height

                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {

                    float oldBlindXValue = motionEvent.getX();
                    float oldBlindYValue = motionEvent.getY();
                    //Log.d(TAG, "oldBlindXValue : " + oldBlindXValue + " oldBlindYValue : " + oldBlindYValue);                     // View 내부에서 터치한 지점의 상대 좌표값.
                    Log.d(TAG, "ACTION_DOWN v.getX() : " + view.getX() + "v.getY() : " + view.getY());                        // View 의 좌측 상단이 되는 지점의 절대 좌표값.
                    /*Log.d(TAG, "**Touch Down** motionEvent.getRawX : " + motionEvent.getRawX()
                                                 + " motionEvent.getRawX : " + motionEvent.getRawY());                              // View 를 터치한 지점의 절대 좌표값.
                    Log.d(TAG, "v.getHeight : " + view.getHeight() + " v.getWidth : " + view.getWidth());                           // View 의 Width, Height*/

                }else if(motionEvent.getAction() == MotionEvent.ACTION_MOVE){
                    // 뷰 이동 중
                    view.setX(view.getX() + (motionEvent.getX()) - (view.getWidth()/2));
                    view.setY(view.getY() + (motionEvent.getY()) - (view.getHeight()/2));

                }else if(motionEvent.getAction() == MotionEvent.ACTION_UP){
                    /*Log.d(TAG, "**Touch Up** MotionEvent.getRawX : " + motionEvent.getRawX()
                                                + " MotionEvent.getRawY : " + motionEvent.getRawY());                               // View 를 터치 종료한 지점의 절대 좌표값.*/
                    Log.d(TAG, "ACTION_UP v.getX() : " + view.getX() + "v.getY() : " + view.getY());                          // View 의 좌측 상단이 되는 지점의 절대 좌표값.
                    // 뷰에서 손을 뗌
                    if(view.getX() < 0){
                        view.setX(0);
                        mBlindPositionX = 0;
                    }else if((view.getX() + view.getWidth()) > parentWidth){
                        view.setX(parentWidth - view.getWidth());
                        mBlindPositionX = (int)view.getX();
                        mLastBlindPosX =  (int)view.getX();
                    }else {
                        mBlindPositionX = (int)view.getX();
                        mLastBlindPosX =  (int)view.getX();
                        //mBlindPositionX = (int) motionEvent.getRawX() - (view.getWidth() / 2);
                    }

                    if(view.getY() < 0){
                        view.setY(0);
                        mBlindPositionY = 0;
                    }else if((view.getY() + view.getHeight()) > parentHeight){
                        view.setY(parentHeight - view.getHeight());
                        mBlindPositionY = (int)view.getY();
                        mLastBlindPosY  =  (int)view.getY();
                    }else {
                        mBlindPositionY = (int)view.getY();
                        mLastBlindPosY  =  (int)view.getY();
                        //mBlindPositionY = (int) motionEvent.getRawY() - (view.getHeight() / 2);
                    }
                    Log.d(TAG, "**Touch Up** mBlindPositionX : " + mBlindPositionX + " mBlindPositionY : " + mBlindPositionY);          // View 를 터치 종료한 지점의 좌표값.
                }
            }
            return true;
        }
    };

    // textureView가 화면에 정상적으로 출력되면 onSurfaceTextureAvailable()호출
    private TextureView.SurfaceTextureListener mSurfaceTextureListener =
            new TextureView.SurfaceTextureListener() {
                @Override
                public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
                    // cameraManager생성하는 메소드
                    Log.d(TAG, "mSurfaceTextureListener onSurfaceTextureAvailable : Width = " + width + ", Height = " + height);
                    openCamera(width, height);
                }

                @Override
                public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
                    Log.d(TAG, "mSurfaceTextureListener onSurfaceTextureSizeChanged : Width = " + width + ", Height = " + height);
                    configureTransform(width, height);
                }

                @Override
                public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
                    return false;
                }

                @Override
                public void onSurfaceTextureUpdated(SurfaceTexture surface) {

                }
            }; //TextureView.SurfaceTextureListener

    /**
     * 1. CameraManager 생성
     * 2. 카메라에 관한 정보 얻기
     * 3. openCamera() 호출 → CameraDevice객체 생성
     */
    private void openCamera(int width, int height) {
        // CameraManager 객체 생성
        mCameraManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        try {
            // default 카메라를 선택한다.
            String cameraId = mCameraManager.getCameraIdList()[0];

            // 카메라 특성 알아보기
            CameraCharacteristics characteristics = mCameraManager.getCameraCharacteristics(cameraId);
            int level = characteristics.get(CameraCharacteristics.INFO_SUPPORTED_HARDWARE_LEVEL);
            Range<Integer> fps[] = characteristics.get(CameraCharacteristics.CONTROL_AE_AVAILABLE_TARGET_FPS_RANGES);
            Log.d(TAG, "maximum frame rate is :" + fps[fps.length - 1] + "hardware level = " + level);

            // StreamConfigurationMap 객체에는 카메라의 각종 지원 정보가 담겨있다.
            map = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);

            // 미리보기용 textureview 화면크기용을 설정 <- 제공할 수 있는 최대크기를 가리킨다.
            mPreviewSize = map.getOutputSizes(SurfaceTexture.class)[0];
            Size maxSize = map.getOutputSizes(SurfaceTexture.class)[0];
            // 가로 사이즈 기준으로 16:9 비율로 조정
            int nWidth = maxSize.getWidth();
            // 가로 화면 대비 세로 화면 크기
            int nHeight = (nWidth / 16) * 9;
            Size settedSize = new Size(nWidth, nHeight);
            Log.d(TAG, "openCamera mPreviewSize : Width = " + nWidth + ", Height = " + nHeight);
            mPreviewSize = settedSize;

            Range<Integer> fpsForVideo[] = map.getHighSpeedVideoFpsRanges();
            //Log.e(TAG, "for video :" + fpsForVideo[fpsForVideo.length - 1] + " preview Size width:" + mPreviewSize.getWidth() + ", height" + height);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                // 권한에 대한
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                        ContextCompat.checkSelfPermission(this, Manifest.permission.READ_MEDIA_IMAGES) != PackageManager.PERMISSION_GRANTED ||
                        ContextCompat.checkSelfPermission(this, Manifest.permission.READ_MEDIA_VIDEO) != PackageManager.PERMISSION_GRANTED ||
                        ContextCompat.checkSelfPermission(this, Manifest.permission.READ_MEDIA_AUDIO) != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(PassEvaluationShootActivity.this, "권한 획득 실패", Toast.LENGTH_SHORT).show();
                } else {
                    configureTransform(width, height);
                    // CameraDevice생성
                    mCameraManager.openCamera(cameraId, mStateCallback, null);
                }
            }else {
                // 권한에 대한
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                        ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                        ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(PassEvaluationShootActivity.this, "권한 획득 실패", Toast.LENGTH_SHORT).show();
                } else {
                    configureTransform(width, height);
                    // CameraDevice생성
                    mCameraManager.openCamera(cameraId, mStateCallback, null);
                }
            }

        } catch (CameraAccessException e) {
            Log.e(TAG, "openCamera() :카메라 디바이스에 정상적인 접근이 안됩니다.");
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(requestCode == REQUEST_CAMERA) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG, "Permission: " + permissions[0] + "was " + grantResults[0]);

                initLayout();

                mDBPath = Common.DB_NAME;

                if (photo_db == null) {
                    photo_db = new PhotoDBHelper(this, mDBPath, null, 1);
                }

                // Set Test DB Data
                setTestData();

                // 촬영 가이드 목록을 DB로부터 가져와 외/내부 촬영 가이드 버튼 생성
                setShootGuides();

                // 이전에 촬영한 사진이 있다면 불러와서 외/내부 버튼에 넣는다
                setPhotosToGuideBtns();
            } else {
                Toast.makeText(PassEvaluationShootActivity.this, "권한 획득 실패", Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }

    private CameraDevice.StateCallback mStateCallback
            = new CameraDevice.StateCallback() {
        @Override
        public void onOpened(@NonNull CameraDevice camera) {
            // CameraDevice 객체 생성
            mCameraDevice = camera;
            // CaptureRequest.Builder객체와 CaptureSession 객체 생성하여 미리보기 화면을 실행시킨다.
            startPreview();

            if (null != mTextureView) {
                configureTransform(mTextureView.getWidth(), mTextureView.getHeight());
            }
        }

        @Override
        public void onDisconnected(@NonNull CameraDevice camera) {
        }

        @Override
        public void onError(@NonNull CameraDevice camera, int error) {
        }
    };

    /**
     * startPreview() > CaptureRequest.Builder 객체 와 CaptureSession객체 생성
     */
    private void startPreview() {
        if( mCameraDevice==null ||
                !mTextureView.isAvailable() || mPreviewSize == null) {
            Log.e(TAG, "startPreview() fail , return ");
            return;
        }

        SurfaceTexture texture = mTextureView.getSurfaceTexture();
        Surface surface = new Surface(texture);
        try {
            mPreviewBuiler = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
        } catch (CameraAccessException e) {
            Log.e(TAG, "CaptureRequest 객체 생성 실패");
            e.printStackTrace();
        }
        mPreviewBuiler.addTarget(surface);

        try {
            mCameraDevice.createCaptureSession(Arrays.asList(surface),  // / 미리보기용으로 위에서 생성한 surface객체 사용
                    new CameraCaptureSession.StateCallback() {
                        @Override
                        public void onConfigured(@NonNull CameraCaptureSession session) {
                            mPreviewSession = session;
                            updatePreview();
                        }

                        @Override
                        public void onConfigureFailed(@NonNull CameraCaptureSession session) {

                        }
                    }, null );

        } catch (CameraAccessException e) {
            Log.e(TAG, "CaptureSession 객체 생성 실패");
            e.printStackTrace();
        }
    }

    /**
     * updatePreview()에서 CaptureRequest객체를 카메라 시스템에 전달
     */
    private void updatePreview() {
        if(mCameraDevice == null) {
            return;
        }
        mPreviewBuiler.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO);

        HandlerThread thread = new HandlerThread("CameraPreview");
        thread.start();
        Handler backgroundHandler = new Handler(thread.getLooper());
        try {
            mPreviewSession.setRepeatingRequest(mPreviewBuiler.build(), null, backgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    /**
     * Configures the necessary {@link Matrix} transformation to `mTextureView`.
     * This method should not to be called until the camera preview size is determined in
     * openCamera, or until the size of `mTextureView` is fixed.
     *
     * @param viewWidth  The width of `mTextureView`
     * @param viewHeight The height of `mTextureView`
     */
    private void configureTransform(int viewWidth, int viewHeight) {

        if (null == mTextureView || null == mPreviewSize) {
            return;
        }
        Log.d(TAG, "configureTransform viewWidth = " + viewWidth + ", viewHeight = " + viewHeight);
        int rotation = getWindowManager().getDefaultDisplay().getRotation();
        Matrix matrix = new Matrix();
        RectF viewRect = new RectF(0, 0, viewWidth, viewHeight);
        RectF bufferRect = new RectF(0, 0, mPreviewSize.getHeight(), mPreviewSize.getWidth());
        float centerX = viewRect.centerX();
        float centerY = viewRect.centerY();
        if (Surface.ROTATION_90 == rotation || Surface.ROTATION_270 == rotation) {
            bufferRect.offset(centerX - bufferRect.centerX(), centerY - bufferRect.centerY());
            matrix.setRectToRect(viewRect, bufferRect, Matrix.ScaleToFit.FILL);
            float scale = Math.max(
                    (float) viewHeight / mPreviewSize.getHeight(),
                    (float) viewWidth / mPreviewSize.getWidth());
            matrix.postScale(scale, scale, centerX, centerY);
            matrix.postRotate(90 * (rotation - 2), centerX, centerY);
        }
        mTextureView.setTransform(matrix);
    }

    /**
     * 촬영 완료된 가이드(차량 부위) 이후 연속으로 다음 가이드 및 촬영 모드 설정
     */
    public void setContinuousShooting() {

        // 마지막 촬영 가이드 Position (즉, 내부 촬영 마지막 순서) 인 경우, 처음 순서부터 체크한다
        if(mSelectPositon == mCntGuides) {
            mSelectPositon = 0;
        }else if (mSelectPositon == 0) {
            return;
        }

        int nNextPosition = 0;

        // nNextPosition Check (가이드 버튼 목록 중, 촬영되지 않은 순서로 진행되도록
        int nGuideCount = shoot_guides.size();
        boolean isShootingComplete = true;     // 연속촬영 완료 여부 (모든 부위 사진 촬영)

        // 촬영 가이드 중 촬영되지 않는 곳이 있는지 체크 (뒤 순서 방향으로 체크 즉, 앞 순서에 촬영 안된 곳이 있어도 뒤로 체크 진행)
        for(int i = 0; i < nGuideCount; i++) {
            boolean existPhoto = shoot_guides.get(i).getExistPhoto();
            if( false == existPhoto && mSelectPositon < (i + 1) ) {
                nNextPosition = i + 1;
                isShootingComplete = false;
                break;
            }
        }

        if(true == isShootingComplete) {                                                        // 가이드 마지막 촬영 순번까지 도달했다면

            int nNoPhotoPosition = getNoPhotoPosition(nGuideCount);                             // 아직 촬영되지 않은 부위가 있는지 확인 후,
            if(nNoPhotoPosition == 0) {                                                         // 모든 부위를 촬영했다면 연속촬영 종료(가이드 해지)
                mSelectPositon = 0;
                iv_guide_line.setVisibility(View.GONE);
                Log.d(TAG, "setContinuousShooting() - Finished!");
                //Toast.makeText(PassEvaluationShootActivity.this, getString(R.string.finish_continueous_shooting), Toast.LENGTH_LONG).show();
                showShootInfoToast(getString(R.string.finish_continueous_shooting), Toast.LENGTH_LONG);
                return;
            }else {                                                                             // 남은 촬영 부위가 있다면 그 부위의 Position으로 Setting
                mSelectPositon = nNoPhotoPosition;
            }

        }else {
            mSelectPositon = nNextPosition;
            Log.d(TAG, "setContinuousShooting() - Next Shoot Position = " + nNextPosition);
        }

        Resources res = getResources();
        TypedArray arrGuide = null;

        // 촬영 가이드 선택
        if (mCurCarType == 1) {          // 세단
            arrGuide = res.obtainTypedArray(R.array.passeval_guide_lines_type_1);
        } else if (mCurCarType == 2) {    // VAN A
            arrGuide = res.obtainTypedArray(R.array.passeval_guide_lines_type_1);
        } else if (mCurCarType == 3) {    // VAN B
            arrGuide = res.obtainTypedArray(R.array.passeval_guide_lines_type_1);
        } else if (mCurCarType == 4) {    // 경차
            arrGuide = res.obtainTypedArray(R.array.passeval_guide_lines_type_1);
        }

        int getGuidePos = nNextPosition - 1;
        int resID = arrGuide.getResourceId(getGuidePos, 0);
        iv_guide_line.setBackgroundResource(resID);

        showShootInfoMessage(true);
    }

    /**
     * 현재 촬영 부위 안내 메시지 출력 (차량등록증 촬영인 경우 관련 메시지 및 가리기 도구 버튼 상태 결정)
     */
    private void showShootInfoMessage(boolean isShow) {

        if(isShow) {
            tv_info_current_shoot.setVisibility(View.VISIBLE);

            String strGuideCode = getGuideCode(mSelectPositon);
            String strMessage = "";
            if (strGuideCode.equals("PASS_LC")) {    // 차량 등록증 촬영일 경우
                strMessage = getString(R.string.inform_shoot_license);
                // 가리기 도구(iv_blind) 위치값 유지
                /*if(mLastBlindPosX != 0 || mLastBlindPosY != 0) {
                    iv_blind.setX(mLastBlindPosX);
                    iv_blind.setY(mLastBlindPosY);
                    if(mUseBlind) {
                        iv_blind.setVisibility(View.VISIBLE);
                    }
                }*/
                btn_blind.setVisibility(View.VISIBLE);

            } else {
                strMessage = getString(R.string.inform_shoot_side, strGuideCode);
                btn_blind.setVisibility(View.GONE);
                iv_blind.setVisibility(View.GONE);
            }
            tv_info_current_shoot.setText(strMessage);
            //tv_info_current_shoot.startAnimation(animMsgTransDown);
        }else {
            tv_info_current_shoot.setVisibility(View.GONE);
            btn_blind.setVisibility(View.GONE);
        }

        if(mUseBlind) {
            // 차량등록증 촬영 가리기 도구 초기화
            mUseBlind = false;
            btn_blind.setBackgroundResource(R.drawable.btn_blind);
            iv_blind.setVisibility(View.GONE);
        }
    }

    /**
     * 촬영 안내 Toast를 출력
     */
    private void showShootInfoToast(String strText, int nDuration) {

        LayoutInflater inflater = getLayoutInflater();
        View toastDesign = inflater.inflate(R.layout.toast_next_shoot_info, (ViewGroup)findViewById(R.id.toast_design_root)); //toast_design.xml 파일의 toast_design_root 속성을 로드
        TextView text = toastDesign.findViewById(R.id.tv_toast_design);
        text.setText(strText);

        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER, 0, - (Util.getScreenHeight() / 3)); // CENTER를 기준으로 0, 상단 위치에 메시지 출력
        toast.setDuration(nDuration);
        toast.setView(toastDesign);
        toast.show();
    }

    /**
     * 촬영되지 않은 가이드의 가장 빠른 순번을 구한다
     * @param nGuideCount : 무평가 가이드 개수
     * @return 아직 촬영되지 않은 가장 빠른 외/내부 촬영 가이드 순번 (0 : 모든 촬영 완료)
     */
    private int getNoPhotoPosition(int nGuideCount) {

        int nNextPosition = 0;
        // 촬영 가이드 중 촬영되지 않는 곳이 있는지 체크
        for(int i = 0; i < nGuideCount; i++) {
            boolean existPhoto = shoot_guides.get(i).getExistPhoto();
            if( false == existPhoto ) {
                nNextPosition = i + 1;
                return nNextPosition;
            }
        }
        return 0;
    }

    private void setDialog(boolean show){

        if(mDialog != null) {
            if (show) {
                mDialog.show();
            } else {
                mDialog.dismiss();
            }
        }
    }

    public Point getScreenSize(AppCompatActivity activity) {
        Display display = activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return  size;
    }

    /**
     * 현재 선택된 가이드(차량 부위)를 기준으로 저장할 사진 파일명을 설정
     * 선택된 가이드 순번으로 촬영가이드코드를 읽어온다.
     * @return 저장할 사진 파일 명
     */
    public String getPicFileName() {

        if (mSelectPositon == 0) {
            return "";
        }

        String strPicFileName = "";
        String strGuideCode = getGuideCode(mSelectPositon);
        if (strGuideCode.length() < 2) {
            return "";
        }
        strPicFileName = String.valueOf(mCarID) + "_" + strGuideCode + Common.PHOTO_FILE_EXTENSION;

        return strPicFileName;
    }

    /**
     * 선택된 가이드 위치값으로 촬영가이드코드를 읽어온다.
     * @return 선택된 촬영가이드 코드
     */
    public String getGuideCode(int nSelPos) {

        int nGuideSize = shoot_guides.size();
        int nReturnPos = 0;

        nReturnPos = nSelPos - 1;
        if(nReturnPos < 0) {
            return "";
        }
        return shoot_guides.get(nReturnPos).getGuide_code();
    }

    /**
     * 선택된 가이드 위치값으로 촬영가이드명을 읽어온다.
     * @return 선택된 촬영가이드 명
     */
    public String getGuideName(int nSelPos) {

        int nGuideSize = shoot_guides.size();
        int nReturnPos = 0;

        nReturnPos = nSelPos - 1;
        if(nReturnPos < 0) {
            return "";
        }
        return shoot_guides.get(nReturnPos).getGuide_name();
    }

    /**
     * 현재 선택된 가이드(차량 부위) 버튼에 촬영된 사진을 배경으로 설정
     * 설정 순서는 외부촬영가이드 다음 내부촬영가이드 순으로 한다 (따라서 내부 Position은 (외부 개수 + 내부순서)이다)
     * @param photo_id 촬영된 사진 DB id
     * @param photo_guide_code 촬영된 사진 가이드 코드
     * @param path 촬영된 사진 파일 경로
     */
    public void setPhotoToGuideBtn(int photo_id, String photo_guide_code, String path) {

        File imgFile = new File(path);
        if (imgFile.exists()) {
            // 썸네일 추출
            Bitmap thumbnail = ThumbnailUtils.extractThumbnail(BitmapFactory.decodeFile(path), 178, 100);

            int nPosition = 0;
            // 무평가 촬영 가이드 사진
            for(ShootGuide guideData : shoot_guides) {
                String sGuideCode = guideData.getGuide_code();
                if(photo_guide_code.equals(sGuideCode)) {
                    guide_btn_images.get(nPosition).setImageBitmap(thumbnail);
                    shoot_guides.get(nPosition).setExistPhoto(true);
                    shoot_guides.get(nPosition).setPhoto_id(photo_id);
                    return;
                }else {
                    nPosition++;
                }
            }
        }
    }

    /**
     * 촬영된 사진을 삭제 (재촬영 등)
     */
    public void deleteShootedPic() {

        String deleteFileName = getPicFileName();

        if (deleteFileName.length() > 2) {
            String strFilePath = Util.getPhotoFilePath() + deleteFileName;
            File f = new File(strFilePath);
            if(f.delete()) {
                Log.d(TAG, "deleteShootedPic() Success");
            }else {
                Log.e(TAG, "deleteShootedPic() Failed...");
            }
        }else {
            Log.e(TAG, "deleteShootedPic() Failed get Pic Filename");
        }
    }

    /**
     * Bitmap 이미지를 가운데를 기준으로 w, h 크기 만큼 crop한다.
     *
     * @param src 원본
     * @param w 넓이
     * @param h 높이
     * @return
     */
    public static Bitmap cropCenterBitmap(Bitmap src, int w, int h) {
        if(src == null)
            return null;
        int width = src.getWidth();
        int height = src.getHeight();

        if(width < w && height < h)
            return src;

        int x = 0;
        int y = 0;

        if(width > w)
            x = (width - w) / 2;
        if(height > h)
            y = (height - h) / 2;

        int cw = w; // crop width
        int ch = h; // crop height

        if(w > width)
            cw = width;
        if(h > height)
            ch = height;

        Log.d(TAG, "cropCenterBitmap createBitmap : X = " + x + ", Y = " + y + ", crop width = " + cw + ", crop height = " + ch);
        return Bitmap.createBitmap(src, x, y, cw, ch);
    }

}