package glovis.glovisaa.autobell.data;

import java.io.File;

public class UploadImgData {

    int photo_id;
    String phCode;
    File uploadFile;
    String fileUri;

    public int getPhoto_id() {
        return photo_id;
    }
    public String getPhCode() {
        return phCode;
    }
    public File getUploadFile() {
        return uploadFile;
    }
    public String getFileUri() {
        return fileUri;
    }

    public UploadImgData(int photo_id, String phCode, File uploadFile, String fileUri) {
        this.photo_id = photo_id;
        this.phCode = phCode;
        this.uploadFile = uploadFile;
        this.fileUri = fileUri;
    }
}