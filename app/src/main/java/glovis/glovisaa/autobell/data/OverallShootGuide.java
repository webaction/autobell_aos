package glovis.glovisaa.autobell.data;

public class OverallShootGuide {
    int guide_side;
    int guide_sort_no;
    String guide_code;
    String guide_name;
    boolean exist_photo;
    int photo_id;
    String photo_file_name;
    String photo_uri;

    public int getGuide_side() {
        return guide_side;
    }
    public int getGuide_sort_no() {
        return guide_sort_no;
    }
    public String getGuide_code() {
        return guide_code;
    }
    public String getGuide_name() {
        return guide_name;
    }
    public boolean getExistPhoto() {
        return exist_photo;
    }
    public void setExistPhoto(boolean bExist) {
        this.exist_photo = bExist;
    }
    public int getPhoto_id() {
        return photo_id;
    }
    public void setPhoto_id(int photo_id) {
        this.photo_id = photo_id;
    }
    public String getPhoto_file_name() {
        return photo_file_name;
    }
    public void setPhoto_file_name(String photo_file_name) {
        this.photo_file_name = photo_file_name;
    }
    public void setPhoto_uri(String photo_uri) {
        this.photo_uri = photo_uri;
    }
    public String getPhoto_uri() {
        return photo_uri;
    }

    public OverallShootGuide(int guide_side, int guide_sort_no, String guide_code, String guide_name, boolean exist_photo, int photo_id, String photo_file_name, String photo_uri) {
        this.guide_side = guide_side;
        this.guide_sort_no = guide_sort_no;
        this.guide_code = guide_code;
        this.guide_name = guide_name;
        this.exist_photo = exist_photo;
        this.photo_id = photo_id;
        this.photo_file_name = photo_file_name;
        this.photo_uri = photo_uri;
    }
}