package glovis.glovisaa.autobell.data;

public class ShootGuide {
    int guide_side;
    int guide_sort_no;
    String guide_code;
    String guide_name;
    String photo_code;
    boolean exist_photo;
    int photo_id;
    String photo_uri;

    public int getGuide_side() {
        return guide_side;
    }
    public int getGuide_sort_no() {
        return guide_sort_no;
    }
    public String getGuide_code() {
        return guide_code;
    }
    public String getGuide_name() {
        return guide_name;
    }
    public String getPhoto_code() {
        return photo_code;
    }
    public boolean getExistPhoto() {
        return exist_photo;
    }
    public int getPhoto_id() {
        return photo_id;
    }
    public String getPhoto_uri() {
        return photo_uri;
    }

    public void setExistPhoto(boolean bExist) {
        this.exist_photo = bExist;
    }
    public void setPhoto_id(int photo_id) {
        this.photo_id = photo_id;
    }
    public void setPhoto_uri(String photo_uri) {
        this.photo_uri = photo_uri;
    }

    public ShootGuide(int guide_side, int guide_sort_no, String guide_code, String guide_name, String photo_code, boolean exist_photo, int photo_id, String photo_uri) {
        this.guide_side = guide_side;
        this.guide_sort_no = guide_sort_no;
        this.guide_code = guide_code;
        this.guide_name = guide_name;
        this.photo_code = photo_code;
        this.exist_photo = exist_photo;
        this.photo_id = photo_id;
        this.photo_uri = photo_uri;
    }
}